<?php
/*
Plugin Name: Ejercicio 5
Plugin URI: http://local.wordpress.test/
Description: 5.- Añadir en los post el título de quién lo ha escrito.
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/


defined('ABSPATH') or die("Bye bye");

//## En este plugin uso un hook para introducir el nombre del autor de los post al título

function yith_add_author_name_post_title($title){

    //Obtengo el nombre del autor y lo concateno al titulo
    $author = get_the_author();
    $title = $title ." Escrito por " . $author;
    
    return $title;

}

//Vinculo la función al hook del titulo
add_filter('the_title', 'yith_add_author_name_post_title');


?>