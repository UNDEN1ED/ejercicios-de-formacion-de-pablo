<?php

/*
Plugin Name: Ejercicio 1
Plugin URI: http://local.wordpress.test/
Description: 1.- Mostrar al final de cada post un mensaje tipo:
                "Este post ha sido escrito por Christian"
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/

defined('ABSPATH') or die("Bye bye");

//Esta es la función que vincularemos al hook, en esta función cojo el contenido del post y guardamos en otra variable el cotenido a añadir y lo concatenamos;
function yith_add_predefined_author_message($content){
    
    //Contenido original
    $original_content = $content;
    //Contenido nuevo
    $added_content = '<p><i>"Este post ha sido escrito por Christian"</i></p>';
    //Concatenación
    $content = $original_content . $added_content;

    return $content;
}
//Vinculo la función al contenido del post
add_filter('the_content', 'yith_add_predefined_author_message', 10);

?>

