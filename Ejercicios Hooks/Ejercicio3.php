<?php
/*
Plugin Name: Ejercicio 3
Plugin URI: http://local.wordpress.test/
Description: 3.-Crear dos post distintos con contenido y al final de cada post, escribir el autor del otro post.
Author: Pablo Pérez Arteaga.
Version: 1.7.2
Author URI: 
*/

defined('ABSPATH') or die("Bye bye");

//### Para este plugin será necesario tener dos post en la base de datos con el id 16 y 18, sino cambia las dos ids por dos de tus posts
    

/*
Esta es la función que vincularemos al hook, en ella obtengo los autores de dos post diferentes predefinidos sabiendo el id y luego dependiendo del Id que tenga introduciremos
el el autor un autor u otro. 
*/
function yith_message_author_from_another_post($content){
    //Obtengo los id de los autores
    $post1= get_post(16);
    $authorid_page1 = $post1 ->post_author;
    $post2 = get_post(18);
    $authorid_page2 = $post2 ->post_author;
    //Compruebo el id de post actual e introduzco el nombre del autor del post contrario
    if(get_the_ID() == 16){
        $content = $content. '<p>'.get_the_author_meta('display_name', $authorid_page2). '</p>';
    }elseif(get_the_ID() == 18){
        $content = $content. '<p>'.get_the_author_meta('display_name', $authorid_page1). '</p>';
    }
    return $content;
}
//Vinculo la función al contenido del post
add_filter('the_content', 'yith_message_author_from_another_post', 10);
?>