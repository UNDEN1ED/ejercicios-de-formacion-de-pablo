<?php

/*
Plugin Name: Ejercicio 4
Plugin URI: http://local.wordpress.test/
Description: 4.- A la hora de guardar un post, guardar una serie de datos aleatorios como puede ser.
	                “Nombre del padre del autor”
	                “Nombre de la madre del autor”
	                “Edad”
                Al final de todo, imprimir estos datos al final del post.
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/

defined('ABSPATH') or die("Bye bye");

/*
### Para este plugin he usado dos hooks con una función respectiva para introducir y sacar datos de la tabla post_meta de wordpress para luego mostrarlos en el 
contenido del post
*/


//Con esta función he introducido un array bidreccional y un bluce para ir por cada array e introcir los datos en la tabla post_meta de mi pagina web

function yith_add_meta_info_post($post_id){

    // Datos que introduciremos en la tabla, con la key del meta y el valor
    $datos = [
        ['yith_author_dad_name','Rodrigo'],
        ['yith_author_mom_name', 'Marisa'],
        ['yith_author_age', '33']
    ];
    
    // Bucle para introducir los datos
    foreach ($datos as $dato){
        update_post_meta($post_id, $dato[0], $dato[1]);
       error_log( print_r( $datos, true));
    }

}
 //En esta otra funcion la utilizo para sacar los valore meta previamente introducidos en la tabla y mostralos en el contenido de su post
function yith_add_meta_post_content($content){

    //Realizo una pequeña comprobación para saber si ese post tiene las claves meta
    if(get_post_meta(get_the_ID(), 'yith_author_dad_name',true)){
    //Obtenemos el contenido del post e introducimos los valores de la tabla meta con get_post_meta e introduciendo el ID del mismo post y el nombre de la key    
    $original_content = $content;
    $new_content_dad = get_post_meta(get_the_ID(), 'yith_author_dad_name',true);
    $new_content_mum = get_post_meta(get_the_ID(), 'yith_author_mom_name', true);
    $new_content_age = get_post_meta(get_the_ID(), 'yith_author_age', true);
    $added_content = "<p> Dad's name: ". $new_content_dad .  "<br>" 
                          ."Mom's name: ". $new_content_mum . "<br>"
                          . "Age: ". $new_content_age . "</p>";
    $content = $original_content . $added_content;
    };

    return $content;
}

//Añado la funcionalidad de introducir los datos a la tabla siempre que se guarde un post
add_action('save_post', 'yith_add_meta_info_post');

//Vinculo la función para mostar los datos al contenido.
add_filter('the_content', 'yith_add_meta_post_content');




?>