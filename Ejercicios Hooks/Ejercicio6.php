<?php
/*
Plugin Name: Ejercicio 6
Plugin URI: http://local.wordpress.test/
Description: 6.- Añadir en el mismo post, la fecha de publicación del post
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/

defined('ABSPATH') or die("Bye bye");

//### En este plugin introduzco la fecha de publicación del post al contenido del post

function yith_add_date_post_content($content){

    //Guardo en una variable la fecha de publicacion del post y añado un texto extra y etiquetas para mejor presentación
    $publish_date = '<p><i> Publish: '.get_the_date().'</i></p>';

    //Concateno la fecha con el contenido
    $content = $content . $publish_date;

    return $content;

};


//Vinculo la función al contenido del post
add_filter('the_content', 'yith_add_date_post_content');

?>