<?php
/*
Plugin Name: Ejercicio 7
Plugin URI: http://local.wordpress.test/
Description: 7.- Del post, borrar el nombre del autor sin borrar nuestro código personalizado que hemos creado previamente.
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/
defined('ABSPATH') or die("Bye bye");

//### En este plugin eliminaré los cambio realizado al título recuperando el valor de la base de datos y asi eliminando cualquier cambio realizado por otro plugin 

function yith_delete_author_name_title($title){

    //Obtengo cada post
    global $post;
    //Saco el valor del titulo del post y se lo asigno al título
    $title = $post->post_title;
    return $title;
}

//Vinculo  la función al hook del titulo
add_filter('the_title', 'yith_delete_author_name_title', 20,1);

?>