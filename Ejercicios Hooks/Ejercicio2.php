<?php

/*
Plugin Name: Ejercicio 2
Plugin URI: http://local.wordpress.test/
Description: 2.- Partiendo del ejercicio anterior, mostrar el nombre del autor que ha hecho ese post
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/

defined('ABSPATH') or die("Bye bye");

//Esta es la función que vincularemos al hook, en ella cojo el contenido del post y guardamos en otra variable el cotenido a añadir y lo concatenamos;
function yith_add_author_message($content){
    
    //Contenido original
    $original_content = $content;
    //Contenido nuevo obteniendo el autor del post
    $added_content = '<p><i>"Este post ha sido escrito por '. get_the_author() . '"</i></p>';
    //Concatenación
    $content = $original_content . $added_content;

    return $content;
}
//Vinculo la función al contenido del post
add_filter('the_content', 'yith_add_author_message', 10);

?>
