

jQuery(document).ready(function ($) {


$('.yith_ppwcpm_save_messsage_button').on('click', function(){
    var form_arr ={};
    form_arr['rule_onoff'] = $('#yith_ppwcpm_apply_rules_onoff').val();
    form_arr['search_product'] = $('#yith_ppwcpm_search_product').val();
    form_arr['product_message'] = $('#yith_ppwcpm_product_message').val();
    form_arr['placement_select'] = $('#yith_ppwcpm_message_placements_select').val();
    form_arr['price_onoff'] = $('#yith_ppwcpm_hide_price_onoff').val();
    jQuery.ajax({
        url: yith_ppwcpm_ajax.ajaxurl,
        type: "POST",
        data: {
            action: "save_action",
            values: form_arr,
        },
        error: function () {
        },
        success: function (response) {
            window.onbeforeunload = null;
            window.location.replace(response.back_to_list);
            
        }

    });
});

$('.yith_ppwcpm_back_product_list_button').on('click', function(){
    jQuery.ajax({
        url: yith_ppwcpm_ajax.ajaxurl,
        type: "POST",
        data: {
            action: "back_action",
        },
        error: function () {
        },
        success: function (response) {
            window.onbeforeunload = null;
            window.location.replace(response.back_to_list);
            
        }

    });
});

$('.yith_ppwcpm_apply_rule_product_list').find('input').on('change', function(){
    let data_colummn_product_message = {};
    data_colummn_product_message['value'] = $(this).val();
    data_colummn_product_message['id'] = $(this).parent().data('id');

    jQuery.ajax({
        url: yith_ppwcpm_ajax.ajaxurl,
        type: "POST",
        data: {
            action: "apply_rule_action",
            values: data_colummn_product_message,
        },
        error: function () {
        },
        success: function (response) {
            
        }

    });
});

    

}, false);