<?php
/**
 * Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPM_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_PPWCPM_Plugin_Skeleton' ) ) {
	/**
	 * YITH_PPWCPM_Plugin_Skeleton
	 *
	 * @var static $instance instace of the class
	 * @param $frontend frontend main file
	 */
	class YITH_PPWCPM_Plugin_Skeleton {

		/**
		 * Description
		 *
		 * @var YITH_PPWCPM_Plugin_Skeleton
		 */

		private static $instance;   // Variable.

		/**
		 * Description
		 *
		 * @var YITH_PPWCPM_Frontend
		 */

		public $frontend = null;

		/**
		 * Main Frontend Instance
		 *
		 * @var YITH_PPWCPM_Frontend
		 */

		public $admin = null;


		/**
		 * Get_instance
		 *
		 * @return  YITH_PPWCPM_Plugin_Skeleton
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			$require = apply_filters(
				'yith_ppwcpm_require_class',
				array(
					'common'   => array(
						'includes/functions.php',

					),
					'frontend' => array(
						'includes/class-yith-ppwcpm-frontend.php',
					),
					'admin'    => array(
						'includes/class-yith-ppwcpm-admin.php',

					),
				)
			);

			$this->require_( $require );

			$this->init_classes();

			$this->init();

		}
		/**
		 *
		 * Require_()
		 *
		 * @param mixed $main_classes each document of the folder include.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_PPWCPM_DIR_PATH . $class ) ) {
						require_once YITH_PPWCPM_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {

		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( ! is_admin() ) {
				$this->frontend = YITH_PPWCPM_Frontend::get_instance();
			}
			if ( is_admin() ) {
				$this->admin = YITH_PPWCPM_Admin::get_instance();
			}
		}

		/**
		 * Plugin_fw_loader
		 *
		 * @return void
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

	}
}

if ( ! function_exists( 'yith_ppwcpm_plugin_skeleton' ) ) {
	/**
	 * Yith_ppwcpm_plugin_skeleton
	 *
	 * @return YITH_PPWCPM_Plugin_Skeleton
	 */
	function yith_ppwcpm_plugin_skeleton() {
		return YITH_PPWCPM_Plugin_Skeleton::get_instance();
	}
}
