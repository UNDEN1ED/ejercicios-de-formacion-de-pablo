<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPM_Frontend' ) ) {
	/**
	 * YITH_PPWCPM_Frontend
	 */
	class YITH_PPWCPM_Frontend {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPM_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPM_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'woocommerce_get_price_html', array( $this, 'hide_price_product' ), 10, 2 );
			add_filter( 'woocommerce_cart_item_price', array( $this, 'hide_price_cart_checkout' ), 10, 3 );
			add_filter( 'woocommerce_cart_item_subtotal', array( $this, 'hide_price_cart_checkout' ), 10, 3 );
			add_action( 'woocommerce_order_formatted_line_subtotal', array( $this, 'hide_price_order' ), 10, 2 );
			add_action( 'the_post', array( $this, 'check_for_message_product_display' ) );
		}

		/**
		 * Hide_price_ppwcpm_product
		 *
		 * @param  mixed $price
		 * @param  mixed $product
		 * @return void
		 */
		public function hide_price_product( $price, $product ) {
			error_log( print_r( $product->get_id(), true ) );
			$product_message_meta = get_post_meta( $product->get_id(), '_yith_ppwcpm_values', true );
			if ( $product_message_meta ) {
				if ( isset( $product_message_meta['hide_price_onoff'] ) && 'yes' === $product_message_meta['hide_price_onoff'] ) {
					$price = '';
				}
			}

			return $price;

		}

		/**
		 * Hide_price_cart_checkout
		 *
		 * @param  mixed $wc
		 * @param  mixed $cart_item
		 * @param  mixed $cart_item_key
		 * @return void
		 */
		public function hide_price_cart_checkout( $wc, $cart_item, $cart_item_key ) {
			$product_message_meta = get_post_meta( $cart_item['product_id'], '_yith_ppwcpm_values', true );

			if ( $product_message_meta ) {
				if ( isset( $product_message_meta['hide_price_onoff'] ) && 'yes' === $product_message_meta['hide_price_onoff'] ) {
					$wc = '';
				}

				return $wc;

			}

		}

		/**
		 * Hide_price_order
		 *
		 * @param  mixed $subtotal
		 * @param  mixed $item
		 * @return void
		 */
		public function hide_price_order( $subtotal, $item ) {
			error_log( 'Item' . print_r( $item->get_product_id(), true ) );

			$product_message_meta = get_post_meta( $item->get_product_id(), '_yith_ppwcpm_values', true );
			if ( $product_message_meta ) {
				if ( isset( $product_message_meta['hide_price_onoff'] ) && 'yes' === $product_message_meta['hide_price_onoff'] ) {
					$subtotal = '';
				}
			}
			return $subtotal;
		}

		/**
		 * Hide_price_order
		 *
		 * @return void
		 */
		public function check_for_message_product_display() {
			global $post;
			if ( wc_get_product( $post->ID ) ) {
				$product_meta = get_post_meta( $post->ID, '_yith_ppwcpm_values', true );
				if ( $product_meta ) {
					$message_placement = $product_meta['message_placement_select'];
					if ( 'before_title' === $message_placement ) {
						add_action( 'woocommerce_single_product_summary', array( $this, 'display_product_message' ), 4 );
					} elseif ( 'after_title' === $message_placement ) {
						add_action( 'woocommerce_single_product_summary', array( $this, 'display_product_message' ), 6 );
					} elseif ( 'before_price' === $message_placement ) {
						add_action( 'woocommerce_single_product_summary', array( $this, 'display_product_message' ), 8 );
					} elseif ( 'after_price' === $message_placement ) {
						add_action( 'woocommerce_single_product_summary', array( $this, 'display_product_message' ), 13 );
					} elseif ( 'before_add_to_cart' === $message_placement ) {
						add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'display_product_message' ), 10 );
					} elseif ( 'after_add_to_cart' === $message_placement ) {
						add_action( 'woocommerce_after_add_to_cart_form', array( $this, 'display_product_message' ), 10 );

					}
				}
			}
		}

		/**
		 * Display_product_message
		 *
		 * @return void
		 */
		public function display_product_message() {
			global $post;

			$product_meta = get_post_meta( $post->ID, '_yith_ppwcpm_values', true );
			echo '<p>' . esc_html( $product_meta['product_message'] ) . '</p>';
		}
	}
}
