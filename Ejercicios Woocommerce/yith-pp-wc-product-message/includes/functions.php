<?php
/**
 *
 * Commons Functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_ppwcpm_get_view' ) ) {

	/**
	 * Yith_ppwcpm_get_view
	 *
	 * @param  mixed $file_name name of the file.
	 * @param  mixed $args array with the details.
	 * @param  mixed $post desired post.
	 * @return void
	 */
	function yith_ppwcpm_get_view( $file_name, $args = array(), $post = '' ) {
		$full_path = YITH_PPWCN_DIR_VIEWS_PATH . $file_name;

		include $full_path;
	}
}




