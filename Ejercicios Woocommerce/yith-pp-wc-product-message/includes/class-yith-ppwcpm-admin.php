<?php
/**
 *
 * Admin Main Class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPM_Admin' ) ) {
	/**
	 * YITH_PPWCPM_Admin
	 */
	class YITH_PPWCPM_Admin {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPM_Admin
		 */
		private static $instance;

		/**
		 * Purchase Notes options
		 *
		 * @var mixed
		 */
		private $ppwcn_panel_fields;

		/**
		 * Panel
		 *
		 * @var Panel page
		 */
		protected $panel_page = 'yith_ppwcpm_panel';


		/**
		 * Option Panel
		 *
		 * @var YIT_Plugin_Panel $_panel the panel
		 */
		private $_panel;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPM_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'plugin_action_links_' . plugin_basename( YITH_PPWCPM_DIR_PATH . '/' . basename( YITH_PPWCPM_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'yith-ppwcpm-product-list-title-field', array( $this, 'setup_custom_field_pm_title' ) );
			add_filter( 'yith_plugin_fw_add_new_post_url', array( $this, 'add_new_button_config_product_message' ) );
			add_action( 'yith_ppwcpm_back_product_list_button', array( $this, 'back_to_product_list_button' ) );
			add_action( 'wp_ajax_nopriv_save_action', array( $this, 'save_action_product_list' ) );
			add_action( 'wp_ajax_save_action', array( $this, 'save_action_product_list' ) );
			add_action( 'wp_ajax_nopriv_back_action', array( $this, 'back_action_product_list' ) );
			add_action( 'wp_ajax_back_action', array( $this, 'back_action_product_list' ) );
			add_action( 'wp_ajax_nopriv_apply_rule_action', array( $this, 'apply_rule_action_product_list' ) );
			add_action( 'wp_ajax_apply_rule_action', array( $this, 'apply_rule_action_product_list' ) );
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_PPWCPM_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_PPWCPM_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_PPWCPM_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

		/**
		 * Register_panel
		 *
		 * @return void
		 */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings' => __( 'Product Message', 'yith-wc-product-message' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Product Message', // this text MUST be NOT translatable.
				'menu_title'         => 'YITH Product Message', // this text MUST be NOT translatable.
				'plugin_description' => __( 'Put here your plugin description', 'yith-wc-product-message' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith_ppwc_product_message',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_ppwcpm_panel',
				'admin-tabs'         => $admin_tabs,
				'options-path'       => YITH_PPWCPM_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Setup_custom_field_pm_title
		 *
		 * @param  mixed $fields arguments of the field.
		 * @return void
		 */
		public function setup_custom_field_pm_title( $fields ) {
			yith_ppwcpm_get_view( '/admin/fields/ppm_title.php', $fields );
		}

		/**
		 * Add_new_button_config_product_message
		 *
		 * @param  mixed $params params of the url.
		 * @return string
		 */
		public function add_new_button_config_product_message( $params ) {
			return add_query_arg( 'edit', 'true' );

		}

		/**
		 * Back_to_product_list_button
		 *
		 * @return void
		 */
		public function back_to_product_list_button() {
			wp_safe_redirect( remove_query_arg( 'edit' ) );
			exit;
		}

		/**
		 * Save_action_product_list
		 *
		 * @return void
		 */
		public function save_action_product_list() {
			$values = $_POST['values'];

			if ( isset( $values['search_product'] ) ) {
				if ( isset( $values['rule_onoff'] ) ) {
					$confirmed_values['apply_rule_onoff'] = $values['rule_onoff'];
				}
				if ( isset( $values['product_message'] ) ) {
					$confirmed_values['product_message'] = $values['product_message'];
				}
				if ( isset( $values['placement_select'] ) ) {
					$confirmed_values['message_placement_select'] = $values['placement_select'];
				}

				if ( isset( $values['price_onoff'] ) ) {
					$confirmed_values['hide_price_onoff'] = $values['price_onoff'];
				}
				if ( null !== $confirmed_values ) {
					update_post_meta(
						$values['search_product'],
						'_yith_ppwcpm_values',
						$confirmed_values
					);
				}
			}

			wp_send_json( array( 'back_to_list' => remove_query_arg( 'edit', $_SERVER['HTTP_REFERER'] ) ) );
			wp_die();
		}

		/**
		 * Back_action_product_list
		 *
		 * @return void
		 */
		public function back_action_product_list() {
			wp_send_json( array( 'back_to_list' => remove_query_arg( 'edit', $_SERVER['HTTP_REFERER'] ) ) );
			wp_die();
		}

		/**
		 * Apply_rule_action_product_list
		 *
		 * @return void
		 */
		public function apply_rule_action_product_list() {
			$values                            = $_POST['values'];
			$original_data                     = get_post_meta( $values['id'], '_yith_ppwcpm_values', true );
			$original_data['apply_rule_onoff'] = $values['value'];
			update_post_meta( $values['id'], '_yith_ppwcpm_values', $original_data );
			wp_die();
		}

		/**
		 * Hide_price_ppwcpm_product
		 *
		 * @param  mixed $price
		 * @param  mixed $product
		 * @return void
		 */


		/**
		 * Enqueue_admin_styles
		 *
		 * @return void
		 */
		public function enqueue_admin_styles() {
			wp_enqueue_style( 'yith_ppwcpm_admin_stylesheet', YITH_PPWCPM_DIR_ASSETS_CSS_URL . '/yith-ppwcpm-admin-stylesheet.css', array(), '1.0.0' );
			wp_enqueue_script( 'yith_ppwcpm_admin_script', YITH_PPWCPM_DIR_ASSETS_JS_URL . '/yith-ppwcpm-admin-script.js', array( 'jquery' ), '1.0.0', true );
			wp_localize_script( 'yith_ppwcpm_admin_script', 'yith_ppwcpm_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		}

	}
}
