<?php
/**
 * Plugin Name: YITH WooCommerce Product Message and Hidden Price
 * Description: Adds a message to the product with a variaty of options for placement and hides the price of the product.
 * Version: 1.0.0
 * Author: Pablo Pérez Arteaga
 * Author URI: https://yithemes.com/
 * Text Domain: yith-wc-product-message
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PPWCPM_VERSION' ) ) {
	define( 'YITH_PPWCPM_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PPWCPM_DIR_URL' ) ) {
	define( 'YITH_PPWCPM_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCPM_FILE' ) ) {
	define( 'YITH_PPWCPM_FILE', __FILE__ );
}

if ( ! defined( 'YITH_PPWCPM_SLUG' ) ) {
	define( 'YITH_PPWCPM_SLUG', 'yith-pp-wcnotes' );
}

if ( ! defined( 'YITH_PPWCPM_INIT' ) ) {
	define( 'YITH_PPWCPM_INIT', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCPM_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PPWCPM_DIR_ASSETS_URL', YITH_PPWCPM_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PPWCPM_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PPWCPM_DIR_ASSETS_CSS_URL', YITH_PPWCPM_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PPWCPM_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PPWCPM_DIR_ASSETS_JS_URL', YITH_PPWCPM_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PPWCPM_DIR_PATH' ) ) {
	define( 'YITH_PPWCPM_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCPM_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PPWCPM_DIR_INCLUDES_PATH', YITH_PPWCPM_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PPWCPM_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PPWCPM_DIR_TEMPLATES_PATH', YITH_PPWCPM_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PPWCPM_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PPWCPM_DIR_VIEWS_PATH', YITH_PPWCPM_DIR_PATH . 'views' );
}

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_PPWCPM_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_PPWCPM_DIR_PATH . 'plugin-fw/init.php';
}

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}


yit_maybe_plugin_fw_loader( YITH_PPWCPM_DIR_PATH );
register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_ppwcn_init_classes' ) ) {

	/**
	 * Yith_ppwcn_init_classes
	 *
	 * @return void
	 */
	function yith_ppwcn_init_classes() {

		load_plugin_textdomain( 'yith-wc-product-message', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PPWCPM_DIR_INCLUDES_PATH . '/class-yith-ppwcpm-plugin-skeleton.php';

		if ( class_exists( 'YITH_PPWCPM_Plugin_Skeleton' ) ) {
			/*
			*	Call the main function
			*/
			yith_ppwcpm_plugin_skeleton();
		}
	}
}


add_action( 'plugins_loaded', 'yith_ppwcn_init_classes', 11 );
