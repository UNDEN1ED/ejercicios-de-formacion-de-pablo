<?php
if ( isset( $_GET['edit'] ) ) {
	$edit_value = $_GET['edit'];
	error_log( print_r( get_post_meta( intval( $edit_value ), '_yith_ppwcpm_values', true ), true ) );
	$settings = array(
		'settings' => array(
			'general-options'                      => array(
				'title' => __( 'General Options', 'yith-wc-product-message' ),
				'type'  => 'title',
				'desc'  => '',
			),
			'yith_ppwcpm_apply_rules_onoff'        => array(
				'id'        => 'yith_ppwcpm_apply_rules_onoff',
				'name'      => __( 'Apply this rule', 'yith-wc-product-message' ),
				'type'      => 'yith-field',
				'yith-type' => 'onoff',
				'default'   => ( 'true' !== $edit_value ) ? get_post_meta( intval( $edit_value ), '_yith_ppwcpm_values', true )['apply_rule_onoff'] : null,
			),
			'yith_ppwcpm_search_product'           => array(
				'id'        => 'yith_ppwcpm_search_product',
				'name'      => __( 'Select products', 'yith-wc-product-message' ),
				'type'      => 'yith-field',
				'yith-type' => 'ajax-products',
				'default'   => ( 'true' !== $edit_value ) ? intval( $edit_value ) : null,
			),
			'yith_ppwcpm_textarea-editor'          => array(
				'id'        => 'yith_ppwcpm_product_message',
				'name'      => __( 'Message', 'yith-wc-product-message' ),
				'type'      => 'yith-field',
				'yith-type' => 'textarea',
				'default'   => ( 'true' !== $edit_value ) ? get_post_meta( intval( $edit_value ), '_yith_ppwcpm_values', true )['product_message'] : null,
			),
			'yith_ppwcpm_message_placements_select' => array(
				'id'        => 'yith_ppwcpm_message_placements_select',
				'name'      => __( 'Where to display the message on product page', 'yith-wc-product-message' ),
				'type'      => 'yith-field',
				'yith-type' => 'select',
				'options'   => array(
					'before_title'       => 'Before title',
					'after_title'        => 'After title',
					'before_price'       => 'Before price',
					'after_price'        => 'After price',
					'before_add_to_cart' => 'Before add to cart',
					'after_add_to_cart'  => 'After add to cart',
				),
				'default'   => ( 'true' !== $edit_value ) ? get_post_meta( intval( $edit_value ), '_yith_ppwcpm_values', true )['message_placement_select'] : 'before-title',
			),
			'yith_ppwcpm_hide_price_onoff'         => array(
				'id'        => 'yith_ppwcpm_hide_price_onoff',
				'name'      => __( 'Hide product price', 'yith-wc-product-message' ),
				'type'      => 'yith-field',
				'yith-type' => 'onoff',
				'default'   => ( 'true' !== $edit_value ) ? get_post_meta( intval( $edit_value ), '_yith_ppwcpm_values', true )['hide_price_onoff'] : null,
			),
			'yith_ppwcpm_options_message_buttons'  => array(
				'type'      => 'yith-field',
				'yith-type' => 'buttons',
				'buttons'   => array(
					array(
						'name'  => 'Back to product list ',
						'class' => 'button-secondary yith_ppwcpm_back_product_list_button',
					),
					array(
						'name'  => 'Save products',
						'class' => 'button-primary yith_ppwcpm_save_messsage_button',
					),
				),
			),
			'section-options-end'                  => array(
				'type' => 'sectionend',
			),
		),
	);
} else {
	$settings = array(
		'settings' => array(
			'general-options'          => array(
				'title' => __( 'General Options', 'yith-wc-product-message' ),
				'type'  => 'title',
				'desc'  => '',
			),
			'yith-ppwcpm-product-list' => array(
				'id'                   => 'yith-ppwcpm-product-list',
				'type'                 => 'yith-field',
				'post_type'            => 'simple',
				'yith-type'            => 'list-table',
				'list_table_class'     => 'TT_Example_List_Table',
				'list_table_class_dir' => YITH_PPWCPM_DIR_PATH . 'includes/class-yith-table.php',
				'title'                => 'Product Message List',
				'add_new_button'       => 'Add New',
			),
			/*
			'yith_ppwcpm_badge_position_in_product' => array(
				'id'        => 'yith_ppwcpm_badge_position_in_product',
				'name'      => __( 'Badge Position in Shop', 'yith-wc-product-message' ),
				'type'      => 'yith-field',
				'yith-type' => 'radio',
				'id' => 'my_id',
				'type' => 'ajax-posts',
				'data'     => array(
					'placeholder' => __('Search Pages', 'text-domain'),
					'post_type'   => 'page',
				),
			),
			*/
			'section-options-end'      => array(
				'type' => 'sectionend',
			),
		),
	);
}



return apply_filters( 'yith_ppwcpm_settings_options', $settings );
