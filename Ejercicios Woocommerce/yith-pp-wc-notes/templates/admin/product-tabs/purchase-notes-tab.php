<?php
/**
 *
 * Product Tab Purchase Notes Options
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

$auction_product = wc_get_product( $post );
$id_post         = '';

if ( $auction_product instanceof WC_Product ) {
	$id_post = $auction_product->get_id();

} else {
	return;
}
?>
<div>
	<h3 class="yith_ppwcn_title_settings"><?php esc_html_e( 'Purchase Notes Settings' ); ?> </h3>
</div>

<?php

do_action( 'yith_before_purchase_notes_tab', $id_post );
