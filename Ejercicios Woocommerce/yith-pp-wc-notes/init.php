<?php
/**
 * Plugin Name: YITH WooCommerce Notes
 * Description: Add personalization option to the product with engravings
 * Version: 1.0.0
 * Author: Pablo Pérez Arteaga
 * Author URI: https://yithemes.com/
 * Text Domain: yith-pp-wcnotes
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PPWCN_VERSION' ) ) {
	define( 'YITH_PPWCN_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PPWCN_DIR_URL' ) ) {
	define( 'YITH_PPWCN_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCN_FILE' ) ) {
	define( 'YITH_PPWCN_FILE', __FILE__ );
}

if ( ! defined( 'YITH_PPWCN_SLUG' ) ) {
	define( 'YITH_PPWCN_SLUG', 'yith-pp-wcnotes' );
}

if ( ! defined( 'YITH_PPWCN_INIT' ) ) {
	define( 'YITH_PPWCN_INIT', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCN_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PPWCN_DIR_ASSETS_URL', YITH_PPWCN_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PPWCN_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PPWCN_DIR_ASSETS_CSS_URL', YITH_PPWCN_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PPWCN_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PPWCN_DIR_ASSETS_JS_URL', YITH_PPWCN_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PPWCN_DIR_PATH' ) ) {
	define( 'YITH_PPWCN_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCN_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PPWCN_DIR_INCLUDES_PATH', YITH_PPWCN_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PPWCN_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PPWCN_DIR_TEMPLATES_PATH', YITH_PPWCN_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PPWCN_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PPWCN_DIR_VIEWS_PATH', YITH_PPWCN_DIR_PATH . 'views' );
}

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_PPWCN_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_PPWCN_DIR_PATH . 'plugin-fw/init.php';
}

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}


yit_maybe_plugin_fw_loader( YITH_PPWCN_DIR_PATH );
register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_ppwcn_init_classes' ) ) {

	/**
	 * Yith_ppwcn_init_classes
	 *
	 * @return void
	 */
	function yith_ppwcn_init_classes() {

		load_plugin_textdomain( 'yith-pp-wcnotes', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PPWCN_DIR_INCLUDES_PATH . '/class-yith-ppwcn-plugin-skeleton.php';

		if ( class_exists( 'YITH_PPWCN_Plugin_Skeleton' ) ) {
			/*
			*	Call the main function
			*/
			yith_ppwcn_plugin_skeleton();
		}
	}
}


add_action( 'plugins_loaded', 'yith_ppwcn_init_classes', 11 );
