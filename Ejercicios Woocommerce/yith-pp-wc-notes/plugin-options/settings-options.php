<?php
$settings = array(
	'settings' => array(
		'general-options'                      => array(
			'title' => __( 'General Options', 'yith-pp-wcnotes' ),
			'type'  => 'title',
			'desc'  => '',
		),
		'yith-ppwcn-padding'                   => array(
			'id'        => 'yith-ppwcn-padding',
			'name'      => __( 'Padding', 'yith-pp-wcnotes' ),
			'type'      => 'yith-field',
			'yith-type' => 'dimensions',
			'default'   => array(
				'dimensions' => array(
					'top'    => 20,
					'right'  => 25,
					'bottom' => 25,
					'left'   => 25,
				),
				'units'      => 'px',
				'linked'     => 'no',
			),
			'units'     => array(
				'px' => 'PX',
			),
			'desc'      => __( 'Padding that the container of the purchase notes will have.', 'yith-pp-wcnotes' ),
		),
		'yith-ppwcn-border'                    => array(
			'id'        => 'yith-ppwcn-border',
			'name'      => __( 'Border', 'yith-pp-wcnotes' ),
			'type'      => 'yith-field',
			'yith-type' => 'custom',
			'action'    => 'yith-ppwcn-border_custom_field',
			'desc'      => __( 'Border style of the purchase notes container.', 'yith-pp-wcnotes' ),
		),
		'yith_ppwcn_badge_position_in_shop'    => array(
			'id'        => 'yith_ppwcn_badge_position_in_shop',
			'name'      => __( 'Badge Position in Product', 'yith-pp-wcnotes' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'options'   => array(
				'top-left'  => 'Top-left',
				'top-right' => 'Top-right',
			),
		),
		'yith_ppwcn_badge_position_in_product' => array(
			'id'        => 'yith_ppwcn_badge_position_in_product',
			'name'      => __( 'Badge Position in Shop', 'yith-pp-wcnotes' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'options'   => array(
				'top-left'  => 'Top-left',
				'top-right' => 'Top-right',
			),
		),

		'section-2-options-end'                => array(
			'type' => 'sectionend',
		),
	),
);

return apply_filters( 'yith_ppwcn_settings_options', $settings );
