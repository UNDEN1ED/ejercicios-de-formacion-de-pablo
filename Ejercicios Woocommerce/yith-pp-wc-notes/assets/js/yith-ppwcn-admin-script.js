var badge_elements = document.getElementsByClassName('yith-ppwcn-field yith_ppwcn_badge_option');
var price_elements = document.getElementsByClassName('yith_ppwcn_price_stg');
function price_type_check() {
    if (jQuery('#yith_ppwcn_price_type input:checked').val() == 'free_radio') {
        jQuery('.show-yith_ppwcn_price_type').addClass('hide-yith_ppwcn_price_type');
        jQuery('.show-yith_ppwcn_price_type').removeClass('show-yith_ppwcn_price_type');
    } else {
        jQuery('.hide-yith_ppwcn_price_type').addClass('show-yith_ppwcn_price_type');
        jQuery('.hide-yith_ppwcn_price_type').removeClass('hide-yith_ppwcn_price_type');
    }
}
function note_enable_check() {
    if (jQuery('#yith_ppwcn_is_enabled').prop('checked') == false) {
        jQuery('.show-yith_ppwcn_is_enabled').addClass('hide-yith_ppwcn_is_enabled');
        jQuery('.show-yith_ppwcn_is_enabled').removeClass('show-yith_ppwcn_is_enabled');
        jQuery('#yith_ppwcn_price_type-free_radio').prop('checked', true);
        jQuery('#yith_ppwcn_badge_enable').prop('checked', false);
        price_type_check();
        badge_enable_check();
    } else {
        jQuery('.hide-yith_ppwcn_is_enabled').addClass('show-yith_ppwcn_is_enabled');
        jQuery('.hide-yith_ppwcn_is_enabled').removeClass('hide-yith_ppwcn_is_enabled');
    }

}

function badge_enable_check() {
    if (jQuery('#yith_ppwcn_badge_enable').prop('checked') == false) {
        jQuery('.show-yith_ppwcn_badge_enable').addClass('hide-yith_ppwcn_badge_enable');
        jQuery('.show-yith_ppwcn_badge_enable').removeClass('show-yith_ppwcn_badge_enable');
    } else {
        jQuery('.hide-yith_ppwcn_badge_enable').addClass('show-yith_ppwcn_badge_enable');
        jQuery('.hide-yith_ppwcn_badge_enable').removeClass('hide-yith_ppwcn_badge_enable');
    }

}

jQuery(document).ready(function ($) {
    price_type_check();
    note_enable_check();
    badge_enable_check();

    $('#yith_ppwcn_is_enabled').on('click', function () {
        note_enable_check();
    });
    $('#yith_ppwcn_badge_enable').on('click', function () {
        badge_enable_check();
    });
    $('#yith_ppwcn_price_type input').on('click', function () {
        price_type_check();
    });

});

function display_price_options() {
    var elements = document.getElementsByClassName('yith_ppwcn_price_stg');
    switch (document.querySelector('input[name="_yith_ppwcn_price_stg"]:checked').value) {
        case "free":
            elements[0].style.display = 'none';
            elements[1].style.display = 'none';
            break;
        case "per-character":
            elements[0].style.display = 'block';
            elements[1].style.display = 'block';
            break;
        case "fixed-price":
            elements[0].style.display = 'block';
            elements[1].style.display = 'block';
            break;
    }
}


function display_badge_options() {

    if (document.getElementById('yith_ppwcn_show_badge').checked) {
        badge_elements[0].style.display = 'block';
        badge_elements[1].style.display = 'block';
        badge_elements[2].style.display = 'block';

    } else {
        badge_elements[0].style.display = 'none';
        badge_elements[1].style.display = 'none';
        badge_elements[2].style.display = 'none';

    }
}

function enabled_pn_options() {

    if (document.getElementById('yith_ppwcn_is_enabled').checked) {
        document.getElementById("yith_ppwcn_note_label").disabled = false;
        document.getElementById("yith_ppwcn_note_desc").disabled = false;
        document.getElementById("yith_ppwcn_field_type_text").disabled = false;
        document.getElementById("yith_ppwcn_field_type_text-area").disabled = false;
        document.getElementById("yith_ppwcn_price_stg_free").disabled = false;
        document.getElementById("yith_ppwcn_price_stg_per-character").disabled = false;
        document.getElementById("yith_ppwcn_price_stg_fixed-price").disabled = false;
        document.getElementById("yith_ppwcn_show_badge").disabled = false;

    } else {
        document.getElementById("yith_ppwcn_note_label").disabled = true;
        document.getElementById("yith_ppwcn_note_desc").disabled = true;
        document.getElementById("yith_ppwcn_field_type_text").disabled = true;
        document.getElementById("yith_ppwcn_field_type_text-area").disabled = true;
        document.getElementById("yith_ppwcn_price_stg_free").disabled = true;
        document.getElementById("yith_ppwcn_price_stg_free").checked = true;
        document.getElementById("yith_ppwcn_price_stg_per-character").disabled = true;
        document.getElementById("yith_ppwcn_price_stg_fixed-price").disabled = true;
        document.getElementById("yith_ppwcn_show_badge").disabled = true;
        document.getElementById("yith_ppwcn_show_badge").checked = false;
        display_badge_options();
        display_price_options();

    }
}
document.addEventListener('DOMContentLoaded', function () {

    if (document.getElementById("yith_ppwcn_is_enabled").checked) {
        document.getElementById("yith_ppwcn_note_label").disabled = false;
        document.getElementById("yith_ppwcn_note_desc").disabled = false;
        document.getElementById("yith_ppwcn_field_type_text").disabled = false;
        document.getElementById("yith_ppwcn_field_type_text-area").disabled = false;
        document.getElementById("yith_ppwcn_price_stg_free").disabled = false;
        document.getElementById("yith_ppwcn_price_stg_per-character").disabled = false;
        document.getElementById("yith_ppwcn_price_stg_fixed-price").disabled = false;
        document.getElementById("yith_ppwcn_show_badge").disabled = false;


    } else {
        document.getElementById("yith_ppwcn_note_label").disabled = true;
        document.getElementById("yith_ppwcn_note_desc").disabled = true;
        document.getElementById("yith_ppwcn_field_type_text").disabled = true;
        document.getElementById("yith_ppwcn_field_type_text-area").disabled = true;
        document.getElementById("yith_ppwcn_price_stg_free").disabled = true;
        document.getElementById("yith_ppwcn_price_stg_free").checked = true;
        document.getElementById("yith_ppwcn_price_stg_per-character").disabled = true;
        document.getElementById("yith_ppwcn_price_stg_fixed-price").disabled = true;
        document.getElementById("yith_ppwcn_show_badge").checked = false;
        document.getElementById("yith_ppwcn_show_badge").disabled = true;


    }

    if (document.getElementById("yith_ppwcn_show_badge").checked) {
        badge_elements[0].style.display = 'block';
        badge_elements[1].style.display = 'block';
        badge_elements[2].style.display = 'block';
    }
    if (document.querySelector('input[name="_yith_ppwcn_price_stg"]:checked').value == 'per-character') {
        price_elements[0].style.display = 'block';
        price_elements[1].style.display = 'block';
    } else if (document.querySelector('input[name="_yith_ppwcn_price_stg"]:checked').value == 'fixed-price') {
        price_elements[0].style.display = 'block';
        price_elements[1].style.display = 'block';
    }




}, false);