function change_price() {
    var price = parseFloat(price_modifier);
    var note_input = document.getElementById('yith_ppwcn_note_text').value;
    var final_price = 0;
    var chars_discount = parseFloat(free_chars);

    if ('free_radio' != price_stgs) {
        if (0 != note_input.length) {

            if ('per_character_radio' == price_stgs) {

                final_price = (parseFloat(note_input.length) - chars_discount) * price;
            } else {
                if (parseFloat(note_input.length) > chars_discount) {
                    final_price = price;
                }

            }
        }
        if (final_price > 0) {

            document.getElementById('yith_ppwcn_price_label').innerHTML = '+ ' + final_price.toFixed(2);
            document.getElementById('yith_ppwcn_price_label').style.color = "green";
        } else {
            document.getElementById('yith_ppwcn_price_label').innerHTML = '' + parseFloat(0).toFixed(2);
            document.getElementById('yith_ppwcn_price_label').style.color = "";

        }
        document.getElementById('yih_ppwcn_hidden_price').value = "" + final_price.toFixed(2);

    }
}
document.addEventListener('DOMContentLoaded', function() {
    if ('free_radio' == price_stgs) {
        document.getElementById('yith_ppwcn_price_label').innerHTML = free_label;
        document.getElementById('yith_ppwcn_price_currency').innerHTML = '';
        document.getElementById('yith_ppwcn_price_label').style.color = "green";

    }
}, false);