<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCN_Printer' ) ) {
	/**
	 * YITH_PPWCN_Printer
	 */
	class YITH_PPWCN_Printer {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCN_Printer
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCN_Printer
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
		}


		/**
		 * Print_input_field
		 *
		 * @param  mixed $field_data params of the input.
		 * @param  mixed $post  $_POST.
		 * @return void
		 */
		public function print_input_field( $field_data, $post = '' ) {
			$default    = array(
				'label'           => '',
				'name'            => '',
				'id'              => '',
				'type'            => 'text',
				'container_class' => '',
				'container_id'    => '',
				'label_class'     => '',
				'input_class'     => '',
				'options'         => array(),
			);
			$field_data = array_merge( $default, $field_data );
			if ( array_key_exists( 'type', $field_data ) ) {
				yith_ppwcn_get_view( '/input-fields/' . $field_data['type'] . '.php', $field_data, $post );
			}
		}

		/**
		 * Print_input_fields
		 *
		 * @param  mixed $fields_data params of the input.
		 * @param  mixed $post  $_POST.
		 * @return void
		 */
		public function print_input_fields( $fields_data, $post = '' ) {
			foreach ( $fields_data as $field_data ) {
				$this->print_input_field( $field_data, $post );
			}

		}
	}
}


if ( ! function_exists( 'yith_ppwcn_printer' ) ) {

	/**
	 * Yith_ppwcn_printer
	 *
	 * @return YITH_PPWCN_Printer
	 */
	function yith_ppwcn_printer() {
		return YITH_PPWCN_Printer::get_instance();

	}
}
