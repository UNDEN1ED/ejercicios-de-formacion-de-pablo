<?php
/**
 *
 * Common Functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_ppwcn_get_view' ) ) {

	/**
	 * Yith_ppwcn_get_view
	 *
	 * @param  mixed $file_name name of the file.
	 * @param  mixed $args array with the details.
	 * @param  mixed $post desired post.
	 * @return void
	 */
	function yith_ppwcn_get_view( $file_name, $args = array(), $post = '' ) {
		$full_path = YITH_PPWCN_DIR_VIEWS_PATH . $file_name;

		include $full_path;
	}
}

if ( ! function_exists( 'yith_ppwcn_product_meta_form_field' ) ) {

	/**
	 * Yith_ppwcn_product_meta_form_field
	 *
	 * @param  mixed $field arguements of the field.
	 * @return void
	 */
	function yith_ppwcn_product_meta_form_field( $field ) {
		$defaults = array(
			'class'     => '',
			'title'     => '',
			'label_for' => '',
			'desc'      => '',
			'data'      => array(),
			'fields'    => array(),
		);
		$field    = apply_filters( 'yith_ppwcn_product_metabox_form_field_args', wp_parse_args( $field, $defaults ), $field );
		foreach ( $field as $key => $val ) {
			${$key} = $val;
		}

		if ( ! $label_for && $fields ) {
			$first_field = current( $fields );
			if ( isset( $first_field['id'] ) ) {
				$label_for = $first_field['id'];
			}
		}

		$data_html = '';
		foreach ( $data as $key => $value ) {
			$data_html .= "data-{$key}='{$value}' ";
		}

		$html  = '';
		$html .= "<div class='yith-ppwcn-form-field {$class} " . ( isset( $deps ) && 'hide' === $deps['action'] ? $deps['action'] . '-' . $deps['id'] : '' ) . "' {$data_html}>";
		$html .= "<label class='yith-ppwcn-form-field__label' for='{$label_for}'>{$title}</label>";
		$html .= "<div class='yith-ppwcn-form-field__container'>";
		ob_start();
		foreach ( $fields as $fields_item ) {
			yith_plugin_fw_get_field( $fields_item, true ); // Print field using plugin-fw.
		}
		$html .= ob_get_clean();
		$html .= '</div>';
		if ( $desc ) {
			$html .= "<div class='yith-ppwcn-form-field__description'>{$desc}</div>";
		}

		$html .= '</div>';
		echo apply_filters( 'yith_ppwcn_product_metabox_form_field_html', $html, $field );
	}
}

if ( ! function_exists( 'yith_ppwcn_product_meta_form_field' ) ) {

	/**
	 * Yith_ppwcn_product_meta_form_field
	 *
	 * @param  mixed $field arguements of the field.
	 * @return void
	 */
	function yith_ppwcn_product_meta_deps_field( $field ) {
	}
}
