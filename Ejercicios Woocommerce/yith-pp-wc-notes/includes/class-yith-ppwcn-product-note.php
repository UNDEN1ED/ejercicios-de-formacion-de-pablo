<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCN_Product_Note' ) ) {
	/**
	 * YITH_PPWCN_Product_Note
	 */
	class YITH_PPWCN_Product_Note {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCN_Product_Note
		 */
		private static $instance;

		/**
		 * Price
		 *
		 * @var int
		 */
		private $price = 0;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCN_Product_Note
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'woocommerce_before_add_to_cart_quantity', array( $this, 'product_notes_setup' ) );
			add_action( 'woocommerce_before_single_product_summary', array( $this, 'add_badge_in_product' ) );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_note_to_cart' ), 10, 3 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'add_note_price_to_product' ) );
			add_filter( 'woocommerce_get_item_data', array( $this, 'display_product_notes_cart' ), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'save_cart_item_product_note_as_order_item_meta' ), 10, 4 );
			add_action( 'woocommerce_shop_loop_item_title', array( $this, 'add_badge_in_shop' ) );
		}

		/**
		 * Product_notes_setup
		 *
		 * @return void
		 */
		public function product_notes_setup() {

			$product_note_args = array();
			global $post;

			$product_note_args['enabled']        = get_post_meta( $post->ID, '_yith_ppwcn_is_enabled', true );
			$product_note_args['label']          = get_post_meta( $post->ID, '_yith_ppwcn_note_label', true );
			$product_note_args['description']    = get_post_meta( $post->ID, '_yith_ppwcn_note_desc', true );
			$product_note_args['field_type']     = get_post_meta( $post->ID, '_yith_ppwcn_field_type', true );
			$product_note_args['price_settings'] = get_post_meta( $post->ID, '_yith_ppwcn_price_stg', true );
			$product_note_args['price']          = ! empty( get_post_meta( $post->ID, '_yith_ppwcn_price', true ) ) ? get_post_meta( $post->ID, '_yith_ppwcn_price', true ) : '0';
			$product_note_args['free_char']      = ! empty( get_post_meta( $post->ID, '_yith_ppwcn_free_char', true ) ) ? get_post_meta( $post->ID, '_yith_ppwcn_free_char', true ) : '0';
			if ( '0' !== $product_note_args['price'] ) {
				$product_note_args['price'] = str_replace( ',', '.', $product_note_args['price'] );
			}
			wp_localize_script( 'yith_ppwcn_frontend_script', 'price_modifier', $product_note_args['price'] );
			wp_localize_script( 'yith_ppwcn_frontend_script', 'price_stgs', $product_note_args['price_settings'] );
			wp_localize_script( 'yith_ppwcn_frontend_script', 'free_chars', $product_note_args['free_char'] );
			wp_localize_script( 'yith_ppwcn_frontend_script', 'free_label', __( 'Free', 'yith-pp-wcnotes' ) );

			if ( '1' === $product_note_args['enabled'] ) {
				yith_ppwcn_get_view( '/frontend/product-notes.php', $product_note_args );
			}
		}

		/**
		 * Add_badge_in_product
		 *
		 * @return void
		 */
		public function add_badge_in_product() {
			global $post;
			if ( '1' === get_post_meta( $post->ID, '_yith_ppwcn_show_badge', true ) ) {
				$product_badge_args['enable']           = get_post_meta( $post->ID, '_yith_ppwcn_show_badge', true );
				$product_badge_args['text']             = get_post_meta( $post->ID, '_yith_ppwcn_badge_text', true );
				$product_badge_args['background-color'] = get_post_meta( $post->ID, '_yith_ppwcn_badge_background', true );
				$product_badge_args['text-color']       = get_post_meta( $post->ID, '_yith_ppwcn_badge_text_color', true );
				$product_badge_args['position']         = get_option( 'yith_ppwcn_badge_position_in_product', 'top-left' );
				$product_badge_args['input_class']      = 'onsale yith_ppwcn_note_badge_product';

				echo '<div class="yith_ppwcn_badge_product_container">';
				yith_ppwcn_get_view( '/frontend/product-notes-badge.php', $product_badge_args );
				echo '</div>';
			}

		}

		/**
		 * Add_badge_in_shop
		 *
		 * @return void
		 */
		public function add_badge_in_shop() {
			global $post;
			if ( '1' === get_post_meta( $post->ID, '_yith_ppwcn_show_badge', true ) ) {
				$product_badge_args['enable']           = get_post_meta( $post->ID, '_yith_ppwcn_show_badge', true );
				$product_badge_args['text']             = get_post_meta( $post->ID, '_yith_ppwcn_badge_text', true );
				$product_badge_args['background-color'] = get_post_meta( $post->ID, '_yith_ppwcn_badge_background', true );
				$product_badge_args['text-color']       = get_post_meta( $post->ID, '_yith_ppwcn_badge_text_color', true );
				$product_badge_args['position']         = get_option( 'yith_ppwcn_badge_position_in_shop', 'top-left' );
				$product_badge_args['input_class']      = 'yith_ppwcn_note_badge_shop';

				yith_ppwcn_get_view( '/frontend/product-notes-badge.php', $product_badge_args );
			}
		}

		/**
		 * Add_note_to_cart
		 *
		 * @param  mixed $cart_item_data Cart data.
		 * @param  mixed $product_id    ID of the product.
		 * @param  mixed $variation_id  ID of the variation.
		 * @return ARRAY
		 */
		public function add_note_to_cart( $cart_item_data, $product_id, $variation_id ) {
			if ( array_key_exists( '_yith_ppwcn_note_final_price', $_POST ) && array_key_exists( '_yith_ppwcn_note_product', $_POST ) ) {// phpcs:ignore
				$cart_item_data['yith_ppwcn_note_price'] = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_note_final_price'] ) );// phpcs:ignore 
				$cart_item_data['yith_ppwcn_note_text']  = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_note_product'] ) );// phpcs:ignore 

			}
			return $cart_item_data;
		}

		/**
		 * Display_product_notes_cart
		 *
		 * @param  mixed $item_data  Cart product data.
		 * @param  mixed $cart_item Cart Product.
		 * @return ARRAY
		 */
		public function display_product_notes_cart( $item_data, $cart_item ) {

			if ( isset( $cart_item['yith_ppwcn_note_price'] ) && isset( $cart_item['yith_ppwcn_note_text'] ) ) {
				$item_data[] = array(
					'key'   => 'Note',
					'value' => '\'' . $cart_item['yith_ppwcn_note_text'] . '\'',
				);
				$item_data[] = array(
					'key'   => 'Price',
					'value' => '+ ' . $cart_item['yith_ppwcn_note_price'] . ' ' . get_woocommerce_currency_symbol(),
				);

			}
			return $item_data;

		}

		/**
		 * Add_note_price_to_product
		 *
		 * @param  mixed $cart_object WC_CART.
		 * @return void
		 */
		public function add_note_price_to_product( $cart_object ) {
			$cart_items = $cart_object->get_cart();
			foreach ( $cart_items as $cart_item ) {
				if ( isset( $cart_item['yith_ppwcn_note_text'] ) && isset( $cart_item['yith_ppwcn_note_price'] ) ) {
					$cart_item['data']->set_price( $cart_item['data']->get_price() + $cart_item['yith_ppwcn_note_price'] );

				}
			}

		}

		/**
		 * Save_cart_item_custom_meta_as_order_item_meta
		 *
		 * @param  mixed $item Item.
		 * @param  mixed $cart_item_key Cart.
		 * @param  mixed $values Value.
		 * @param  mixed $order Order.
		 * @return void
		 */
		public function save_cart_item_product_note_as_order_item_meta( $item, $cart_item_key, $values, $order ) {
			if ( isset( $values['yith_ppwcn_note_text'] ) && isset( $values['yith_ppwcn_note_price'] ) ) {
				$item->update_meta_data( 'Note', $values['yith_ppwcn_note_text'] );
				$item->update_meta_data( 'Note price', $values['yith_ppwcn_note_price'] . ' ' . get_woocommerce_currency_symbol() );
			}
		}
	}
}
