<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCN_Admin' ) ) {
	/**
	 * YITH_PPWCN_Admin
	 */
	class YITH_PPWCN_Admin {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCN_Admin
		 */
		private static $instance;

		/**
		 * Purchase Notes options
		 *
		 * @var mixed
		 */
		private $ppwcn_panel_fields;

		/**
		 * Panel
		 *
		 * @var Panel page
		 */
		protected $panel_page = 'yith_ppwcn_panel';


		/**
		 * Option Panel
		 *
		 * @var YIT_Plugin_Panel $_panel the panel
		 */
		private $_panel;

		/**
		 * Option Panel
		 *
		 * @var ARRAY $panel_deps the panel
		 */
		private $panel_deps;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCN_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'woocommerce_product_data_tabs', array( $this, 'wc_purchase_notes_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_wc_purchase_notes_panel' ) );
			add_action( 'yith_before_purchase_notes_tab', array( $this, 'display_fields_purchase_notes' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_notes_meta' ) );
			add_action( 'admin_menu', array( $this, 'yith_ppwcn_create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'yith_ppwcn_register_settings' ) );
			add_action( 'yith-ppwcn-border_custom_field', array( $this, 'print_custom_field' ), 10, 1 );
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_PPWCN_DIR_PATH . '/' . basename( YITH_PPWCN_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			$this->initialize_ppwcn_panel_fields();

		}


		/**
		 * Initialize_ppwcn_panel_fields
		 *
		 * @return void
		 */
		private function initialize_ppwcn_panel_fields() {
			$this->ppwcn_panel_fields = array(
				array(
					'type'            => 'toggle',
					'label'           => __( 'Enabled', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_is_enabled',
					'name'            => '_yith_ppwcn_is_enabled',
					'container_class' => 'yith_ppwcn_is_enabled',
					'value'           => 'on',
					'onclick'         => 'enabled_pn_options();',
				),
				array(
					'type'            => 'text',
					'label'           => __( 'Note label', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_note_label',
					'name'            => '_yith_ppwcn_note_label',
					'container_class' => 'yith_ppwcn_note_label',
					'value'           => 'Note',

				),
				array(
					'type'            => 'text-area',
					'label'           => __( 'Note description', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_note_desc',
					'name'            => '_yith_ppwcn_note_desc',
					'container_class' => 'yith_ppwcn_note_desc',

				),
				array(
					'type'            => 'radio',
					'label'           => __( 'Field type', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_field_type',
					'name'            => '_yith_ppwcn_field_type',
					'container_class' => 'yith_ppwcn_field_type',
					'value'           => 'text',
					'radio_values'    => array( 'text', 'text-area' ),

				),
				array(
					'type'          => 'radio',
					'label'         => __( 'Price settings', 'yith-pp-wcnotes' ),
					'id'            => 'yith_ppwcn_price_stg',
					'name'          => '_yith_ppwcn_price_stg',
					'value'         => 'free',
					'radio_values'  => array( 'free', 'per-character', 'fixed-price' ),
					'radio_onclick' => 'display_price_options();',

				),
				array(
					'type'            => 'number',
					'label'           => __( 'Price', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_price',
					'name'            => '_yith_ppwcn_price',
					'with_container'  => 'yes',
					'container_class' => 'yith_ppwcn_price_stg',
					'min'             => 0,

				),
				array(
					'type'            => 'number',
					'label'           => __( 'Free characters', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_free_char',
					'name'            => '_yith_ppwcn_free_char',
					'with_container'  => 'yes',
					'container_class' => 'yith_ppwcn_price_stg',
					'min'             => 0,

				),
				array(
					'type'    => 'toggle',
					'label'   => __( 'Show Badge', 'yith-pp-wcnotes' ),
					'id'      => 'yith_ppwcn_show_badge',
					'name'    => '_yith_ppwcn_show_badge',
					'value'   => 'on',
					'onclick' => 'display_badge_options();',
				),
				array(
					'type'            => 'text',
					'label'           => __( 'Badge Text', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_badge_text',
					'name'            => '_yith_ppwcn_badge_text',
					'with_container'  => 'yes',
					'container_class' => 'yith_ppwcn_badge_option',

				),
				array(
					'type'            => 'colorpicker',
					'label'           => __( 'Badge Background Color', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_badge_background',
					'name'            => '_yith_ppwcn_badge_background',
					'with_container'  => 'yes',
					'container_class' => 'yith_ppwcn_badge_option',
					'default'         => '#007694',
					'value'           => '#007694',

				),
				array(
					'type'            => 'colorpicker',
					'label'           => __( 'Badge Text Color', 'yith-pp-wcnotes' ),
					'id'              => 'yith_ppwcn_badge_text_color',
					'name'            => '_yith_ppwcn_badge_text_color',
					'with_container'  => 'yes',
					'container_class' => 'yith_ppwcn_badge_option',
					'default'         => '#ffffff',
					'value'           => '#ffffff',

				),
			);
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_PPWCN_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_PPWCN_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_PPWCN_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}


		/**
		 * Wc_purchase_notes_tab
		 *
		 * @param  mixed $tabs re.
		 * @return ARRAY
		 */
		public function wc_purchase_notes_tab( $tabs ) {
			// Adds the new tab.
			$tabs['purchase_notes'] = array(
				'label'    => __( 'Purchase Note', 'yith-pp-wcnotes' ),
				'target'   => 'yith_purchase_notes_settings',
				'class'    => array( 'show_if_simple', 'show_if_variable' ),
				'priority' => 80,
			);

			return $tabs;
		}
		/**
		 * Display_wc_purchase_notes_fields
		 *
		 * @return void
		 */
		public function display_wc_purchase_notes_panel() {
			global $post;
			$tabs = array(
				'purchase-notes' => 'yith_purchase_notes_settings',
			);
			foreach ( $tabs as $key => $tab_id ) {
				echo "<div id='" . esc_html( $tab_id ) . "' class='panel woocommerce_options_panel'>";
				include YITH_PPWCN_DIR_TEMPLATES_PATH . '/admin/product-tabs/' . $key . '-tab.php';
				echo '</div>';
			}
		}

		/**
		 * Display_fields_purchase_notes
		 *
		 * @param  mixed $post_id ID of post.
		 * @return void
		 */
		public function display_fields_purchase_notes( $post_id ) {
			$product = wc_get_product( $post_id );
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Enable Note' ),
					'desc'   => esc_html__( 'Enables the note for the product' ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-enable',
							'type'  => 'checkbox',
							'id'    => 'yith_ppwcn_is_enabled',
							'name'  => '_yith_ppwcn_is_enabled',
							'value' => get_post_meta( $post_id, '_yith_ppwcn_is_enabled', true ),
						),
					),

				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Note Name' ),
					'desc'   => esc_html__( 'Especify the name of the note' ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-text',
							'type'  => 'text',
							'id'    => 'yith_ppwcn_name_text',
							'name'  => '_yith_ppwcn_note_label',
							'value' => get_post_meta( $post_id, '_yith_ppwcn_note_label', true ),

						),
					),
					'deps'   => array(
						'id'     => 'yith_ppwcn_is_enabled',
						'value'  => 'yes',
						'action' => 'hide',
					),

				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Note Description' ),
					'desc'   => esc_html__( 'Write a description of the note' ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-textarea',
							'type'  => 'textarea',
							'id'    => 'yith_ppwcn_desc_text',
							'name'  => '_yith_ppwcn_note_desc',
							'value' => get_post_meta( $post_id, '_yith_ppwcn_note_desc', true ),
						),
					),
					'deps'   => array(
						'id'     => 'yith_ppwcn_is_enabled',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Field Type' ),
					'desc'   => esc_html__( 'Especify the type of the note field' ),
					'fields' => array(
						array(
							'class'   => 'ppwcn-product-metabox-radio',
							'type'    => 'radio',
							'id'      => 'yith_ppwcn_field_type',
							'name'    => '_yith_ppwcn_field_type',
							'options' => array(
								'text_radio'     => 'Text',
								'textarea_radio' => 'Text-Area',
							),
							'value'   => ( get_post_meta( $post_id, '_yith_ppwcn_field_type', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_field_type', true ) : 'text_radio',

						),
					),
					'deps'   => array(
						'id'     => 'yith_ppwcn_is_enabled',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Price Type' ),
					'desc'   => esc_html__( 'Especify which price type will cost the note' ),
					'fields' => array(
						array(
							'class'   => 'ppwcn-product-metabox-radio',
							'type'    => 'radio',
							'id'      => 'yith_ppwcn_price_type',
							'name'    => '_yith_ppwcn_price_stg',
							'options' => array(
								'free_radio'          => 'Free',
								'per_character_radio' => 'Per-character',
								'fixed_price_radio'   => 'Fixed-price',
							),
							'value'   => ( get_post_meta( $post_id, '_yith_ppwcn_price_stg', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_price_stg', true ) : 'free_radio',
						),
					),
					'deps'   => array(
						'id'     => 'yith_ppwcn_is_enabled',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Price' ),
					'desc'   => esc_html__( 'Write the price modifier' ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-number',
							'type'  => 'number',
							'id'    => 'yith_ppwcn_price_text',
							'name'  => '_yith_ppwcn_price',
							'value' => ( get_post_meta( $post_id, '_yith_ppwcn_price', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_price', true ) : '',
						),
					),

					'deps'   => array(
						'id'     => 'yith_ppwcn_price_type',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Free Characters' ),
					'desc'   => esc_html__( "Especify the number of characters which won't add a cost to the final price" ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-number',
							'type'  => 'number',
							'id'    => 'yith_ppwcn_free_chars_text',
							'name'  => '_yith_ppwcn_free_char',
							'value' => ( get_post_meta( $post_id, '_yith_ppwcn_free_char', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_free_char', true ) : '',
						),
					),

					'deps'   => array(
						'id'     => 'yith_ppwcn_price_type',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Show Badge' ),
					'desc'   => esc_html__( 'Enable the badge for the product' ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-radio',
							'type'  => 'checkbox',
							'id'    => 'yith_ppwcn_badge_enable',
							'name'  => '_yith_ppwcn_show_badge',
							'value' => get_post_meta( $post_id, '_yith_ppwcn_show_badge', true ),
						),

					),

					'deps'   => array(
						'id'     => 'yith_ppwcn_is_enabled',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Badge Name' ),
					'desc'   => esc_html__( 'Especify the name of the note' ),
					'fields' => array(
						array(
							'class' => 'ppwcn-product-metabox-text',
							'type'  => 'text',
							'id'    => 'yith_ppwcn_badge_text',
							'name'  => '_yith_ppwcn_badge_text',
							'value' => ( get_post_meta( $post_id, '_yith_ppwcn_badge_text', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_badge_text', true ) : '',
						),

					),

					'deps'   => array(
						'id'     => 'yith_ppwcn_badge_enable',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Background Color' ),
					'desc'   => esc_html__( 'Especify the name of the note' ),
					'fields' => array(
						array(
							'class'         => 'ppwcn-product-metabox-colorpicker yith-plugin-fw-colorpicker color-picker',
							'type'          => 'colorpicker',
							'alpha_enabled' => false,
							'id'            => 'yith_ppwcn_background_color',
							'name'          => '_yith_ppwcn_badge_background',
							'value'         => ( get_post_meta( $post_id, '_yith_ppwcn_badge_background', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_badge_background', true ) : '',
						),

					),

					'deps'   => array(
						'id'     => 'yith_ppwcn_badge_enable',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
			yith_ppwcn_product_meta_form_field(
				array(
					'class'  => 'form-field wc_enable_field yith-plugin-ui ',
					'title'  => esc_html__( 'Text Color' ),
					'desc'   => esc_html__( 'Especify the name of the note' ),
					'fields' => array(
						array(
							'class'         => 'ppwcn-product-metabox-colorpicker yith-plugin-fw-colorpicker color-picker',
							'type'          => 'colorpicker',
							'alpha_enabled' => false,
							'id'            => 'yith_ppwcn_text_color',
							'name'          => '_yith_ppwcn_badge_text_color',
							'value'         => ( get_post_meta( $post_id, '_yith_ppwcn_badge_text_color', true ) ) ? get_post_meta( $post_id, '_yith_ppwcn_badge_text_color', true ) : '',
						),
					),
					'deps'   => array(
						'id'     => 'yith_ppwcn_badge_enable',
						'value'  => 'yes',
						'action' => 'hide',
					),
				)
			);
		}

		/**
		 * Enqueue_admin_styles
		 *
		 * @return void
		 */
		public function enqueue_admin_styles() {
			global $post;
			$product = wc_get_product( $post->ID );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'yith_ppwcn_admin_stylesheet', YITH_PPWCN_DIR_ASSETS_CSS_URL . '/yith-ppwcn-admin-stylesheet.css', array( 'yith-plugin-fw-fields' ), '1.0.0' );
			if ( $product ) {
				wp_enqueue_script( 'yith_ppwcn_admin_script', YITH_PPWCN_DIR_ASSETS_JS_URL . '/yith-ppwcn-admin-script.js', array( 'jquery', 'yith-plugin-fw-fields' ), '1.0.0', true );
			}
		}


		/**
		 * Save_product_notes_meta
		 *
		 * @param  mixed $post_id Id of the product.
		 * @return void
		 */
		public function save_product_notes_meta( $post_id ) {

			// TODO: only need the post_id.
			$product    = wc_get_product( $post_id );
			$product_id = $product->get_id();
			if ( array_key_exists( '_yith_ppwcn_is_enabled', $_POST ) ) {// phpcs:ignore 
				$wcn_meta_enabled = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_is_enabled'] ) );// phpcs:ignore 
				update_post_meta(
					$product_id,
					'_yith_ppwcn_is_enabled',
					$wcn_meta_enabled
				);
			} else {
				update_post_meta(
					$product_id,
					'_yith_ppwcn_is_enabled',
					'off'
				);
			};
			if ( array_key_exists( '_yith_ppwcn_note_label', $_POST ) ) {// phpcs:ignore 
				$wcn_meta_note_label = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_note_label'] ) );// phpcs:ignore 
				update_post_meta(
					$product_id,
					'_yith_ppwcn_note_label',
					$wcn_meta_note_label
				);
			};
			if ( array_key_exists( '_yith_ppwcn_note_desc', $_POST ) ) {// phpcs:ignore 
				$wcn_meta_desc = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_note_desc'] ) );// phpcs:ignore 
				update_post_meta(
					$product_id,
					'_yith_ppwcn_note_desc',
					$wcn_meta_desc
				);
			};
			if ( array_key_exists( '_yith_ppwcn_field_type', $_POST ) ) {// phpcs:ignore 
				$wcn_meta_field_type = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_field_type'] ) );// phpcs:ignore 
				update_post_meta(
					$product_id,
					'_yith_ppwcn_field_type',
					$wcn_meta_field_type
				);
			};
			if ( array_key_exists( '_yith_ppwcn_price_stg', $_POST ) ) {// phpcs:ignore 
				$wcn_meta_price_stg = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_price_stg'] ) );// phpcs:ignore 
				update_post_meta(
					$product_id,
					'_yith_ppwcn_price_stg',
					$wcn_meta_price_stg
				);
				if ( array_key_exists( '_yith_ppwcn_price', $_POST ) ) {// phpcs:ignore 
					$wcn_meta_price = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_price'] ) );// phpcs:ignore 
					update_post_meta(
						$product_id,
						'_yith_ppwcn_price',
						$wcn_meta_price
					);
				};
				if ( array_key_exists( '_yith_ppwcn_free_char', $_POST ) ) {// phpcs:ignore 
					$wcn_meta_free_char = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_free_char'] ) );// phpcs:ignore 
					update_post_meta(
						$product_id,
						'_yith_ppwcn_free_char',
						$wcn_meta_free_char
					);
				};
			};
			if ( array_key_exists( '_yith_ppwcn_show_badge', $_POST ) ) {// phpcs:ignore 
				$wcn_meta_show_badge = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_show_badge'] ) );// phpcs:ignore 
				update_post_meta(
					$product_id,
					'_yith_ppwcn_show_badge',
					$wcn_meta_show_badge
				);
				if ( array_key_exists( '_yith_ppwcn_badge_text', $_POST ) ) {// phpcs:ignore 
					$wcn_meta_badge_text = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_badge_text'] ) );// phpcs:ignore 
					update_post_meta(
						$product_id,
						'_yith_ppwcn_badge_text',
						$wcn_meta_badge_text
					);
				};
				if ( array_key_exists( '_yith_ppwcn_badge_background', $_POST ) ) {// phpcs:ignore 
					$wcn_meta_badge_background = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_badge_background'] ) );// phpcs:ignore 
					update_post_meta(
						$product_id,
						'_yith_ppwcn_badge_background',
						$wcn_meta_badge_background
					);
				};
				if ( array_key_exists( '_yith_ppwcn_badge_text_color', $_POST ) ) {// phpcs:ignore 
					$wcn_meta_text_color = sanitize_text_field( wp_unslash( $_POST['_yith_ppwcn_badge_text_color'] ) );// phpcs:ignore 
					update_post_meta(
						$product_id,
						'_yith_ppwcn_badge_text_color',
						$wcn_meta_text_color
					);
				};

			} else {
				update_post_meta(
					$product_id,
					'_yith_ppwcn_show_badge',
					'off'
				);
			}

		}

		/**
		 * Register_panel
		 *
		 * @return void
		 */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings' => __( 'Settings', 'yith-pp-wcnotes' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Test Plugin', // this text MUST be NOT translatable.
				'menu_title'         => 'YITH Test Plugin', // this text MUST be NOT translatable.
				'plugin_description' => __( 'Put here your plugin description', 'yith-pp-wcnotes' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith-pp-wcnotes',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_ppwcn_panel',
				'admin-tabs'         => $admin_tabs,
				'options-path'       => YITH_PPWCN_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Print_custom_field
		 *
		 * @param  mixed $field input parameters.
		 * @return void
		 */
		public function print_custom_field( $field ) {
			yith_ppwcn_get_view( '/fields/container-border.php', $field );
		}

		/**
		 * Create_menu_for_general_options
		 *
		 * @return void
		 */
		public function yith_ppwcn_create_menu_for_general_options() {

			add_menu_page(
				esc_html__( 'Product Notes Options', 'yith-pp-wcnotes' ),
				esc_html__( 'Product Notes Options', 'yith-pp-wcnotes' ),
				'manage_options',
				'product_notes_options',
				array( $this, 'product_notes_options_menu_page' ),
				'',
				42
			);
		}

		/**
		 * Product_notes_options_menu_page
		 *
		 * @return void
		 */
		public function product_notes_options_menu_page() {
			yith_ppwcn_get_view( '/admin/product-notes-options-panel.php', array() );
		}

		/**
		 * Register_settings
		 *
		 * @return void
		 */
		public function yith_ppwcn_register_settings() {

			$page_name    = 'ppwcn-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_ppwcn_padding',
					'title'    => esc_html__( 'Padding', 'yith-pp-wcnotes' ),
					'callback' => 'print_padding_input',
				),
				array(
					'id'       => 'yith_ppwcn_border',
					'title'    => esc_html__( 'Border', 'yith-pp-wcnotes' ),
					'callback' => 'print_border_input',
				),
				array(
					'id'       => 'yith_ppwcn_badge_position_in_shop',
					'title'    => esc_html__( 'Badge Position in Shop', 'yith-pp-wcnotes' ),
					'callback' => 'print_badge_shop_input',
				),
				array(
					'id'       => 'yith_ppwcn_badge_position_in_product',
					'title'    => esc_html__( 'Badge Position in Product', 'yith-pp-wcnotes' ),
					'callback' => 'print_badge_product_input',
				),
			);

			add_settings_section( $section_name, esc_html__( 'Section', 'yith-pp-wcnotes' ), '', $page_name );

			foreach ( $setting_fields as $field ) {
				extract( $field );// phpcs:ignore 

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);
				if ( 'yith_ppwcn_padding' === $id || 'yith_ppwcn_border' === $id ) {
					register_setting( $page_name, $id, array( 'type' => 'array' ) );
				} else {
					register_setting( $page_name, $id );
				}
			}

		}

		/**
		 * Print_padding_input
		 *
		 * @return void
		 */
		public function print_padding_input() {

			$padding_input_option = get_option(
				'yith_ppwcn_padding',
				array(
					'top'   => 20,
					'bot'   => 25,
					'left'  => 25,
					'right' => 25,
				)
			);

			$padding_input = array(
				array(
					'type'     => 'number',
					'label'    => __( 'Top', 'yith-pp-wcnotes' ),
					'id'       => 'yith_ppwcn_padding',
					'name'     => 'yith_ppwcn_padding[top]',
					'min'      => 0,
					'value'    => $padding_input_option['top'],
					'required' => true,

				),
				array(
					'type'     => 'number',
					'label'    => __( 'Bot', 'yith-pp-wcnotes' ),
					'id'       => 'yith_ppwcn_padding',
					'name'     => 'yith_ppwcn_padding[bot]',
					'min'      => 0,
					'value'    => $padding_input_option['bot'],
					'required' => true,
				),
				array(
					'type'     => 'number',
					'label'    => __( 'Left', 'yith-pp-wcnotes' ),
					'id'       => 'yith_ppwcn_padding',
					'name'     => 'yith_ppwcn_padding[left]',
					'min'      => 0,
					'value'    => $padding_input_option['left'],
					'required' => true,
				),
				array(
					'type'     => 'number',
					'label'    => __( 'Right', 'yith-pp-wcnotes' ),
					'id'       => 'yith_ppwcn_padding',
					'name'     => 'yith_ppwcn_padding[right]',
					'min'      => 0,
					'value'    => $padding_input_option['right'],
					'required' => true,
				),
			);

			?>
			<div class= "group_options clasillas">
				<?php yith_ppwcn_printer()->print_input_fields( $padding_input ); ?>
			</div>
			<?php
		}

		/**
		 * Print_border_input
		 *
		 * @return void
		 */
		public function print_border_input() {
			$border_input_option = get_option(
				'yith_ppwcn_border',
				array(
					'weight' => 1,
					'style'  => 'solid',
					'color'  => '#d8d8d8',
					'radius' => 7,
				)
			);

			$border_input = array(
				array(
					'type'     => 'number',
					'label'    => __( 'Weight', 'yith-pp-wcnotes' ),
					'id'       => 'yith_ppwcn_border',
					'name'     => 'yith_ppwcn_border[weight]',
					'min'      => 0,
					'value'    => $border_input_option['weight'],
					'required' => true,

				),
				array(
					'type'          => 'select',
					'label'         => __( 'Style', 'yith-pp-wcnotes' ),
					'id'            => 'yith_ppwcn_border',
					'name'          => 'yith_ppwcn_border[style]',
					'value'         => $border_input_option['style'],
					'select_values' => array( 'none', 'solid', 'dotted', 'dashed' ),
				),
				array(
					'type'    => 'colorpicker',
					'label'   => __( 'Color', 'yith-pp-wcnotes' ),
					'id'      => 'yith_ppwcn_border my-color-field',
					'name'    => 'yith_ppwcn_border[color]',
					'default' => '#d8d8d8',
					'value'   => $border_input_option['color'],

				),
				array(
					'type'     => 'number',
					'label'    => __( 'Radius', 'yith-pp-wcnotes' ),
					'id'       => 'yith_ppwcn_border',
					'name'     => 'yith_ppwcn_border[radius]',
					'min'      => 0,
					'value'    => $border_input_option['radius'],
					'required' => true,

				),

			);

			?>
			<div class= "group_options clasillas">
			<?php yith_ppwcn_printer()->print_input_fields( $border_input ); ?>
			</div>
			<?php
		}

		/**
		 * Print_badge_shop_input
		 *
		 * @return void
		 */
		public function print_badge_shop_input() {
			$badge_shop_option = get_option( 'yith_ppwcn_badge_position_in_shop', 'top-left' );
			$badge_shop_input  = array(
				array(
					'type'         => 'radio',
					'id'           => 'yith_ppwcn_badge_position_in_shop',
					'name'         => 'yith_ppwcn_badge_position_in_shop',
					'radio_values' => array( 'top-left', 'top-right' ),
					'value'        => $badge_shop_option,

				),
			)
			?>
			<div class= "group_options clasillas">
			<?php yith_ppwcn_printer()->print_input_fields( $badge_shop_input ); ?>
			</div>
			<?php
		}

		/**
		 * Print_badge_product_input
		 *
		 * @return void
		 */
		public function print_badge_product_input() {
			$badge_product_option = get_option( 'yith_ppwcn_badge_position_in_product', 'top-left' );
			$badge_product_input  = array(
				array(
					'type'         => 'radio',
					'id'           => 'yith_ppwcn_badge_position_in_product',
					'name'         => 'yith_ppwcn_badge_position_in_product',
					'radio_values' => array( 'top-left', 'top-right' ),
					'value'        => $badge_product_option,

				),
			)
			?>
			<div class= "group_options clasillas">
			<?php yith_ppwcn_printer()->print_input_fields( $badge_product_input ); ?>
			</div>
			<?php
		}

	}
}
