<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCN_Frontend' ) ) {
	/**
	 * YITH_PPWCN_Frontend
	 */
	class YITH_PPWCN_Frontend {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCN_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCN_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_styles' ) );

			YITH_PPWCN_Product_Note::get_instance();
		}

		/**
		 * Enqueue_admin_styles
		 *
		 * @return void
		 */
		public function enqueue_frontend_styles() {
			global $post;
			wp_enqueue_style( 'yith_ppwcn_frontend_stylesheet', YITH_PPWCN_DIR_ASSETS_CSS_URL . '/yith-ppwcn-frontend-stylesheet.css', array(), '1.0.0' );
			$padding = get_option(
				'yith-ppwcn-padding'
			);
			$borders = get_option(
				'yith_ppwcn_border',
				array(
					'weight' => 1,
					'style'  => 'solid',
					'color'  => '#d8d8d8',
					'radius' => 7,
				)
			);

			$product_badge_args['background-color'] = get_post_meta( $post->ID, '_yith_ppwcn_badge_background', true );
			$product_badge_args['text-color']       = get_post_meta( $post->ID, '_yith_ppwcn_badge_text_color', true );
			$product_badge_args['position-product'] = get_option( 'yith_ppwcn_badge_position_in_product', 'top-left' );
			$product_badge_args['position-shop']    = get_option( 'yith_ppwcn_badge_position_in_shop', 'top-left' );

			if ( 'top-left' === $product_badge_args['position-product'] ) {
				$badge_postion['left']               = 0;
				$badge_postion['left-product-phone'] = 0;
			} elseif ( 'top-right' === $product_badge_args['position-product'] ) {
				$badge_postion['left-product']       = 70;
				$badge_postion['left-product-phone'] = 0;
			}

			if ( 'top-left' === $product_badge_args['position-shop'] ) {
				$badge_postion['left-shop']       = 0;
				$badge_postion['left-shop-phone'] = 0;
			} elseif ( 'top-right' === $product_badge_args['position-shop'] ) {
				$badge_postion['left-shop']       = 52;
				$badge_postion['left-shop-phone'] = 62;
			}

			$custom_css = '
                    .yith_ppwcn_container {
                        padding:' . $padding['dimensions']['top'] . 'px ' . $padding['dimensions']['right'] . 'px ' . $padding['dimensions']['bottom'] . 'px ' . $padding['dimensions']['left'] . 'px ;
                        border:' . $borders['weight'] . 'px ' . $borders['style'] . ' ' . $borders['color'] . ';
                        border-radius:' . $borders['radius'] . 'px ;
                    }

                    .yith_ppwcn_note_badge_product {
                        background:' . $product_badge_args['background-color'] . ';
                        color:' . $product_badge_args['text-color'] . ';
                        padding: 1.5rem;
                        text-transform: uppercase;
                        font-weight: 700;
                        width: 180px;
                        text-align: center;
                        z-index: 1;
                        left:' . $badge_postion['left-product'] . '%;
                         
                    }
                    @media (min-width: 481px) and (max-width: 767px) {
                        .yith_ppwcn_badge_product_container {
                            left:' . $badge_postion['left-product-phone'] . '%;
                        }
                    }
                    
                    .yith_ppwcn_note_badge_shop {
                        left: ' . $badge_postion['left-shop'] . '%;
                        background-color: ' . $product_badge_args['background-color'] . ';
                        color:' . $product_badge_args['text-color'] . ';
                    }

                    @media (min-width: 481px) and (max-width: 767px) {
                        .yith_ppwcn_note_badge_shop {
                            left:' . $badge_postion['left-shop-phone'] . '%;
                        }
                    }
                    ';
			wp_add_inline_style( 'yith_ppwcn_frontend_stylesheet', $custom_css );
			wp_enqueue_script( 'yith_ppwcn_frontend_script', YITH_PPWCN_DIR_ASSETS_JS_URL . '/yith-ppwcn-frontend-script.js', array(), '1.0.0', true );
		}






	}
}
