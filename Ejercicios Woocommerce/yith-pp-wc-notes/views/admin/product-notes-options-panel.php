<?php
/**
 * Product Notes Panel
 *
 * @package WordPress
 */

?>
<div class="wrap">
	<h1><?php esc_html_e( 'Settings' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'ppwcn-options-page' );
			do_settings_sections( 'ppwcn-options-page' );
			submit_button();
		?>
	</form>
</div>
