<?php
/**
 *
 * Notes badge
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
?>

<span class='<?php echo esc_attr( $input_class ); ?>'><?php echo esc_html( $text ); ?></span>

