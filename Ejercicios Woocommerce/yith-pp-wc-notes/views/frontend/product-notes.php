<?php
/**
 *
 * Product Note View
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}

?>
<div class="yith_ppwcn_container">

<p id="yith_ppwcn_title"><?php echo isset( $label ) ? esc_html( $label ) : ''; ?></p>
<p id="yith_ppwcn_desc"><?php echo isset( $description ) ? esc_html( $description ) : ''; ?></p>
<p class="yith_ppwcn_note_field">
	<?php if ( 'text_radio' === $field_type ) : ?>
		<input type="text" name="_yith_ppwcn_note_product" id="yith_ppwcn_note_text" onkeyup="change_price()">
	<?php elseif ( 'text-area_radio' === $field_type ) : ?>
		<textarea name="_yith_ppwcn_note_product" id="yith_ppwcn_note_text" cols="30" rows="4" onkeyup="change_price()"></textarea>
	<?php endif; ?>
</p>
<p class="yith_ppwcn_price">
	<span><label id="yith_ppwcn_price_label">0.00</label> <label id="yith_ppwcn_price_currency"><?php echo esc_html( get_woocommerce_currency_symbol() ); ?></label></span>
	<input type="hidden" name="_yith_ppwcn_note_final_price" id="yih_ppwcn_hidden_price" value= '0'>
</p>
</div>
