<?php
/**
 *
 * Main File
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
if ( ! empty( $post ) && get_post_meta( $post->ID, $name, true ) !== false && ! empty( get_post_meta( $post->ID, $name, true ) ) ) {
	$input_text_value = get_post_meta( $post->ID, $name, true );
} elseif ( isset( $value ) ) {
	$input_text_value = $value;
}
?>

<?php if ( isset( $with_container ) ) : ?>
<div class = "yith-ppwcn-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="form-field">
<?php if ( isset( $label ) ) : ?>
	<label for="<?php echo esc_html( $id ); ?>"
			class="yith-ppwcn-text-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>

	<input type="text"  id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
		name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
		class=" my-color-field yith-ppwcn-text-input <?php echo isset( $input_class ) ? esc_attr( $input_class ) : ''; ?>"
		value="<?php echo isset( $input_text_value ) ? esc_html( $input_text_value ) : ''; ?>" data-default-color="<?php echo esc_html( $default ); ?>">
</p>
<?php if ( isset( $with_container ) ) : ?>

</div>
<?php endif; ?>
