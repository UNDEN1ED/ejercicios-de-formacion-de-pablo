<?php
/**
 * Container Border "css"
 *
 * @package WordPress
 */

$border = get_option( 'yith_ppwcn_border' );
?>
<div class="yith_ppwcn_inputs_group">
	<div class="yith_ppwcn_input_item">
		<label for="">Weight</label>
		<input type="number" name="yith_ppwcn_border[weight]" value="<?php echo ( false !== $border ) ? esc_html( $border['weight'] ) : 1; ?>">
	</div>
	<div class="yith_ppwcn_input_item">
		<label for="">Style</label>
		<select name="yith_ppwcn_border[style]" >
			<option value="none" <?php selected( 'none', $border['style'] ); ?>>None</option>
			<option value="solid" <?php selected( 'solid', $border['style'] ); ?>>Solid</option>
			<option value="dotted" <?php selected( 'dotted', $border['style'] ); ?>>Dotted</option>
			<option value="dashed" <?php selected( 'dashed', $border['style'] ); ?>>Dashed</option>
		</select>
	</div>
	<div class="yith_ppwcn_input_item">
		<label for="">Color</label>
		<input type="text" class="yith-plugin-fw-colorpicker color-picker" name="yith_ppwcn_border[color]" data-default-color="#d8d8d8" value="<?php echo ( false !== $border ) ? esc_html( $border['color'] ) : '#d8d8d8'; ?>">
	</div>
	<div class="yith_ppwcn_input_item">
		<label for="">Radius</label>
		<input type="number" name="yith_ppwcn_border[radius]" value="<?php echo ( false !== $border ) ? esc_html( $border['radius'] ) : 7; ?>">
	</div>
</div>
