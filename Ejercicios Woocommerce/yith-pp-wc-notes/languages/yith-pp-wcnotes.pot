# Copyright (C) 2020 Pablo Pérez Arteaga
# This file is distributed under the same license as the YITH WooCommerce Notes plugin.
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Notes 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-pp-wc-notes\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-11-29T19:42:20+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.5.0-alpha-9061b3e\n"
"X-Domain: yith_pp_wcnotes\n"

#. Plugin Name of the plugin
msgid "YITH WooCommerce Notes"
msgstr ""

#. Description of the plugin
msgid "Add personalization option to the product with engravings"
msgstr ""

#. Author of the plugin
msgid "Pablo Pérez Arteaga"
msgstr ""

#. Author URI of the plugin
msgid "https://yithemes.com/"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:68
msgid "Enabled"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:77
msgid "Note label"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:86
msgid "Note description"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:94
msgid "Field type"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:104
msgid "Price settings"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:114
msgid "Price"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:124
msgid "Free characters"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:134
msgid "Show Badge"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:142
msgid "Badge Text"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:151
msgid "Badge Background Color"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:162
msgid "Badge Text Color"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:183
msgid "Purchase Note"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:339
#: includes/class-yith-ppwcn-admin.php:340
msgid "Product Notes Options"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:371
msgid "Padding"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:376
msgid "Border"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:381
msgid "Badge Position in Shop"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:386
msgid "Badge Position in Product"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:391
msgid "Section"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:433
msgid "Top"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:443
msgid "Bot"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:452
msgid "Left"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:461
msgid "Right"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:496
msgid "Weight"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:506
msgid "Style"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:514
msgid "Color"
msgstr ""

#: includes/class-yith-ppwcn-admin.php:523
msgid "Radius"
msgstr ""

#: includes/class-yith-ppwcn-product-note.php:80
msgid "Free"
msgstr ""
