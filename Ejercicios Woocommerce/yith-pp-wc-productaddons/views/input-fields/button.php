<?php
/**
 *
 * Button Input View
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}

if ( ! empty( $post ) && get_post_meta( $post->ID, $name, true ) !== false && ! empty( get_post_meta( $post->ID, $name, true ) ) ) {
	$input_text_value = ( get_post_meta( $post->ID, $name, true ) );
} elseif ( isset( $value ) ) {
	$input_text_value = $value;
}

if ( isset( $with_container ) ) : ?>
<div class = "yith-ppwcpa-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
		id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="yith-ppwcpa-form-field">
<?php if ( isset( $label ) ) : ?>
		<label for="<?php echo esc_html( $id ); ?>"
				class="yith-ppwcpa-text-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
			<?php echo esc_html( $label ); ?>
		</label>
<?php endif; ?>
<button type="button"  id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
		name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
		class="yith-ppwcpa-text-input <?php echo isset( $input_class ) ? esc_attr( $input_class ) : ''; ?>"
		onclick="<?php echo isset( $onclick ) ? esc_html( $onclick ) : ''; ?>"
		<?php echo ( isset( $required ) && true === $required ) ? esc_html( 'required' ) : ''; ?>>
		<?php echo isset( $input_text_value ) ? esc_html( $input_text_value ) : ''; ?>
</button>
</p>
<?php if ( isset( $with_container ) ) : ?>
</div>
<?php endif; ?>
