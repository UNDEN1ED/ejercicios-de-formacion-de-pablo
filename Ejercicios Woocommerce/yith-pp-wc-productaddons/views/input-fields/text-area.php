<?php
/**
 *
 * Main File
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
if ( ! empty( $post ) && get_post_meta( $post->ID, $name, true ) !== false && ! empty( get_post_meta( $post->ID, $name, true ) ) ) {
	$input_text_value = ( get_post_meta( $post->ID, $name, true ) );
} elseif ( isset( $value ) ) {
	$input_text_value = $value;
}
?>

<?php if ( isset( $with_container ) ) : ?>
<div class = " options_group yith-ppwcn-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="form-field">
<?php if ( isset( $label ) ) : ?>
	<label for="<?php echo esc_html( $id ); ?>"
			class="yith-ppwcn-text_area-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>

	<textarea id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
		name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
		class="yith-ppwcn-text_area-input <?php echo isset( $input_class ) ? esc_attr( $input_class ) : ''; ?>"
			rows="4" cols="50"><?php echo isset( $input_text_value ) ? esc_html( $input_text_value ) : ''; ?></textarea>
</p>
<?php if ( isset( $with_container ) ) : ?>

</div>
<?php endif; ?>
