<?php
/**
 *
 * Main File
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}

if ( ! empty( $post ) && get_post_meta( $post->ID, $name, true ) !== false ) {
	$input_text_value = ( get_post_meta( $post->ID, $name, true ) );
} elseif ( isset( $value ) ) {
	$input_text_value = $value;
}
?>

<?php if ( isset( $with_container ) ) : ?>
<div class = "yith-ppwcn-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="form-field">
<?php if ( isset( $label ) ) : ?>
	<label class="yith-ppwcn-text-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>
<select name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>" id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>" class="<?php echo isset( $input_class ) ? esc_attr( $input_class ) : ''; ?>" onchange="<?php echo isset( $on_change ) ? esc_attr( $on_change ) : ''; ?>">
<?php

if ( isset( $select_values ) ) :
	foreach ( $select_values as $select_value ) :
		?>
		<option value="<?php echo esc_html( $select_value ); ?>" <?php isset( $input_text_value ) ? selected( $input_text_value, $select_value ) : ''; ?>><?php echo esc_html( $select_value ); ?></option>	
		<?php
	endforeach;
endif;
?>
</select>
</p>
<?php if ( isset( $with_container ) ) : ?>

</div>
<?php endif; ?>
