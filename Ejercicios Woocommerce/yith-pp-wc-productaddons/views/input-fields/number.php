<?php
/**
 *
 * Main File
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}

?>

<?php if ( isset( $with_container ) ) : ?>
<div class = "yith-ppwcn-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="form-field">
<?php if ( isset( $label ) ) : ?>
	<label for="<?php echo esc_html( $id ); ?>"
			class="yith-ppwcn-text-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>

	<input type="number"  id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
		name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
		class="yith-ppwcn-text-input <?php echo isset( $input_class ) ? esc_attr( $input_class ) : ''; ?>"
		min="<?php echo isset( $min ) ? esc_html( intval( $min ) ) : ''; ?>" max="<?php echo isset( $max ) ? esc_html( intval( $max ) ) : ''; ?>"
		step= '0.01'
		value="<?php echo isset( $value ) ? esc_html( $value ) : ''; ?>"
		<?php echo ( isset( $required ) && true === $required ) ? esc_html( 'required' ) : ''; ?>>
</p>
<?php if ( isset( $with_container ) ) : ?>

</div>
<?php endif; ?>
