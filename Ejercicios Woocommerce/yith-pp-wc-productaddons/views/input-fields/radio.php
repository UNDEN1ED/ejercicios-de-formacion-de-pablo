<?php
/**
 *
 * Main File
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
if ( ! empty( $post ) && get_post_meta( $post->ID, $name, true ) !== false && ! empty( get_post_meta( $post->ID, $name, true ) ) ) {
	$input_text_value = ( get_post_meta( $post->ID, $name, true ) );
} elseif ( isset( $value ) ) {
	$input_text_value = $value;

}

?>

<?php if ( isset( $with_container ) ) : ?>
<div class = "yith-ppwcn-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="form-field">
<?php if ( isset( $label ) ) : ?>
	<label class="yith-ppwcn-text-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>
<?php
if ( isset( $radio_values ) ) :
	foreach ( $radio_values as $radio_value ) :
		?>
		<label for="<?php echo esc_html( $id ); ?>_<?php echo esc_html( $radio_value ) . isset( $input_index ) ? esc_attr( $input_index ) : ''; ?>"
			class="yith-ppwcpa-radio-label">
			<?php echo esc_html( ucfirst( $radio_value ) ); ?>
		</label>
		<input type="radio"  id="<?php echo isset( $id ) ? esc_attr( $id ) . '_' . esc_html( $radio_value ) . ( isset( $variation_index ) ? esc_html( $variation_index . '-' ) : '' ) . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) : ''; ?>"
			class="yith-ppwcpa-radio-input"
			name="<?php echo isset( $name ) ? esc_attr( $name ) . '[' . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) . '][price_stg]' : ''; ?>"
			value="<?php echo isset( $radio_value ) ? esc_html( $radio_value ) : ''; ?>" onclick="<?php echo isset( $radio_onclick ) ? esc_html( $radio_onclick ) : ''; ?>" <?php isset( $input_text_value ) ? checked( $input_text_value, $radio_value ) : ''; ?>>
		<?php
	endforeach;
endif;
?>
</p>
<?php if ( isset( $with_container ) ) : ?>

</div>
<?php endif; ?>
