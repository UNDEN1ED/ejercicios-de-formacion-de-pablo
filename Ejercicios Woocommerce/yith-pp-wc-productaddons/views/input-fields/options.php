<?php
/**
 * Input Option
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}




?>

<?php if ( isset( $open_container ) ) : ?>
<div class = "yith-ppwcn-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<div class="yith-ppwcpa-option-field">
<?php if ( isset( $label ) ) : ?>
	<label class="yith-ppwcpa-text-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>
<?php
if ( isset( $radio_values ) ) :
	?>
<div class="yith-ppwcpa-option-containers">
	<div class = "yith-ppwcpa-options-container" id="yith-ppwcpa-options-container<?php echo ( isset( $variation_index ) ? esc_html( $variation_index . '-' ) : '' ) . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ); ?>" style="display:none">

	<div class="yith-ppwcpa-field-options">
		<label class="yith-ppwcn-text-label">
			<?php echo esc_html( ucfirst( $radio_values[0] ) ); ?>
		</label>
		<input type="text"  id="<?php echo isset( $id ) ? esc_attr( $id ) . '_' . esc_html( $radio_values[0] ) . ( isset( $variation_index ) ? esc_html( $variation_index . '-' ) : '' ) . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) : ''; ?>"
			name="<?php echo isset( $name ) ? esc_attr( $name ) . '[' . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) . '][options][' . esc_html( $radio_values[0] ) . '][]' : ''; ?>"
			value="<?php echo isset( $value['name'] ) ? esc_html( $value['name'] ) : ''; ?>" >
	</div>
	<div class="yith-ppwcpa-field-options">
		<label class="yith-ppwcpa-text-label">
			<?php echo esc_html( ucfirst( $radio_values[1] ) ); ?>
		</label>
		<input type="text"  id="<?php echo isset( $id ) ? esc_attr( $id ) . '_' . esc_html( $radio_values[1] ) . ( isset( $variation_index ) ? esc_html( $variation_index . '-' ) : '' ) . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) : ''; ?>"
			name="<?php echo isset( $name ) ? esc_attr( $name ) . '[' . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) . '][options][' . esc_html( $radio_values[1] ) . '][]' : ''; ?>"
			value="<?php echo isset( $value['price'] ) ? esc_html( $value['price'] ) : ''; ?>" >
	</div>
	<button type='button' class='yith-ppwcpa-delete-buttton'
	<?php
	if ( null === $input_index ) :
		?>
	id="yith-ppwcpa-delete-buttton"
	<?php endif; ?> 
	value="off" onclick="delete_option(this)" >
			<span class='yith-ppwcpa-trash-img  dashicons dashicons-trash' id="yith-ppwcpa-trash-img"></span>
		</button>

	</div>
		<?php
		if ( isset( $values ) && ! empty( $values ) ) :
			$values_len = count( $values['name'] );
			for ( $i = 0; $i < $values_len; $i++ ) :
				?>
	<div class = "yith-ppwcpa-options-container" onmouseover="display_trash_button(this)" onmouseout="hide_trash_button(this)" >

	<div class="yith-ppwcpa-field-options">
		<label class="yith-ppwcn-text-label">
				<?php echo esc_html( ucfirst( $radio_values[0] ) ); ?>
		</label>
		<input type="text"  name="<?php echo isset( $name ) ? esc_attr( $name ) . '[' . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) . '][options][' . esc_html( $radio_values[0] ) . '][]' : ''; ?>"
			value="<?php echo esc_html( $values['name'][ $i ] ); ?>" >
	</div>
	<div class="yith-ppwcpa-field-options">
		<label class="yith-ppwcpa-text-label">
				<?php echo esc_html( ucfirst( $radio_values[1] ) ); ?>
		</label>
		<input type="text" class="yith_ppwcpa_option_price" name="<?php echo isset( $name ) ? esc_attr( $name ) . '[' . ( isset( $input_index ) ? esc_attr( $input_index ) : '' ) . '][options][' . esc_html( $radio_values[1] ) . '][]' : ''; ?>"
			value="<?php echo esc_html( $values['price'][ $i ] ); ?>" >
	</div>
	<button type='button' class='yith-ppwcpa-delete-buttton' value="off" >
			<span class='yith-ppwcpa-trash-img  dashicons dashicons-trash' id="yith-ppwcpa-trash-img"></span>
		</button>

	</div>
				<?php
				endfor;
				endif;
		?>
	<p class="yith-ppwcpa-add-option" >+ Add new option</p>
</div>
				<?php
				endif;
?>
</div>
				<?php if ( isset( $close_container ) ) : ?>

</div>
<?php endif; ?>
