<?php
/**
 *
 * Main File
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
?>

<?php if ( isset( $with_container ) ) : ?>
<div class = " options_group yith-ppwcpa-field <?php echo isset( $container_class ) ? esc_attr( $container_class ) : ''; ?>"
	id="<?php echo isset( $container_id ) ? esc_attr( $container_id ) : ''; ?>">
<?php endif; ?>
<p class="form-field">
<?php if ( isset( $label ) ) : ?>
	<label for="<?php echo esc_html( $id ); ?>"
			class="yith-ppwcpa-checkbox-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>

	<input type="checkbox"  id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
		name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
		class="yith-ppwcpa-checkbox-input <?php echo isset( $input_class ) ? esc_attr( $input_class ) : ''; ?>"
		value="<?php echo isset( $value ) ? esc_html( $value ) : ''; ?>" onclick="<?php echo isset( $onclick ) ? esc_html( $onclick ) : ''; ?>"
		<?php
		if ( get_post_meta( $post->ID, $name, true ) === $value ) {
			echo esc_html( 'checked' );
		}
		?>
		>
</p>
<?php if ( isset( $with_container ) ) : ?>

</div>
<?php endif; ?>
