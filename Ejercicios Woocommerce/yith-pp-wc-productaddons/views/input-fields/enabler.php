<?php
/**
 *
 * Enabler Input
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
?>
<?php if ( isset( $label ) && ! empty( $label ) ) : ?>
<div class="yith-ppwcpa-option-field yith-ppwcpa-option-field-enabler">
	<label for="<?php echo esc_html( $id ); ?>"
			class="yith-ppwcpa-checkbox-label <?php echo isset( $label_class ) ? esc_attr( $label_class ) : ''; ?>">
		<?php echo esc_html( $label ); ?>
	</label>
<?php endif; ?>
<div class="yith-ppwcpa-toggle">
	<input type="checkbox" id="<?php echo isset( $id ) ? esc_html( $id ) : ''; ?>" name="<?php echo isset( $name ) ? esc_html( $name ) : ''; ?>" value="on" <?php echo isset( $value ) ? checked( $value, 'on' ) : ''; ?>>
	<label for="<?php echo isset( $id ) ? esc_html( $id ) : ''; ?>" class= "yith_ppwcpa_toggle_label <?php echo isset( $input_class ) ? esc_html( $input_class ) : ''; ?>" id="<?php echo isset( $input_class ) ? esc_html( $input_class ) : ''; ?>" ></label>
</div>
<?php if ( isset( $label ) && ! empty( $label ) ) : ?>	
</div>
<?php endif; ?>
