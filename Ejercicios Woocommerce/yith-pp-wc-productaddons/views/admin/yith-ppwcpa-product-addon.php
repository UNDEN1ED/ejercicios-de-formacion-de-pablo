<?php
/**
 *
 * Product Addon Template
 *
 * @package WordPress
 */

if ( isset( $addon['index'] ) ) {
	$display_option = '';
} else {
	$display_option = 'none';
	$addon['index'] = '-INDEX-';
}

?>

<div class="yith-ppwcpa-addon-wrapper" id="yith-ppwcpa-addon-container<?php echo isset( $variant_id ) ? esc_html( $variant_id ) . '-' : ''; ?><?php echo isset( $addon['index'] ) ? esc_html( $addon['index'] ) : ''; ?>" style='display:<?php echo esc_attr( $display_option ); ?>' >
	<div class="yith-ppwcpa-addon-header" >
		<button type='button' class='yith-ppwcpa-dropdown-buttton' id="yith-ppwcpa-dropdown-buttton<?php echo isset( $variant_id ) ? esc_html( $variant_id ) . '-' : ''; ?><?php echo isset( $addon['index'] ) ? esc_html( $addon['index'] ) : ''; ?>" value="off">
			<span class='yith-ppwcpa-arrow-img dashicons dashicons-arrow-down'></span>
		</button>
		<?php
			yith_ppwcpa_printer()->print_input_field(
				array(
					'type'        => 'enabler',
					'id'          => 'yith_ppwcpa_is_enabled' . ( isset( $variant_id ) ? $variant_id : '' ) . '-' . $addon['index'],
					'input_class' => 'yith_ppwcpa_toggle_label',
					'name'        => ( ( isset( $variant_id ) && '' !== $variant_id ) ? '_yith_ppwcpa-variant[' . $variant_id . ']' : '_yith_ppwcpa' ) . '[' . $addon['index'] . '][enable]',
					'onclick'     => '',
					'value'       => ! empty( $addon['enable'] ) ? $addon['enable'] : null,
				)
			);
			?>
		<h2 id="yith-ppwcpa-addon-title<?php echo isset( $variant_id ) ? esc_html( $variant_id ) . '-' : ''; ?><?php echo isset( $addon['index'] ) ? esc_html( $addon['index'] ) : ''; ?>" ><?php echo isset( $addon['name'] ) && ! empty( $addon['name'] ) ? esc_html( $addon['name'] ) : 'Custom Add-on'; ?></h2>
	</div>
	<div class="yith-ppwcpa-addon-body" style='display:none'>
		<?php yith_ppwcpa_printer()->print_input_fields( $args ); ?>
		<p class="yith-ppwcpa-addon-field">
			<button type="button" class="button button-secondary yith-ppwcpa-remove-addon-buttton" id="yith-ppwcpa-remove-buttton<?php echo isset( $variant_id ) ? esc_html( $variant_id ) . '-' : ''; ?><?php echo isset( $addon['index'] ) ? esc_html( $addon['index'] ) : ''; ?>">REMOVE ADDON</button>
		</p>
		<input type="hidden" name="_yith_ppwcpa_addon_id<?php echo isset( $variant_id ) ? esc_html( $variant_id ) : ''; ?>[]" id="yith_ppwcpa_addon_id<?php echo isset( $variant_id ) ? esc_html( $variant_id ) : ''; ?>-<?php echo isset( $addon['index'] ) ? esc_html( $addon['index'] ) : ''; ?>" value="<?php echo isset( $addon['index'] ) ? esc_html( $addon['index'] ) : ''; ?>">
	</div>
</div>
