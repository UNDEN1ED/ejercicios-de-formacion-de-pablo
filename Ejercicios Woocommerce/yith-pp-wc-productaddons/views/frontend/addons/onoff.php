<?php
/**
 *
 * Frontend Input OnOff template
 *
 * @package WordPress
 */

foreach ( $addon as $addon_field => $val ) {
	${$addon_field} = $val;
}
if ( isset( $variant_id ) ) {
	$index = '-INDEX-';
}
?>
<div class="yith_ppwcpa_container">

<h4><?php echo isset( $name ) ? esc_html( $name ) : ''; ?></h4>
	<div class="yith-ppwcpa-toggle">
		<input type="checkbox" name="yith_ppwcpa_input[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" id="yith_ppwcpa_addon_input<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" value="<?php echo esc_html( $price ); ?>" <?php checked( isset( $default_enable ) ? $default_enable : 'off', 'on' ); ?>>
		<label for="yith_ppwcpa_addon_input<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" class="yith_ppwcpa_toggle_label"></label>
	</div>
<label class="yith_ppwcpa_toggle_description"><?php echo isset( $description ) ? esc_html( $description ) : ''; ?></label>
<?php if ( 'free' !== $price_stg ) : ?>
<p class="yith_ppwcpa_addon_price">
	<label id="yith_ppwcpa_addon_currency<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>"></label>
	<label ><?php echo esc_html( get_woocommerce_currency_symbol() ); ?></label>
	<label id="yith_ppwcpa_addon_price<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>">0</label>
</p>
<?php else : ?>
	<p>Free</p>
<?php endif; ?>
<input type="hidden"  name ="yith_ppwcpa_name[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="<?php echo isset( $name ) ? esc_html( $name ) : ''; ?>">
<input type="hidden" class ="yith_ppwcpa_hidden_price" id="yith_ppwcpa_hidden_price<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" name ="yith_ppwcpa_price[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="<?php echo ( 'free' !== $price_stg ) ? esc_attr( '' ) : 'Free'; ?>">
</div>
