<?php
/**
 *
 * Frontend Input Radio template
 *
 * @package WordPress
 */

foreach ( $addon as $addon_field => $val ) {
	${$addon_field} = $val;
}
if ( isset( $variant_id ) ) {
	$index = '-INDEX-';
}
$option_len = isset( $options ) ? count( $options['name'] ) : false;
?>
<div class="yith_ppwcpa_container">
	<h4><?php echo isset( $name ) ? esc_html( $name ) : ''; ?></h4>
	<p><?php echo isset( $description ) ? esc_html( $description ) : ''; ?></p>
	<div class="yith_ppwcpa_radio_container">
		<input type="radio" name="yith_ppwcpa_radio_input<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" value="" checked>
		<label><?php esc_html_e( 'None' ); ?></label>
	</div>
		<?php
		if ( $option_len ) :
			for ( $i = 0; $i < $option_len; $i++ ) :
				?>
	<div class="yith_ppwcpa_radio_container">
				<?php if ( 'free' !== $price_stg ) : ?>
		<input type="radio" name="yith_ppwcpa_radio_input<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" value="<?php echo esc_html( $options['name'][ $i ] . ',' . $options['price'][ $i ] ); ?>" >
		<label><?php echo esc_html( $options['name'][ $i ] . ' + ' . get_woocommerce_currency_symbol() . $options['price'][ $i ] ); ?></label>
				<?php else : ?>
		<input type="radio" name="yith_ppwcpa_radio_input<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" value="<?php echo esc_html( $options['name'][ $i ] . ',Free' ); ?>" >
		<label><?php echo esc_html( $options['name'][ $i ] . ' Free' ); ?></label>        
				<?php endif; ?>
	</div>
				<?php
		endfor;
		endif;
		?>
	<input type="hidden"  name ="yith_ppwcpa_name[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="<?php echo isset( $name ) ? esc_html( $name ) : ''; ?>">
	<input type="hidden" id="yith_ppwcpa_hidden_option_name<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" name ="yith_ppwcpa_input[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="">
	<input type="hidden" class ="yith_ppwcpa_hidden_price" id="yith_ppwcpa_hidden_option_price<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" name ="yith_ppwcpa_price[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="">

</div>
