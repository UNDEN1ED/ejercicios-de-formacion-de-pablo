<?php
/**
 *
 * Frontend Select template
 *
 * @package WordPress
 */

foreach ( $addon as $addon_field => $val ) {
	${$addon_field} = $val;
}
if ( isset( $variant_id ) ) {
	$index = '-INDEX-';
}
$option_len = count( $options['name'] );
?>
<div class="yith_ppwcpa_container">
	<h4><?php echo isset( $name ) ? esc_html( $name ) : ''; ?></h4>
	<p><?php echo isset( $description ) ? esc_html( $description ) : ''; ?></p>
	<select name="yith_ppwcpa_select_input<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" id="yith_ppwcpa_select<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" >
		<option value=""><?php esc_html_e( 'None' ); ?></option>
	<?php
	for ( $i = 0; $i < $option_len; $i++ ) :
		if ( 'free' !== $price_stg ) :
			?>
		<option value="<?php echo esc_html( $options['name'][ $i ] . ',' . $options['price'][ $i ] ); ?>">
			<?php echo esc_html( $options['name'][ $i ] . ' + ' . get_woocommerce_currency_symbol() . $options['price'][ $i ] ); ?>
		</option>
	<?php else : ?>
		<option value="<?php echo esc_html( $options['name'][ $i ] . ',Free' ); ?>">
			<?php echo esc_html( $options['name'][ $i ] . ' Free' ); ?>
		</option>
		<?php
		endif;
	endfor;
	?>
	</select>
	<input type="hidden"  name ="yith_ppwcpa_name[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="<?php echo isset( $name ) ? esc_html( $name ) : ''; ?>">
	<input type="hidden" id="yith_ppwcpa_hidden_option_name<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" name ="yith_ppwcpa_input[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="">
	<input type="hidden" class ="yith_ppwcpa_hidden_price" id="yith_ppwcpa_hidden_option_price<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>" name ="yith_ppwcpa_price[<?php echo isset( $index ) ? esc_attr( $index ) : ''; ?>]" value="">
</div>
