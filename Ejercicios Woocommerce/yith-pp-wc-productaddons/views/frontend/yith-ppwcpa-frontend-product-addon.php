<?php
/**
 *
 * Main Front Addon View
 *
 * @package WordPress
 */

global $product;
?>
<div id="yith_ppwcpa_addons_container">

<?php

foreach ( $args as $product_addon ) {

	if ( isset( $product_addon['enable'] ) && 'on' === $product_addon['enable'] ) {
		yith_ppwcpa_get_view( '/frontend/addons/' . $product_addon['field'] . '.php', array(), $product_addon );
	}
}

?>
</div>
<div id ='yith_ppwcpa_variant_addons_container'>

</div>

<div class="yith_ppwcpa_price_summary">
	<div class="yith_ppwcpa_summary_item_title"><h4>Price totals</h4></div>
	<div class="yith_ppwcpa_summary_item_option_title"><p>Product price:</p></div>
	<div class="yith_ppwcpa_summary_item"><p class="price"><span><?php echo esc_html( get_woocommerce_currency_symbol() ); ?></span><span id="yith_ppwcpa_product_price" ><?php echo esc_html( $product->get_price() . ',00' ); ?></span></p></div>
	<div class="yith_ppwcpa_summary_item_option_title"><p>Additional options total:</p></div>
	<div class="yith_ppwcpa_summary_item"><p class="price" ><span><?php echo esc_html( get_woocommerce_currency_symbol() ); ?></span ><span id="yith_ppwcpa_total_addons_price" >0</span></p></div>
	<div class="yith_ppwcpa_summary_item_option_title"><p>Total:</p></div>
	<div class="yith_ppwcpa_summary_item"><p class="price"><span><?php echo esc_html( get_woocommerce_currency_symbol() ); ?></span><span id="yith_ppwcpa_total_price" >0</span></p></div>

</div>
