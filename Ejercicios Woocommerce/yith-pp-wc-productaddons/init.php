<?php
/**
 * Plugin Name: YITH Product Addons
 * Plugin URI: http://local.wordpress.test/
 * Description: Create your own pnfles. Make your viewers and users able to join pnfles.
 * Author: Pablo Pérez Arteaga
 * Text Domain: yith-ppwc-productaddons
 * Version: 1.7.2
 * Author URI:
 *
 * @package WordPress
 */

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'YITH_PPWCPA_VERSION' ) ) {
	define( 'YITH_PPWCPA_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PPWCPA_DIR_URL' ) ) {
	define( 'YITH_PPWCPA_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCPA_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PPWCPA_DIR_ASSETS_URL', YITH_PPWCPA_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PPWCPA_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PPWCPA_DIR_ASSETS_CSS_URL', YITH_PPWCPA_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PPWCPA_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PPWCPA_DIR_ASSETS_JS_URL', YITH_PPWCPA_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PPWCPA_DIR_PATH' ) ) {
	define( 'YITH_PPWCPA_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCPA_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PPWCPA_DIR_INCLUDES_PATH', YITH_PPWCPA_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PPWCPA_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PPWCPA_DIR_TEMPLATES_PATH', YITH_PPWCPA_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PPWCPA_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PPWCPA_DIR_VIEWS_PATH', YITH_PPWCPA_DIR_PATH . 'views' );
}

if ( ! function_exists( 'yith_ppwcpa_init_classes' ) ) {

	/**
	 * Yith_ppwcpa_init_classes
	 *
	 * @return void
	 */
	function yith_ppwcpa_init_classes() {

		load_plugin_textdomain( 'yith-ppwc-productaddons', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PPWCPA_DIR_INCLUDES_PATH . '/class-yith-ppwcpa-plugin-skeleton.php';

		if ( class_exists( 'YITH_PPWCPA_Plugin_Skeleton' ) ) {

			yith_ppwcpa_plugin_skeleton();

		}
	}
}

add_action( 'plugins_loaded', 'yith_ppwcpa_init_classes', 11 );
