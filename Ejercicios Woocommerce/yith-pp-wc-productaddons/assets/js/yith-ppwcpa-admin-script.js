function show_addon_fields(container) {
    if ((jQuery(container).attr('id').split(/(\d+\-\d+)/)).length == 1) {
        var current_index = jQuery(container).attr('id').split(/(\d+)/)[1];
    } else {
        var current_index = jQuery(container).attr('id').split(/(\d+\-\d+)/)[1];
    }
    var element_type = jQuery('#yith_ppwcpa_type_select' + current_index).val();
    var price_setting = jQuery('#yith_ppwcpa_price_stgs_free' + current_index).val();
    jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-radio-input').each(function () {
        if (jQuery(this).is(':checked')) {
            price_setting = jQuery(this).val();
        }

    });

    switch (element_type) {
        case 'text':
        case 'textarea':
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-radio-label:nth-of-type(4)').css('display', '');
            jQuery('#yith_ppwcpa_price_stgs_per-character' + current_index).css('display', 'inline-block');
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-option-field:first').css('display', 'none');
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-option-field:last').css('display', 'none');
            switch (price_setting) {
                case 'free':
                    jQuery('#yith_ppwcpa_price' + current_index).parent().css('display', 'none');
                    jQuery('#yith_ppwcpa_free_characters' + current_index).parent().css('display', 'none');
                    break;
                case 'fixed-price':
                case 'per-character':
                    jQuery('#yith_ppwcpa_price' + current_index).parent().css('display', 'block');
                    jQuery('#yith_ppwcpa_free_characters' + current_index).parent().css('display', 'block');
                    break;
            }
            break;
        case 'select':
        case 'radio':
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-radio-label:nth-of-type(4)').css('display', 'none');
            jQuery('#yith_ppwcpa_price_stgs_per-character' + current_index).css('display', 'none');
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-option-field:first').css('display', 'block');
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-option-field:last').css('display', 'none');
            jQuery('#yith_ppwcpa_price' + current_index).parent().css('display', 'none');
            jQuery('#yith_ppwcpa_free_characters' + current_index).parent().css('display', 'none');
            if (jQuery('#yith_ppwcpa_price_stgs_per-character' + current_index).is(':checked')) {
                jQuery('#yith_ppwcpa_price_stgs_fixed-price' + current_index).prop("checked", true);
            }
            switch (price_setting) {
                case 'free':
                    jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith_ppwcpa_option_price').each(function () {
                        jQuery(this).parent().css('display', 'none');
                    });
                    break;
                case 'fixed-price':
                    jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith_ppwcpa_option_price').each(function () {
                        jQuery(this).parent().css('display', 'block');
                    });
                    break;
            }
            break;
        case 'checkbox':
        case 'onoff':
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-radio-label:nth-of-type(4)').css('display', 'none');
            jQuery('#yith_ppwcpa_price_stgs_per-character' + current_index).css('display', 'none');
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-option-field:first').css('display', 'none');
            jQuery('#yith-ppwcpa-addon-container' + current_index).find('.yith-ppwcpa-option-field:last').css('display', 'inline-block');
            jQuery('#yith_ppwcpa_free_characters' + current_index).parent().css('display', 'none');
            switch (price_setting) {
                case 'free':
                    jQuery('#yith_ppwcpa_price' + current_index).parent().css('display', 'none');
                    break;
                case 'fixed-price':
                    jQuery('#yith_ppwcpa_price' + current_index).parent().css('display', 'block');
                    break;
            }
            break;

    }
}


jQuery(document).ready(function ($) {

    display_body = function () {
        if ($(this).val() == 'off') {
            $(this).parent().parent().children().last().slideToggle('slow', function () {
                $(this).css('display', 'block');
            })
            show_addon_fields(this);
            $(this).val('on');
            $(this).find('span').removeClass('dashicons-arrow-down');
            $(this).find('span').addClass('dashicons-arrow-up');

        } else {
            $(this).parent().parent().children().last().slideToggle('slow', function () {
                $(this).css('display', 'none');
            })
            $(this).val('off');
            $(this).find('span').removeClass('dashicons-arrow-up');
            $(this).find('span').addClass('dashicons-arrow-down');

        }
    }

    display_trash_button = function () {
        $(this).children().eq(2).css('visibility', 'visible');
    }
    hide_trash_button = function () {
        $(this).children().eq(2).css('visibility', 'hidden');
    }

    add_new_option = function () {
        options_original = $(this).parent().children().first();
        if (($(options_original).attr('id').split(/(\d+\-\d+)/)).length == 1) {
            var current_index = options_original.attr('id').split(/(\d+)/)[1];
        } else {
            var current_index = options_original.attr('id').split(/(\d+\-\d+)/)[1];
        }
        var new_option = options_original.clone();
        new_option.attr('id', '');
        new_option.find('#yith_ppwcpa_options_price' + current_index).addClass('yith_ppwcpa_option_price');
        new_option.find('input[type="text"]').each(function () {
            $(this).removeAttr('id');
        });
        new_option.css('display', "flex");
        new_option.mouseover(display_trash_button);
        new_option.mouseout(hide_trash_button);
        new_option.find('.yith-ppwcpa-delete-buttton').on('click', delete_option);

        $(this).before(new_option);
        show_addon_fields(options_original);
    }

    delete_option = function () {
        $(this).parent().remove();
    }

    remove_addon = function () {
        if (($(this).attr('id').split(/(\d+\-\d+)/)).length == 1) {
            var current_index = $(this).attr('id').split(/(\d+)/)[1];
        } else {
            var current_index = $(this).attr('id').split(/(\d+\-\d+)/)[1];
        }
        $('#yith-ppwcpa-addon-container' + current_index).remove();
        $('#yith_ppwcpa_addons_index').value -= 1;

    }

    change_addon_title = function () {
        if (($(this).attr('id').split(/(\d+\-\d+)/)).length == 1) {
            var current_index = $(this).attr('id').split(/(\d+)/)[1];
        } else {
            var current_index = $(this).attr('id').split(/(\d+\-\d+)/)[1];
        }
        var new_text = $(this).val();
        if (new_text != '') {
            $('#yith-ppwcpa-addon-title' + current_index).html(new_text);
        } else {
            $('#yith-ppwcpa-addon-title' + current_index).html('Custom Add-on');
        }
    }

    $('.yith_ppwcpa_sortable_container').sortable();


    $('#yith_ppwcpa_new_addon').on('click', function () {
        var clone = $('#yith-ppwcpa-addon-container-INDEX-').clone();
        addon_index = $('#yith_ppwcpa_addons_index').val();
        $(clone).attr('id', $(clone).attr('id').replace(/-INDEX-/g, addon_index));
        $(clone).html($(clone).html().replace(/-INDEX-/g, addon_index));
        $(clone).css('display', '');
        $(clone).find('.yith-ppwcpa-dropdown-buttton').on('click', display_body);
        $(clone).find('.yith-ppwcpa-options-container').mouseover(display_trash_button);
        $(clone).find('.yith-ppwcpa-options-container').mouseout(hide_trash_button);
        $(clone).find('.yith-ppwcpa-add-option').on('click', add_new_option);
        $(clone).find('.yith-ppwcpa-delete-buttton').on('click', delete_option);
        $(clone).find('.yith-ppwcpa-remove-addon-buttton').on('click', remove_addon);
        $(clone).find('.yith_ppwcpa_name_text').keyup(change_addon_title);
        $('#yith_ppwcpa_sortable_container').append(clone);
        $('#yith_ppwcpa_addons_index').val(parseInt($('#yith_ppwcpa_addons_index').val()) + 1);
    })
    $('.yith-ppwcpa-dropdown-buttton').on('click', display_body);
    $('.yith-ppwcpa-options-container').mouseover(display_trash_button);
    $('.yith-ppwcpa-options-container').mouseout(hide_trash_button);
    $('.yith-ppwcpa-add-option').on('click', add_new_option);
    $('.yith-ppwcpa-delete-buttton').on('click', delete_option);
    $('.yith-ppwcpa-remove-addon-buttton').on('click', remove_addon);
    $('.yith_ppwcpa_name_text').keyup(change_addon_title);


    $(document).on('woocommerce_variations_loaded', function () {
        $('#variable_product_options').find('.yith_ppwcpa_sortable_container').sortable();
        $('.yith_ppwcpa_new_addon').on('click', function () {
            var variation_id = $(this).attr('id').split(/(\d+)/)[1];
            addon_index = $('#yith_ppwcpa_addons_index_variant' + variation_id).val();
            var clone = $('#yith-ppwcpa-addon-container' + variation_id + '--INDEX-').clone();
            $(clone).attr('id', $(clone).attr('id').replace(/-INDEX-/g, addon_index));
            $(clone).html($(clone).html().replace(/-INDEX-/g, addon_index));
            $(clone).css('display', '');
            $(clone).find('.yith-ppwcpa-dropdown-buttton').on('click', display_body);
            $(clone).find('.yith-ppwcpa-options-container').mouseover(display_trash_button);
            $(clone).find('.yith-ppwcpa-options-container').mouseout(hide_trash_button);
            $(clone).find('.yith-ppwcpa-add-option').on('click', add_new_option);
            $(clone).find('.yith-ppwcpa-delete-buttton').on('click', delete_option);
            $(clone).find('.yith-ppwcpa-remove-addon-buttton').on('click', remove_addon);
            $(clone).find('.yith_ppwcpa_name_text').keyup(change_addon_title);
            $('#yith_ppwcpa_sortable_container_variant' + variation_id).append(clone);
            $('#yith_ppwcpa_addons_index_variant' + variation_id).val(parseInt($('#yith_ppwcpa_addons_index_variant' + variation_id).val()) + 1);
        })
        $('#variable_product_options').find('.yith-ppwcpa-dropdown-buttton').on('click', display_body);
        $('#variable_product_options').find('.yith-ppwcpa-options-container').mouseover(display_trash_button);
        $('#variable_product_options').find('.yith-ppwcpa-options-container').mouseout(hide_trash_button);
        $('#variable_product_options').find('.yith-ppwcpa-add-option').on('click', add_new_option);
        $('#variable_product_options').find('.yith-ppwcpa-delete-buttton').on('click', delete_option);
        $('#variable_product_options').find('.yith-ppwcpa-remove-addon-buttton').on('click', remove_addon);
        $('#variable_product_options').find('.yith_ppwcpa_name_text').keyup(change_addon_title);

    })

});

