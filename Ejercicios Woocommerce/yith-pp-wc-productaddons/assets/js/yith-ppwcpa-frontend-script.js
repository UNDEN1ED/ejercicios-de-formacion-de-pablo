price_onkeyup = function () {
    var addon_id = jQuery(this).attr('id').split(/(\d+)/)[1];
    let price = jQuery('#yith_ppwcpa_declared_price' + addon_id).val();
    let price_stg = jQuery('#yith_ppwcpa_declared_price_stgs' + addon_id).val();
    let free_chars = jQuery('#yith_ppwcpa_declared_free_chars' + addon_id).val();
    if (jQuery(this).val().trim() != "") {
        if (jQuery(this).val().length > free_chars) {
            if ('fixed-price' == price_stg) {
                jQuery('#yith_ppwcpa_addon_price' + addon_id).html(parseFloat(price).toFixed(2).replace(/\./g, ','));
            } else if ('per-character' == price_stg) {
                let total_chars_price = 0;
                for (i = 0; i < jQuery(this).val().length - free_chars; i++) {
                    total_chars_price += parseFloat(price);
                }
                jQuery('#yith_ppwcpa_addon_price' + addon_id).html(total_chars_price.toFixed(2).replace(/\./g, ','));
            }
        } else {
            jQuery('#yith_ppwcpa_addon_price' + addon_id).html(0);
        }



    } else {
        jQuery('#yith_ppwcpa_addon_price' + addon_id).html(0);
    }

    if (jQuery('#yith_ppwcpa_addon_price' + addon_id).html() != 0) {
        jQuery('#yith_ppwcpa_addon_currency' + addon_id).html('+ ');
        jQuery('#yith_ppwcpa_hidden_price' + addon_id).val(jQuery('#yith_ppwcpa_addon_price' + addon_id).html().replace(/\,/g, '.'));
    } else {
        jQuery('#yith_ppwcpa_addon_currency' + addon_id).html('');
        jQuery('#yith_ppwcpa_hidden_price' + addon_id).val('');
    }

    total_price();

}

price_onclick = function () {

    var addon_id = jQuery(this).attr('id').split(/(\d+)/)[1];
    var addon_price = parseFloat(jQuery(this).val());

    if (this.checked == true) {
        jQuery('#yith_ppwcpa_addon_currency' + addon_id).html('+ ');

        jQuery('#yith_ppwcpa_hidden_price' + addon_id).val(addon_price.toFixed(2));
        jQuery('#yith_ppwcpa_addon_price' + addon_id).html(jQuery('#yith_ppwcpa_hidden_price' + addon_id).val().replace(/\./g, ','));
    } else {
        jQuery('#ith_ppwcpa_addon_currency' + addon_id).html('');
        jQuery('#yith_ppwcpa_addon_price' + addon_id).html(0);
        jQuery('#yith_ppwcpa_hidden_price' + addon_id).val('');
    }
    total_price();

}

options_onchange = function () {
    var addon_id = jQuery(this).attr('name').split(/(\d+)/)[1];
    if (jQuery(this).val() != '') {
        var option = jQuery(this).val().split(',');
        jQuery('#yith_ppwcpa_hidden_option_name' + addon_id).val(option[0]);
        jQuery('#yith_ppwcpa_hidden_option_price' + addon_id).val(option[1]);
    } else {
        jQuery('#yith_ppwcpa_hidden_option_name' + addon_id).val('');
        jQuery('#yith_ppwcpa_hidden_option_price' + addon_id).val('');
    }
    total_price();
}

function total_price() {

    var addon_values = jQuery('.yith_ppwcpa_hidden_price');
    var product_price = jQuery('#yith_ppwcpa_product_price').html();
    var total_value = parseFloat(0);

    for (i = 0; i < addon_values.length; i++) {
        if (addon_values.eq(i).val() != '' && addon_values.eq(i).val() != 'Free') {
            total_value += parseFloat(jQuery(addon_values[i]).val());
        }
    }
    jQuery('#yith_ppwcpa_total_addons_price').html(total_value.toFixed(2).replace(/\./g, ','));

    var total_summary = parseFloat(total_value) + parseFloat(product_price);
    jQuery('#yith_ppwcpa_total_price').html(total_summary.toFixed(2).replace(/\./g, ','));
}

jQuery(document).ready(function ($) {
    $('#yith_ppwcpa_addons_container').find('input[type=checkbox]').each(price_onclick);
    $(document).on('found_variation', function (event, variation) {

        variation_id = variation.variation_id;
        jQuery.ajax({
            url: yith_ppwcpa_ajax.ajaxurl,
            type: "POST",
            data: {
                action: "variation",
                variant_id: variation_id,
            },
            error: function (response) {
                console.log('Nop');
            },
            success: function (response) {
                let last_element_id = parseInt($('#yith_ppwcpa_addons_container').find('input[type=hidden]:last').attr('id').split(/(\d+)/)[1]) + 1;
                $('#yith_ppwcpa_variant_addons_container').html(response['addons']);
                $('#yith_ppwcpa_variant_addons_container').find('.yith_ppwcpa_container').each(function () {
                    $(this).html($(this).html().replace(/-INDEX-/g, last_element_id));
                    last_element_id++;
                });
                $('#yith_ppwcpa_variant_addons_container').find('input[type=checkbox]').each(price_onclick);
                $('#yith_ppwcpa_variant_addons_container').find('input[type=text], textarea').keyup(price_onkeyup);
                $('#yith_ppwcpa_variant_addons_container').find('input[type=checkbox]').on('click', price_onclick);
                $('#yith_ppwcpa_variant_addons_container').find('select, input[type=radio]').change(options_onchange);
            }
        });

    });

    $('#yith_ppwcpa_addons_container').find('input[type=text], textarea').keyup(price_onkeyup);
    $('#yith_ppwcpa_addons_container').find('input[type=checkbox]').on('click', price_onclick);
    $('#yith_ppwcpa_addons_container').find('select, input[type=radio]').change(options_onchange);
});


