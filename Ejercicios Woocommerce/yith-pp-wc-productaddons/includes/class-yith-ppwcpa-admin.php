<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPA_Admin' ) ) {
	/**
	 * YITH_PPWCPA_Admin
	 */
	class YITH_PPWCPA_Admin {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPA_Admin
		 */
		private static $instance;

		/**
		 * Panel_ppwcpa_fields
		 *
		 * @var ARRAY
		 */
		private $panel_ppwcpa_fields;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPA_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'wc_purchase_notes_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_wc_purchase_notes_fields' ) );
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'display_wc_product_addons_variants' ), 10, 3 );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_product_addons_meta' ) );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'save_product_variant_addons_meta' ), 10, 2 );

		}

		/**
		 * Initialize_ppwcpa_panel_fields
		 *
		 * @param  mixed $addons saved addons.
		 * @param  mixed $variation_id ID of the varition.
		 * @return void
		 */
		private function initialize_ppwcpa_panel_fields( $addons = array( 'index' => '-INDEX-' ), $variation_id = '' ) {

			$this->panel_ppwcpa_fields = array(
				array(
					'type'        => 'text',
					'label'       => __( 'Name' ),
					'id'          => ( ( '' !== $variation_id ) ? 'yith_ppwcpa_name_text' . $variation_id . '-' : 'yith_ppwcpa_name_text' ) . $addons['index'],
					'input_class' => 'yith_ppwcpa_name_text',
					'name'        => ( ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa' ) . '[' . $addons['index'] . '][name]',
					'form_field'  => true,
					'value'       => ! empty( $addons['name'] ) ? $addons['name'] : null,
				),
				array(
					'type'        => 'text-area',
					'label'       => __( 'Description' ),
					'id'          => ( ( '' !== $variation_id ) ? 'yith_ppwcpa_desc_text' . $variation_id . '-' : 'yith_ppwcpa_desc_text' ) . $addons['index'],
					'input_class' => 'yith_ppwcpa_desc_text',
					'name'        => ( ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa' ) . '[' . $addons['index'] . '][description]',
					'value'       => ! empty( $addons['description'] ) ? $addons['description'] : null,
				),
				array(
					'type'          => 'select',
					'label'         => __( 'Field Type' ),
					'id'            => ( ( '' !== $variation_id ) ? 'yith_ppwcpa_type_select' . $variation_id . '-' : 'yith_ppwcpa_type_select' ) . $addons['index'],
					'input_class'   => 'yith_ppwcpa_field_type_selec',
					'name'          => ( ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa' ) . '[' . $addons['index'] . '][field]',
					'on_change'     => 'show_addon_fields(this' . ( ( '' !== $variation_id ) ? ', true' : '' ) . ')',
					'select_values' => array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' ),
					'value'         => ! empty( $addons['field'] ) ? $addons['field'] : 'text',
				),
				array(
					'type'            => 'radio',
					'label'           => __( 'Price settings' ),
					'id'              => 'yith_ppwcpa_price_stgs',
					'input_class'     => 'yith_ppwcpa_price_stgs',
					'name'            => ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa',
					'input_index'     => $addons['index'],
					'variation_index' => ( '' !== $variation_id ) ? $variation_id : null,
					'radio_onclick'   => 'show_addon_fields(this' . ( ( '' !== $variation_id ) ? ', true' : '' ) . ')',
					'radio_values'    => array( 'free', 'fixed-price', 'per-character' ),
					'value'           => ! empty( $addons['price_stg'] ) ? $addons['price_stg'] : 'free',
				),
				array(
					'type'            => 'options',
					'label'           => __( 'Options' ),
					'id'              => 'yith_ppwcpa_options',
					'input_class'     => 'yith_ppwcpa_options',
					'name'            => ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa',
					'container_class' => 'yith_ppwcpa_option_container',
					'container_id'    => 'yith_ppwcpa_option_container',
					'input_index'     => $addons['index'],
					'variation_index' => ( '' !== $variation_id ) ? $variation_id : null,
					'values'          => ! empty( $addons['options'] ) ? $addons['options'] : null,
					'radio_values'    => array( 'name', 'price' ),
				),

				array(
					'type'        => 'number',
					'label'       => __( 'Price' ),
					'id'          => ( ( '' !== $variation_id ) ? 'yith_ppwcpa_price' . $variation_id . '-' : 'yith_ppwcpa_price' ) . $addons['index'],
					'input_class' => 'yith_ppwcpa_price',
					'name'        => ( ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa' ) . '[' . $addons['index'] . '][price]',
					'min'         => 0,
					'value'       => ! empty( $addons['price'] ) ? $addons['price'] : 0,
				),
				array(
					'type'        => 'number',
					'label'       => __( 'Free characters' ),
					'id'          => ( ( '' !== $variation_id ) ? 'yith_ppwcpa_free_characters' . $variation_id . '-' : 'yith_ppwcpa_free_characters' ) . $addons['index'],
					'input_class' => 'yith_ppwcpa_free_characters',
					'name'        => ( ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa' ) . '[' . $addons['index'] . '][free_chars]',
					'min'         => 0,
					'value'       => isset( $addons['free_chars'] ) ? intval( $addons['free_chars'] ) : 0,
				),
				array(
					'type'        => 'enabler',
					'label'       => __( 'Default Enabled' ),
					'id'          => ( ( '' !== $variation_id ) ? 'yith_ppwcpa_enabled_default' . $variation_id . '-' : 'yith_ppwcpa_enabled_default' ) . $addons['index'],
					'input_class' => 'yith_ppwcpa_enabled_default_label',
					'name'        => ( ( '' !== $variation_id ) ? '_yith_ppwcpa-variant[' . $variation_id . ']' : '_yith_ppwcpa' ) . '[' . $addons['index'] . '][default_enable]',
					'value'       => ! empty( $addons['default_enable'] ) ? $addons['default_enable'] : null,

				),

			);
		}

		/**
		 * Wc_purchase_notes_tab
		 *
		 * @param  mixed $tabs re.
		 * @return ARRAY
		 */
		public function wc_purchase_notes_tab( $tabs ) {
			// Adds the new tab.
			$tabs['product-notes'] = array(
				'label'    => __( 'Add-ons', 'yith-ppwc-productaddons' ),
				'target'   => 'yith_ppwcpa_panel',
				'class'    => array( 'show_if_simple', 'show_if_variable' ),
				'priority' => 84,
			);

			return $tabs;
		}
		/**
		 * Display_wc_purchase_notes_fields
		 *
		 * @return void
		 */
		public function display_wc_purchase_notes_fields() {
			global $post;

			$product_addons = get_post_meta( $post->ID, '_yith_ppwcpa_product_addons', true );
			echo '<div id="yith_ppwcpa_panel" class="panel woocommerce_options_panel yith_ppwcpa_panel">';
			yith_ppwcpa_printer()->print_input_field(
				array(
					'type'        => 'button',
					'id'          => 'yith_ppwcpa_new_addon',
					'input_class' => 'button-primary',
					'name'        => '_yith_ppwcpa_new_addon',
					'value'       => __( 'NEW ADD-ON', 'yith-pp-wcnotes' ),

				)
			);
			echo '<div class="yith_ppwcpa_sortable_container" id="yith_ppwcpa_sortable_container">';
			$this->initialize_ppwcpa_panel_fields();
			yith_ppwcpa_get_view( '/admin/yith-ppwcpa-product-addon.php', $this->panel_ppwcpa_fields );
			if ( false !== $product_addons && isset( $product_addons ) ) {
				foreach ( $product_addons as $product_addon ) {
					$this->initialize_ppwcpa_panel_fields( $product_addon );
					yith_ppwcpa_get_view( '/admin/yith-ppwcpa-product-addon.php', $this->panel_ppwcpa_fields, $product_addon );
				}
			}
			echo '</div>';
			echo '<input type="hidden" name="_yith_ppwcpa_addons_index" id="yith_ppwcpa_addons_index" value="' . ( ( isset( $product_addons ) && ! empty( $product_addons ) ) ? esc_html( end( $product_addons )['index'] + 1 ) : '' ) . '">';
			echo '</div>';

		}

		/**
		 * Display_wc_product_addons_variants
		 *
		 * @param  mixed $loop variation ID.
		 * @param  mixed $variation_data all data refered to the variation.
		 * @param  mixed $variation Variation Object.
		 * @return void
		 */
		public function display_wc_product_addons_variants( $loop, $variation_data, $variation ) {
			$variation_product_addons = get_post_meta( $variation->ID, '_yith_ppwcpa-variant', true );
			$this->initialize_ppwcpa_panel_fields( array( 'index' => '-INDEX-' ), $loop );
			echo '<div id="yith_ppwcpa_panel_variant' . esc_html( $loop ) . '" class="woocommerce_options_panel yith_ppwcpa_variation_container">';
			yith_ppwcpa_printer()->print_input_field(
				array(
					'type'        => 'button',
					'id'          => 'yith_ppwcpa_new_addon' . $loop,
					'input_class' => 'button-primary yith_ppwcpa_new_addon',
					'name'        => '_yith_ppwcpa_new_addon',
					'value'       => __( 'NEW ADD-ON', 'yith-pp-wcnotes' ),

				)
			);
			echo '<div class="yith_ppwcpa_sortable_container" id="yith_ppwcpa_sortable_container_variant' . esc_html( $loop ) . '">';
			yith_ppwcpa_get_view( '/admin/yith-ppwcpa-product-addon.php', $this->panel_ppwcpa_fields, null, $loop );
			if ( ! empty( $variation_product_addons ) && isset( $variation_product_addons ) ) {
				wp_localize_script( 'yith_ppwcpa_admin_script', 'addons_quantity_variation' . $loop, strval( end( $variation_product_addons )['index'] ) );
				foreach ( $variation_product_addons as $variation_product_addon ) {
					$this->initialize_ppwcpa_panel_fields( $variation_product_addon, $loop );
					yith_ppwcpa_get_view( '/admin/yith-ppwcpa-product-addon.php', $this->panel_ppwcpa_fields, $variation_product_addon, $loop );
				}
			}
			echo '</div>';
			echo '<input type="hidden" name="_yith_ppwcpa_addons_index" id="yith_ppwcpa_addons_index_variant' . esc_html( $loop ) . '" value="' . ( ( isset( $variation_product_addons ) && ! empty( $variation_product_addons ) ) ? esc_html( end( $variation_product_addons )['index'] + 1 ) : '' ) . '">';
			echo '</div>';

		}

		/**
		 * Save_product_addons_meta
		 *
		 * @param  mixed $product Object product.
		 * @return void
		 */
		public function save_product_addons_meta( $product ) {
            if ( array_key_exists( '_yith_ppwcpa', $_POST ) ) {//phpcs:ignore
                $array_order_list = $_POST['_yith_ppwcpa_addon_id'];//phpcs:ignore
				array_shift( $array_order_list );
                $addons      = $_POST['_yith_ppwcpa'];//phpcs:ignore
				$addons_length = count( $array_order_list );
				for ( $i = 0; $i < $addons_length; $i++ ) {
					$ordered_addons[ $i ]          = $addons[ $array_order_list[ $i ] ];
					$ordered_addons[ $i ]['index'] = $i;
					array_shift( $ordered_addons[ $i ]['options']['name'] );
					array_shift( $ordered_addons[ $i ]['options']['price'] );
					if ( empty( $ordered_addons[ $i ]['options']['name'] ) ) {
						$ordered_addons[ $i ]['options'] = '';
					}
				}
			}
			$product->update_meta_data( '_yith_ppwcpa_product_addons', $ordered_addons );

		}

		/**
		 * Save_product_variant_addons_meta
		 *
		 * @param  mixed $variation Object varition.
		 * @param  mixed $i position in the loop.
		 * @return void
		 */
		public function save_product_variant_addons_meta( $variation, $i ) {
            if ( array_key_exists( '_yith_ppwcpa-variant', $_POST ) ) {//phpcs:ignore
                $array_order_list = $_POST['_yith_ppwcpa_addon_id'.$i];//phpcs:ignore
				array_shift( $array_order_list );
                $addons      = $_POST['_yith_ppwcpa-variant'][$i];//phpcs:ignore
				$addons_length = count( $array_order_list );
				for ( $i = 0; $i < $addons_length; $i++ ) {
					$ordered_addons[ $i ]          = $addons[ $array_order_list[ $i ] ];
					$ordered_addons[ $i ]['index'] = $i;
					array_shift( $ordered_addons[ $i ]['options']['name'] );
					array_shift( $ordered_addons[ $i ]['options']['price'] );
					if ( empty( $ordered_addons[ $i ]['options']['name'] ) ) {
						$ordered_addons[ $i ]['options'] = '';
					}
				}

				$variation->update_meta_data( '_yith_ppwcpa-variant', $ordered_addons );
			}

		}
		/**
		 * Enqueue_admin_styles
		 *
		 * @return void
		 */
		public function enqueue_admin_styles() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( $product ) {

				wp_enqueue_style( 'yith_ppwcpa_admin_stylesheet', YITH_PPWCPA_DIR_ASSETS_CSS_URL . '/yith-ppwcpa-admin-style.css', array(), '1.0.0' );
				wp_enqueue_script( 'jquery' );
				wp_enqueue_script( 'yith_ppwcpa_admin_script', YITH_PPWCPA_DIR_ASSETS_JS_URL . '/yith-ppwcpa-admin-script.js', array( 'jquery' ), '1.0.0', true );
			}
		}
	}
}
