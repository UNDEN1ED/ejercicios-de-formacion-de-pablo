<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPA_AJAX' ) ) {
	/**
	 * YITH_PPWCPA_AJAX
	 */
	class YITH_PPWCPA_AJAX {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPA_AJAX
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPA_AJAX
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_ajax_nopriv_variation', array( $this, 'variation_change_ajax' ) );
			add_action( 'wp_ajax_variation', array( $this, 'variation_change_ajax' ) );
		}

		/**
		 * Variation_change_ajax
		 *
		 * @return void
		 */
		public function variation_change_ajax() {
			$addons = isset( $_POST['variant_id'] ) ? get_post_meta( $_POST['variant_id'], '_yith_ppwcpa-variant', true ) : 'false';//phpcs:ignore
			if ( $addons ) {
				ob_start();
				foreach ( $addons as $addon ) {
					if ( isset( $addon['enable'] ) && 'on' === $addon['enable'] ) {
						yith_ppwcpa_get_view( '/frontend/addons/' . $addon['field'] . '.php', array(), $addon, $_POST['variant_id'] );//phpcs:ignore
					}
				}
				wp_send_json( array( 'addons' => ob_get_clean() ) );
			} else {
				wp_send_json( array( 'addons' => '' ) );
			}
			wp_die();
		}
	}
}
