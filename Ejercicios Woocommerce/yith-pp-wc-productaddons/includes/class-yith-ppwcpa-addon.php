<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPA' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPA_Addon' ) ) {
	/**
	 * YITH_PPWCPA_Addon
	 */
	class YITH_PPWCPA_Addon {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPA_Addon
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPA_Addon
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

		}
	}
}
