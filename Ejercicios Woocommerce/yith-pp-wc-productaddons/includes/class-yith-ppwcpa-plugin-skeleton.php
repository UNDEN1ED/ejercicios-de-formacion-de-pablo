<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPA_Plugin_Skeleton' ) ) {
	/**
	 * YITH_PPWCPA_Plugin_Skeleton
	 */
	class YITH_PPWCPA_Plugin_Skeleton {

		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPA_Plugin_Skeleton
		 */
		private static $instance;

		/**
		 * Frontend instance
		 *
		 * @var YITH_PPWCPA_Frontend
		 */
		public $frontend;

		/**
		 * Admin instance
		 *
		 * @var YITH_PPWCPA_Admin
		 */
		public $admin;

		/**
		 * Ajax instance
		 *
		 * @var YITH_PPWCPA_AJAX
		 */
		public $ajax;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPA_Plugin_Skeleton
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			$require = apply_filters(
				'yith_ppwcpa_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-ppwcpa-printer.php',
						'includes/class-yith-ppwcpa-ajax.php',

					),
					'frontend' => array(
						'includes/class-yith-ppwcpa-frontend.php',
					),
					'admin'    => array(
						'includes/class-yith-ppwcpa-admin.php',

					),
				)
			);

			$this->require_( $require );

			$this->init_classes();

			$this->init();
		}

		/**
		 *
		 * Require_()
		 *
		 * @param mixed $main_classes each document of the folder include.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_PPWCPA_DIR_PATH . $class ) ) {
						require_once YITH_PPWCPA_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			$this->ajax = YITH_PPWCPA_AJAX::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( ! is_admin() ) {
				$this->frontend = YITH_PPWCPA_Frontend::get_instance();
			}
			if ( is_admin() ) {
				$this->admin = YITH_PPWCPA_Admin::get_instance();
			}
		}
	}
}

if ( ! function_exists( 'yith_ppwcpa_plugin_skeleton' ) ) {
	/**
	 * Yith_ppwcpa_plugin_skeleton
	 *
	 * @return YITH_PPWCPA_Plugin_Skeleton
	 */
	function yith_ppwcpa_plugin_skeleton() {
		return YITH_PPWCPA_Plugin_Skeleton::get_instance();
	}
}
