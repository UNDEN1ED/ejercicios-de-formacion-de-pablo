<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

use Automattic\WooCommerce\Blocks\StoreApi\Routes\CartItems;

if ( ! defined( 'YITH_PPWCPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPA_Frontend' ) ) {
	/**
	 * YITH_PPWCPA_Frontend
	 */
	class YITH_PPWCPA_Frontend {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPA_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPA_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'woocommerce_before_add_to_cart_quantity', array( $this, 'yith_ppwcpa_addons_setup' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_ppwcpa_enqueue_frontend_styles' ) );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_addon_to_cart_meta' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'display_product_addon_cart' ), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'save_cart_item_product_addon_as_order_item_meta' ), 10, 4 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'add_addon_price_to_product' ) );

		}



		/**
		 * Addons_setup
		 *
		 * @return void
		 */
		public function yith_ppwcpa_addons_setup() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( $product instanceof WC_Product_Variable ) {
				$variations = $product->get_available_variations( 'objects' );
				foreach ( $variations as $variation ) {
					$variation_addons[] = $variation->get_meta( '_yith_ppwcpa-variant' );

				}
			}
			$product_addons = get_post_meta( $post->ID, '_yith_ppwcpa_product_addons', true );
			yith_ppwcpa_get_view( '/frontend/yith-ppwcpa-frontend-product-addon.php', $product_addons );
		}

		/**
		 * Enqueue_admin_styles
		 *
		 * @return void
		 */
		public function yith_ppwcpa_enqueue_frontend_styles() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( $product ) {
				wp_enqueue_style( 'yith_ppwcpa_frontend_stylesheet', YITH_PPWCPA_DIR_ASSETS_CSS_URL . '/yith-ppwcpa-frontend-style.css', array(), '1.0.0' );
				wp_enqueue_script( 'yith_ppwcpa_frontend_script', YITH_PPWCPA_DIR_ASSETS_JS_URL . '/yith-ppwcpa-frontend-script.js', array( 'jquery' ), '1.0.0', true );
				wp_localize_script( 'yith_ppwcpa_frontend_script', 'yith_ppwcpa_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			}
		}

		/**
		 * Add_addon_to_cart_meta
		 *
		 * @param  mixed $cart_item_data Cart data.
		 * @param  mixed $product_id    ID of the product.
		 * @param  mixed $variation_id  ID of the variation.
		 * @return ARRAY
		 */
		public function add_addon_to_cart_meta( $cart_item_data, $product_id, $variation_id ) {
				$product = wc_get_product( $product_id );

            if ( array_key_exists( 'yith_ppwcpa_name', $_POST ) && array_key_exists( 'yith_ppwcpa_price', $_POST ) && array_key_exists( 'yith_ppwcpa_input', $_POST ) ) {// phpcs:ignore
				$addons_name        = $_POST['yith_ppwcpa_name'];//phpcs:ignore
                $addons_input       = $_POST['yith_ppwcpa_input'];//phpcs:ignore
                $addons_price       = $_POST['yith_ppwcpa_price'];//phpcs:ignore
                $addons_list_length = count( $addons_name );//phpcs:ignore

				$cart_item_data['yith_ppwcpa'][0] = array(
					'name'  => __( 'Base Price' ),
					'price' => $product->get_price() . ',00',

				);
				for ( $i = 0; $i < $addons_list_length; $i++ ) {
					if ( '' !== $addons_price [ $i ] ) {
						$cart_item_data['yith_ppwcpa'][] = array(
							'name'  => $addons_name[ $i ],
							'price' => $addons_price [ $i ],
							'input' => $addons_input[ $i ],
						);
					}
				}
			}
			return $cart_item_data;
		}

		/**
		 * Display_product_addon_cart
		 *
		 * @param  mixed $item_data  Cart product data.
		 * @param  mixed $cart_item Cart Product meta.
		 * @return ARRAY
		 */
		public function display_product_addon_cart( $item_data, $cart_item ) {
			if ( isset( $cart_item['yith_ppwcpa'] ) ) {
				foreach ( $cart_item['yith_ppwcpa'] as $addon ) {
					if ( isset( $addon['input'] ) && 'on' !== $addon['input'] ) {
						$item_data[] = array(
							'key'   => $addon['name'],
							'value' => '\' ' . $addon['input'] . ' \' (' . get_woocommerce_currency_symbol() . $addon['price'] . ')',
						);
					} else {
						$item_data[] = array(
							'key'   => $addon['name'],
							'value' => '(' . get_woocommerce_currency_symbol() . $addon['price'] . ')',
						);
					}
				}
			}

			return $item_data;

		}



		/**
		 * Save_cart_item_product_addon_as_order_item_meta
		 *
		 * @param  mixed $item Item.
		 * @param  mixed $cart_item_key Cart.
		 * @param  mixed $values Value.
		 * @param  mixed $order Order.
		 * @return void
		 */
		public function save_cart_item_product_addon_as_order_item_meta( $item, $cart_item_key, $values, $order ) {
			if ( isset( $values['yith_ppwcpa'] ) ) {
				foreach ( $values['yith_ppwcpa'] as $value ) {
					if ( isset( $value['input'] ) ) {
						if ( 'on' !== $value['input'] ) {
							$item->update_meta_data( $value['name'], '\' ' . $value['input'] . ' \' (' . get_woocommerce_currency_symbol() . $value['price'] . ')' );

						} else {
							$item->update_meta_data( $value['name'], '(' . get_woocommerce_currency_symbol() . $value['price'] . ')' );
						}
					} else {
						$item->update_meta_data( $value['name'], $value['price'] );
					}
				}
			}
		}

		/**
		 * Add_addon_price_to_product
		 *
		 * @param  mixed $cart_object WC_CART.
		 * @return void
		 */
		public function add_addon_price_to_product( $cart_object ) {
			$cart_items = $cart_object->get_cart();
			foreach ( $cart_items as $cart_item ) {
				$total_addon_price = 0.00;
				if ( isset( $cart_item['yith_ppwcpa'] ) ) {
					foreach ( $cart_item['yith_ppwcpa'] as $addon ) {
						if ( 'Free' !== $addon['price'] ) {
							$total_addon_price += floatval( $addon['price'] );
						}
					}
				}
				$cart_item['data']->set_price( $total_addon_price );
			}

		}

	}
}
