<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCPA_Printer' ) ) {
	/**
	 * YITH_PPPN_Printer
	 */
	class YITH_PPWCPA_Printer {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCPA_Printer
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCPA_Printer
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
		}


		/**
		 * Print_input_field
		 *
		 * @param  mixed $field_data params of the input.
		 * @param  mixed $index  addon_index.
		 * @return void
		 */
		public function print_input_field( $field_data, $index = null ) {
			$default    = array(
				'label'           => '',
				'name'            => '',
				'id'              => '',
				'type'            => 'text',
				'container_class' => '',
				'container_id'    => '',
				'label_class'     => '',
				'input_class'     => '',
				'options'         => array(),
			);
			$field_data = array_merge( $default, $field_data );
			if ( array_key_exists( 'type', $field_data ) ) {
				yith_ppwcpa_get_view( '/input-fields/' . $field_data['type'] . '.php', $field_data, $index );
			}
		}

		/**
		 * Print_input_fields
		 *
		 * @param  mixed $fields_data params of the input.
		 * @param  mixed $index  addon_index.
		 * @return void
		 */
		public function print_input_fields( $fields_data, $index = null ) {
			foreach ( $fields_data as $field_data ) {
				$this->print_input_field( $field_data, $index );
			}

		}
	}
}


if ( ! function_exists( 'yith_ppwcpa_printer' ) ) {

	/**
	 * Yith_ppwcpa_printer
	 *
	 * @return YITH_PPWCPA_Printer
	 */
	function yith_ppwcpa_printer() {
		return YITH_PPWCPA_Printer::get_instance();

	}
}
