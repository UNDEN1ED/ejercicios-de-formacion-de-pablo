<?php
/**
 *
 * Common Functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_ppwcpa_get_view' ) ) {

	/**
	 * Yith_ppwcpa_get_view
	 *
	 * @param  mixed $file_name name of the file.
	 * @param  mixed $args array with the details.
	 * @param  mixed $addon addon fields.
	 * @param  mixed $variant_id index of the variation.
	 * @return void
	 */
	function yith_ppwcpa_get_view( $file_name, $args = array(), $addon = null, $variant_id = null ) {
		$full_path = YITH_PPWCPA_DIR_VIEWS_PATH . $file_name;

		include $full_path;
	}
}
