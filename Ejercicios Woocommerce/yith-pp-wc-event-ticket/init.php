<?php
/**
 * Plugin Name: YITH WC Product Event Ticket
 * Plugin URI: http://local.wordpress.test/
 * Description: Event Tickets
 * Author: Pablo Pérez Arteaga
 * Text Domain: yith-ppwc-eventticket
 * Version: 1.7.2
 * Author URI:
 *
 * @package WordPress
 */

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	define( 'YITH_PPWCET_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PPWCET_DIR_URL' ) ) {
	define( 'YITH_PPWCET_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCET_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PPWCET_DIR_ASSETS_URL', YITH_PPWCET_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PPWCET_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PPWCET_DIR_ASSETS_CSS_URL', YITH_PPWCET_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PPWCET_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PPWCET_DIR_ASSETS_JS_URL', YITH_PPWCET_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PPWCET_DIR_PATH' ) ) {
	define( 'YITH_PPWCET_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PPWCET_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PPWCET_DIR_INCLUDES_PATH', YITH_PPWCET_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PPWCET_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PPWCET_DIR_TEMPLATES_PATH', YITH_PPWCET_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PPWCET_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PPWCET_DIR_VIEWS_PATH', YITH_PPWCET_DIR_PATH . 'views' );
}
if ( ! defined( 'YITH_PPWCET_DIR_VENDOR_PATH' ) ) {
	define( 'YITH_PPWCET_DIR_VENDOR_PATH', YITH_PPWCET_DIR_PATH . 'vendor' );
}

if ( ! function_exists( 'yith_ppwcet_init_classes' ) ) {

	/**
	 * Yith_ppwcet_init_classes
	 *
	 * @return void
	 */
	function yith_ppwcet_init_classes() {

		load_plugin_textdomain( 'yith-ppwc-eventticket', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PPWCET_DIR_INCLUDES_PATH . '/class-yith-ppwcet-plugin-skeleton.php';

		if ( class_exists( 'YITH_PPWCET_Plugin_Skeleton' ) ) {

			yith_ppwcet_plugin_skeleton();

		}
	}
}

add_action( 'plugins_loaded', 'yith_ppwcet_init_classes', 11 );
