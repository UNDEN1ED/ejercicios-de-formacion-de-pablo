jQuery(document).ready(function ($) {

    $('.yith_ppwcet_search_button').on('click', function () {
        var input_id = $('#yith_ppwcet_search_input').val();
        if (input_id != '') {
            $('.yith_ppwcet_ticket_id').each(function () {
                let post_id = $(this).html().split(/(\#\d+)/)[1];

                if (post_id.indexOf(input_id) > -1) {
                    $(this).parent().parent().css('display', '');
                } else {
                    $(this).parent().parent().css('display', 'none');
                }
            });
        } else {
            $('.yith_ppwcet_ticket_id').parent().parent().css('display', '');
        }
    });

    $('.yith_ppwcet_info_button').click(function () {
        let id = $(this).attr('id').split(/(\d+)/)[1];
        if ($(this).hasClass('fa-caret-up')) {
            $('#yith_ppwect_row_info-' + id).css('display', '');
            $(this).removeClass('fa-caret-up');
            $(this).addClass('fa-caret-down');
        } else {
            $('#yith_ppwect_row_info-' + id).css('display', 'none');
            $(this).removeClass('fa-caret-down');
            $(this).addClass('fa-caret-up');
        }

    });

    $('.yith_ppwcet_check_button').on('click', function () {
        let id = $(this).attr('id').split(/(\d+)/)[1];
        jQuery.ajax({
            url: yith_ppwcet_ajax.ajaxurl,
            type: "POST",
            data: {
                action: "ticket_used_front",
                event_id: id,
            },
            error: function (response) {
                console.log('Nop');
            },
            success: function (response) {
                if (response.post_status == 'yith-pending-check') {
                    $('#yith_ppwcet_ticket_status-' + id).removeClass('yith_pppwcet_status_symbol_checked');
                    $('#yith_ppwcet_ticket_status-' + id).addClass('yith_pppwcet_status_symbol');
                } else if (response.post_status == 'yith-checked-check') {
                    $('#yith_ppwcet_ticket_status-' + id).removeClass('yith_pppwcet_status_symbol');
                    $('#yith_ppwcet_ticket_status-' + id).addClass('yith_pppwcet_status_symbol_checked');
                }
            }
        });
    });

    $('.yith_ppwcet_pdf_button').on('click', function () {
        alert('Paso');
        let id = $(this).attr('id').split(/(\d+)/)[1];
        jQuery.ajax({
            url: yith_ppwcet_ajax.ajaxurl,
            type: "POST",
            data: {
                action: "print_ticket_pdf",
                event_id: id,
            },
            error: function (response) {
                console.log('Nop');
            },
            success: function (response) {

            }
        });
    });

});