jQuery(document).ready(function ($) {
    var product_quantity = $('.cart').find('input[type=number].input-text.qty').val();
    if (1 < product_quantity) {
        for (let i = 2; i <= product_quantity; i++) {
            var clone = $('#yith_ppwcet_container-INDEX-').clone();
            $(clone).find('input[type=text]').first().attr('name', 'yith_ppwcet_event_fields[-INDEX-][name]');
            $(clone).find('input[type=text]').last().attr('name', 'yith_ppwcet_event_fields[-INDEX-][surname]');
            $(clone).attr('id', $(clone).attr('id').replace(/-INDEX-/g, i));
            $(clone).html($(clone).html().replace(/-INDEX-/g, i));
            $(clone).css('display', 'block');

            $('#yith_ppwcet_event_fields_container').append(clone);
        }

    }

    $('.cart').on('change', 'input[type=number].input-text.qty', function () {
        var ticket_quantity = $('.yith_ppwcet_tickets').length - 1;
        var product_quantity = $(this).val();
        if (product_quantity > ticket_quantity) {
            for (let i = ticket_quantity; i < product_quantity; i++) {
                var clone = $('#yith_ppwcet_container-INDEX-').clone();
                $(clone).find('input[type=text]').first().attr('name', 'yith_ppwcet_event_fields[-INDEX-][name]');
                $(clone).find('input[type=text]').last().attr('name', 'yith_ppwcet_event_fields[-INDEX-][surname]');
                $(clone).attr('id', $(clone).attr('id').replace(/-INDEX-/g, i + 1));
                $(clone).html($(clone).html().replace(/-INDEX-/g, i + 1));
                $(clone).css('display', 'block');
                $('#yith_ppwcet_event_fields_container').append(clone);
            }
        } else if (product_quantity < ticket_quantity) {
            for (let i = product_quantity; i < ticket_quantity; i++) {
                $('#yith_ppwcet_event_fields_container').children().last().remove();
            }

        }
    });
})