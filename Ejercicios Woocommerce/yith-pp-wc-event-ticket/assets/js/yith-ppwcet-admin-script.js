jQuery(document).ready(function ($) {

    $('.yith_ppwcet_check_ticket').on('click', function () {
        var element_id = $(this).attr('id').split(/(\d+)/)[1];
        jQuery.ajax({
            url: yith_ppwcet_ajax.ajaxurl,
            type: "POST",
            data: {
                action: "ticket_used",
                ticket_id: element_id,
            },
            error: function () {
            },
            success: function (response) {
                console.log(response);
                if (response.post_status == 'yith-pending-check') {
                    $('#yith_ppwcet_post_status' + element_id).removeClass('yith_ppwcet_checked');
                    $('#yith_ppwcet_post_status' + element_id).addClass('yith_ppwcet_pending');
                } else if (response.post_status == 'yith-checked-check') {
                    $('#yith_ppwcet_post_status' + element_id).removeClass('yith_ppwcet_pending');
                    $('#yith_ppwcet_post_status' + element_id).addClass('yith_ppwcet_checked');
                }
            }

        });

    });

});
