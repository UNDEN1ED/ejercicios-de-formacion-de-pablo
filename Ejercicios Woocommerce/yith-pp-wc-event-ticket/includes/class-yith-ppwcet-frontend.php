<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCET_Frontend' ) ) {
	/**
	 * YITH_PPWCET_Frontend
	 */
	class YITH_PPWCET_Frontend {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'woocommerce_before_add_to_cart_quantity', array( $this, 'display_event_fields' ) );
			add_action( 'woocommerce_event_ticket_add_to_cart', array( $this, 'event_ticket_template' ) );
			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'purchase_add_to_cart_validation' ), 10, 2 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'display_product_addon_cart' ), 10, 2 );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_styles' ) );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'save_cart_item_event_ticket_as_order_item_meta' ), 10, 4 );
			add_action( 'woocommerce_add_to_cart_handler_event_ticket', array( $this, 'display_ticket_event_products_cart' ), 10, 1 );
			add_filter( 'woocommerce_order_item_display_meta_key', array( $this, 'show_as_string_order_item_meta_key' ), 10, 1 );
		}

		/**
		 * Display_event_fields
		 *
		 * @return void
		 */
		public function display_event_fields() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( 'event_ticket' === $product->get_type() ) {
				$required_list = get_post_meta( $post->ID, '_yith_ppwcet_required', true );
				echo '<div id="yith_ppwcet_event_fields_container">';
				yith_ppwcet_get_view( '/frontend/yith-ppwcet-front-fields-view.php', $required_list );
				echo '</div>';
			}

		}

		/**
		 * Event_ticket_template
		 *
		 * @return void
		 */
		public function event_ticket_template() {

			global $product;
			if ( 'event_ticket' === $product->get_type() ) {
				wc_get_template( 'single-product/add-to-cart/simple.php' );
			}

		}

		/**
		 * Purchase_add_to_cart_validation
		 *
		 * @param  mixed $passed signal for validation.
		 * @param  mixed $product_id ID of product.
		 * @return Boolean
		 */
		public function purchase_add_to_cart_validation( $passed, $product_id ) {

			$product       = wc_get_product( $product_id );
			$required_list = get_post_meta( $product_id, '_yith_ppwcet_required', true );
			$event_inputs  = $_POST['yith_ppwcet_event_fields'];//phpcs:ignore

			if ( 'event_ticket' === $product->get_type() ) {
				foreach ( $event_inputs as $event_input ) {

					if ( isset( $required_list['name'] ) && '' === $event_input['name'] ) {
						$passed = false;
						wc_add_notice( 'Name is required', 'error' );
					}
					if ( isset( $required_list['surname'] ) && '' === $event_input['surname'] ) {
						$passed = false;
						wc_add_notice( 'Surname is required', 'error' );
					}
				}
			}

			return $passed;
		}

		/**
		 * Display_product_addon_cart
		 *
		 * @param  mixed $item_data  Cart product data.
		 * @param  mixed $cart_item Cart Product meta.
		 * @return ARRAY
		 */
		public function display_product_addon_cart( $item_data, $cart_item ) {
			if ( isset( $cart_item['yith_ppwcet']['name'] ) ) {
				if ( isset( $cart_item['yith_ppwcet']['name'] ) ) {
					$item_data[] = array(
						'key'   => 'Name',
						'value' => $cart_item['yith_ppwcet']['name'],
					);
				}
				if ( isset( $cart_item['yith_ppwcet']['surname'] ) ) {
					$item_data[] = array(
						'key'   => 'Surname',
						'value' => $cart_item['yith_ppwcet']['surname'],
					);
				}
			}
			return $item_data;
		}


		/**
		 * Display_ticket_event_products_cart
		 *
		 * @return Boolean
		 */
		public function display_ticket_event_products_cart() {
			$quantity          = empty( $_REQUEST['quantity'] ) ? 1 : wc_stock_amount( wp_unslash( $_REQUEST['quantity'] ) );//phpcs:ignore
            $product_id =  $_REQUEST['add-to-cart'] ;//phpcs:ignore

			$tickets    = empty( $_REQUEST['yith_ppwcet_event_fields'] ) ? false : $_REQUEST['yith_ppwcet_event_fields'];//phpcs:ignore
			$tickets_len       = count( $tickets );
			$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

			if ( '' !== $quantity ) {
				for ( $i = 1; $i <= $tickets_len; $i++ ) {
					$cart_item_data['yith_ppwcet'] = array(
						'name'    => ( isset( $tickets[ $i ]['name'] ) ) ? $tickets[ $i ]['name'] : array(),
						'surname' => ( isset( $tickets[ $i ]['surname'] ) ) ? $tickets[ $i ]['surname'] : array(),
					);

					if ( false === $passed_validation || false === WC()->cart->add_to_cart( $product_id, 1, 0, array(), $cart_item_data ) ) {
						return false;
					}
				}
				wc_add_to_cart_message( array( $product_id => $quantity ), true );
				return true;
			} else {
				return false;
			}

		}

		/**
		 * Save_cart_item_event_ticket_as_order_item_meta
		 *
		 * @param  mixed $item Item.
		 * @param  mixed $cart_item_key Cart.
		 * @param  mixed $values Value.
		 * @param  mixed $order Order.
		 * @return void
		 */
		public function save_cart_item_event_ticket_as_order_item_meta( $item, $cart_item_key, $values, $order ) {

			$product = wc_get_product( $item->get_product_id() );
			if ( isset( $values['yith_ppwcet'] ) ) {
				$item->update_meta_data( '_yith_ppwcet_product_type', $product->get_type() );
				$item->update_meta_data( 'yith_ppwcet_name', $values['yith_ppwcet']['name'] );
				$item->update_meta_data( 'yith_ppwcet_surname', $values['yith_ppwcet']['surname'] );
			}
		}

		/**
		 * Show_as_string_order_item_meta_key
		 *
		 * @param  mixed $display_key Key of every Meta in Item.
		 * @return String
		 */
		public function show_as_string_order_item_meta_key( $display_key ) {

			if ( strpos( $display_key, 'yith_ppwcet' ) !== false ) {
				if ( 'yith_ppwcet_name' === $display_key ) {
					$display_key = esc_html__( 'Name', 'yith-ppwc-eventticket' );
				} elseif ( 'yith_ppwcet_surname' === $display_key ) {
					$display_key = esc_html__( 'Surname', 'yith-ppwc-eventticket' );
				}
			}
			return $display_key;
		}

		/**
		 * Enqueue_frontend_styles
		 *
		 * @return void
		 */
		public function enqueue_frontend_styles() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( $product && 'event_ticket' === $product->get_type() ) {
				wp_enqueue_style( 'yith_ppwcet_frontend_stylesheet', YITH_PPWCET_DIR_ASSETS_CSS_URL . '/yith-ppwcet-front-style.css', array(), '1.0.0' );
				wp_enqueue_script( 'yith_ppwcet_frontend_script', YITH_PPWCET_DIR_ASSETS_JS_URL . '/yith-ppwcet-front-script.js', array( 'jquery' ), '1.0.0', true );

			}
			wp_register_style( 'yith_ppwcet_shortcode_stylesheet', YITH_PPWCET_DIR_ASSETS_CSS_URL . '/yith-ppwcet-front-shortcode-style.css', array(), '1.0.0' );
			wp_register_script( 'yith_ppwcet_shortcode_script', YITH_PPWCET_DIR_ASSETS_JS_URL . '/yith-ppwcet-shortcode-script.js', array( 'jquery' ), '1.0.0', true );
			wp_register_style( 'yith-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', null, '1.0.0' );
			wp_enqueue_style( 'yith-fontawesome' );
		}

	}
}
