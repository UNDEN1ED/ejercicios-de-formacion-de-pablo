<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCET_Plugin_Skeleton' ) ) {
	/**
	 * YITH_PPWCET_Plugin_Skeleton
	 */
	class YITH_PPWCET_Plugin_Skeleton {

		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_Plugin_Skeleton
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_Plugin_Skeleton
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			$require = apply_filters(
				'yith_ppwcet_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-ppwcet-product-type.php',
						'includes/class-yith-ppwcet-ticket-post-type.php',
						'includes/class-yith-ppwcet-ajax.php',
						'includes/class-yith-ppwcet-shortcode.php',
					),
					'frontend' => array(
						'includes/class-yith-ppwcet-frontend.php',
					),
					'admin'    => array(
						'includes/class-yith-ppwcet-admin.php',

					),
				)
			);

			$this->require_( $require );

			$this->init_classes();

			$this->init();
		}

		/**
		 *
		 * Require_()
		 *
		 * @param mixed $main_classes each document of the folder include.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_PPWCET_DIR_PATH . $class ) ) {
						require_once YITH_PPWCET_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			YITH_PPWCET_Product_Type::get_instance();
			YITH_PPWCET_Ticket_Post_Type::get_instance();
			YITH_PPWCET_Shortcode::get_instance();
			YITH_PPWCET_AJAX::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( ! is_admin() ) {
				$this->frontend = YITH_PPWCET_Frontend::get_instance();
			}
			if ( is_admin() ) {
				$this->admin = YITH_PPWCET_Admin::get_instance();

			}
		}
	}
}

if ( ! function_exists( 'yith_ppwcet_plugin_skeleton' ) ) {
	/**
	 * Yith_ppwcet_plugin_skeleton
	 *
	 * @return YITH_PPWCET_Plugin_Skeleton
	 */
	function yith_ppwcet_plugin_skeleton() {
		return YITH_PPWCET_Plugin_Skeleton::get_instance();
	}
}
