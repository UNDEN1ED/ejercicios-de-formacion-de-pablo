<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCET_Shortcode' ) ) {
	/**
	 * YITH_PPWCET_Shortcode
	 */
	class YITH_PPWCET_Shortcode {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_Shortcode
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_Shortcode
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_ppwcet_add_custom_shortcode' ) );
		}

		/**
		 * Setup_shortcode
		 *
		 * @param  mixed $atts atributtes.
		 * @return String
		 */
		public function setup_shortcode( $atts ) {
			$a = shortcode_atts(
				array(
					'product_id' => '',
				),
				$atts
			);

			$args = array(
				'numberposts' => 10,
				'orderby'     => 'date',
				'order'       => 'DESC',
				'post_status' => 'any',
				'post_type'   => 'yith_event_ticket',
				'meta_key'   => '_yith_ppwcet_product_id',//phpcs:ignore
				'meta_value' => isset( $a['product_id'] ) && ! empty( $a['product_id'] ) ? $a['product_id'] : false,//phpcs:ignore
			);

			$posts_list     = get_posts( $args );
			$shortcode_args = array(
				'a'          => $a,
				'posts_list' => $posts_list,
			);

			ob_start();
			wp_enqueue_style( 'yith_ppwcet_shortcode_stylesheet' );
			if ( 0 !== count( $posts_list ) ) {
				yith_ppwcet_get_template( '/frontend/event-list-manager.php', $shortcode_args );
				wp_enqueue_script( 'yith_ppwcet_shortcode_script' );
				wp_localize_script( 'yith_ppwcet_shortcode_script', 'yith_ppwcet_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			} else {
				echo "<p class='yith_ppwcet_error'>There's an error with the product_id you supplied, check it is from a an ticket_event post type</p>";
			}

			return ob_get_clean();
		}

		/**
		 * Yith_ppwcet_add_custom_shortcode
		 *
		 * @return void
		 */
		public function yith_ppwcet_add_custom_shortcode() {
			add_shortcode( 'yith_ppwcet_shortcode_grid', array( $this, 'setup_shortcode' ) );
		}

	}
}
