<?php
/**
 *
 * Product Type Type Class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCET_Product_Type' ) ) {
	/**
	 * YITH_PPWCET_Product_Type
	 */
	class YITH_PPWCET_Product_Type {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_Product_Type
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_Product_Type
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'product_type_selector', array( $this, 'add_custom_product_type' ) );
			add_action( 'init', array( $this, 'create_custom_product_type' ) );
			add_filter( 'woocommerce_product_class', array( $this, 'woocommerce_product_class' ), 10, 2 );
			add_action( 'admin_footer', array( $this, 'event_ticket_product_admin_custom_js' ) );
		}

		/**
		 * Add_custom_product_type
		 *
		 * @param  mixed $types Product Types.
		 * @return Array
		 */
		public function add_custom_product_type( $types ) {
			$types['event_ticket'] = 'Event-Ticket';
			return $types;
		}

		/**
		 * Create_custom_product_type
		 *
		 * @return void
		 */
		public function create_custom_product_type() {
			require_once YITH_PPWCET_DIR_PATH . 'includes/class-wc-product-event-ticket.php';
		}

		/**
		 * Woocommerce_product_class
		 *
		 * @param  mixed $classname cosa.
		 * @param  mixed $product_type cosa.
		 * @return String
		 */
		public function woocommerce_product_class( $classname, $product_type ) {
			if ( 'event_ticket' === $product_type ) {
				$classname = 'WC_Product_Event_Ticket';
			}
			return $classname;
		}

		/**
		 * Event_ticket_product_admin_custom_js
		 *
		 * @return void
		 */
		public function event_ticket_product_admin_custom_js() {

			if ( 'product' !== get_post_type() ) :
				return;
			endif;
			?>
			<script type='text/javascript'>
				jQuery(document).ready(function () {
					//for Price tab
					jQuery('.general_options').show();
					jQuery('.options_group.pricing').addClass('show_if_event_ticket').show();
					//for Inventory tab
					jQuery('.inventory_options').addClass('show_if_event_ticket').show();
					jQuery('#inventory_product_data ._manage_stock_field').addClass('show_if_event_ticket').show();
					jQuery('#inventory_product_data ._sold_individually_field').parent().addClass('show_if_event_ticket').show();
					jQuery('#inventory_product_data ._sold_individually_field').addClass('show_if_event_ticket').show();
				});
			</script>
			<?php
		}


	}
}
