<?php
/**
 *
 * Admin Main Class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCET_Admin' ) ) {
	/**
	 * YITH_PPWCET_Admin
	 */
	class YITH_PPWCET_Admin {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_Admin
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'wc_purchase_notes_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_wc_purchase_notes_fields' ) );
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_product_addons_meta' ) );
			add_filter( 'manage_yith_event_ticket_posts_columns', array( $this, 'add_event_ticket_post_type_columns' ) );
			add_action( 'manage_yith_event_ticket_posts_custom_column', array( $this, 'display_event_ticket_post_type_custom_column' ), 10, 2 );
			add_action( 'woocommerce_order_status_processing', array( $this, 'event_ticket_changing_processing_status' ) );
			add_action( 'woocommerce_order_status_completed', array( $this, 'event_ticket_changing_processing_status' ) );
			add_action( 'woocommerce_order_status_cancelled', array( $this, 'event_ticket_changing_cancelled_status' ) );
			add_action( 'woocommerce_order_status_refunded', array( $this, 'event_ticket_changing_cancelled_status' ) );
			add_action( 'woocommerce_order_status_failed', array( $this, 'event_ticket_changing_cancelled_status' ) );
			add_action( 'admin_init', array( $this, 'setup_role_shop' ) );
			add_action( 'init', array( $this, 'register_post_status' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes_event_ticket' ) );
			add_action( 'wp_ajax_nopriv_ticket_used', array( $this, 'variation_change_ajax' ) );
			add_action( 'wp_ajax_ticket_used', array( $this, 'variation_change_ajax' ) );
		}

		/**
		 * Wc_purchase_notes_tab
		 *
		 * @param  mixed $tabs f.
		 * @return Array
		 */
		public function wc_purchase_notes_tab( $tabs ) {

			// Adds the new tab.
			$tabs['event_fields'] = array(
				'label'    => __( 'Fields', 'yith-ppwc-eventticket' ),
				'target'   => 'yith_ppwcet_container',
				'class'    => array( 'show_if_event_ticket' ),
				'priority' => 86,
			);
			return $tabs;
		}

		/**
		 * Display_wc_purchase_notes_fields
		 *
		 * @return void
		 */
		public function display_wc_purchase_notes_fields() {
			global $post;
			$args = get_post_meta( $post->ID, '_yith_ppwcet_required', true );
			echo '<div  id="yith_ppwcet_container" class="panel">';
			yith_ppwcet_get_view( '/admin/yith-ppwcet-fields-view.php', $args );
			echo '</div>';
			wp_enqueue_style( 'yith_ppwcet_product_data_styles' );
		}

		/**
		 * Save_product_addons_meta
		 *
		 * @param  mixed $product f.
		 * @return void
		 */
		public function save_product_addons_meta( $product ) {

			if ( isset( $_POST['yith_ppwcet_required'] ) ) {//phpcs:ignore
				$required_list = $_POST['yith_ppwcet_required'];//phpcs:ignore
				$product->update_meta_data( '_yith_ppwcet_required', $required_list );
			}

		}

		/**
		 * Add_testimonials_post_type_columns
		 *
		 * @param  mixed $post_columns Desired post column.
		 * @return Array
		 */
		public function add_event_ticket_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_ppwcet_custom_columns',
				array(
					'status_column'          => '<span id="yith_ppwcet_status_colummn" class="fa fa-check-square">',
					'ticket_column'          => __( 'Ticket', 'yith-ppwc-eventticket' ),
					'order_column'           => __( 'Order', 'yith-ppwc-eventticket' ),
					'purchase_status_column' => __( 'Purchase Status', 'yith-ppwc-eventticket' ),
					'actions_column'         => __( 'Actions', 'yith-ppwc-eventticket' ),
				)
			);
			unset( $post_columns['title'] );
			unset( $post_columns['date'] );
			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;

		}

		/**
		 * Event_ticket_changing_processing_status
		 *
		 * @param  mixed $order_id Id of the order.
		 * @return void
		 */
		public function event_ticket_changing_processing_status( $order_id ) {
			$order = wc_get_order( $order_id );
			$items = $order->get_items();
			foreach ( $items as $item ) {

				$product_id = $item->get_product_id();
				$product    = wc_get_product( $product_id );
				if ( 'event_ticket' === $product->get_type() ) {
					$product_name  = $item->get_name();
					$meta_name     = $item->get_meta( 'yith_ppwcet_name' );
					$meta_surname  = $item->get_meta( 'yith_ppwcet_surname' );
					$item_id       = $item->get_id();
					$item_quantity = $item->get_quantity();
					for ( $i = 0; $i < $item_quantity; $i++ ) {
						$post_meta    = array(
							'name'                     => $meta_name,
							'surname'                  => $meta_surname,
							'post_title'               => $product_name,
							'order_id'                 => $order_id,
							'customer_email'           => $order->get_billing_email(),
							'customer_name'            => email_exists( $order->get_billing_email() ) ? get_userdata( email_exists( $order->get_billing_email() ) )->user_login : $order->first_name . ' ' . $order->last_name,
							'order_status'             => ucfirst( $order->get_status() ),
							'order_status_date_simple' => get_the_date( 'F d, Y' ),
							'order_status_datetime'    => current_time( 'mysql' ),
							'yith_ppwcet_product_id'   => $product_id,
						);
						$post_created = get_posts(
							array(
								'post_status' => 'any',
								'post_type'   => 'yith_event_ticket',
							    'meta_query'  => array(//phpcs:ignore
									'relation' => 'AND',
									array(
										'key'   => '_yith_ppwcet_item_id',
										'value' => ( $item_quantity > 1 ) ? $item_id . '-' . $i : $item_id,
									),
									array(
										'key'   => '_yith_ppwcet_order_id',
										'value' => $order_id,
									),

								),
							)
						);
						$post_arr = array(
							'ID'          => isset( $post_created[0]->ID ) ? intval( $post_created[0]->ID ) : 0,
							'post_author' => '1',
							'post_title'  => $product_name,
							'post_status' => isset( $post_created[0] ) ? get_post_status( $post_created[0] ) : 'yith-pending-check',
							'post_type'   => 'yith_event_ticket',
							'meta_input'  => array(
								'_yith_ppwcet_meta'       => $post_meta,
								'_yith_ppwcet_order_id'   => $order_id,
								'_yith_ppwcet_item_id'    => ( $item_quantity > 1 ) ? $item_id . '-' . $i : $item_id,
								'_yith_ppwcet_product_id' => $product_id,
							),

						);

						$post_id = wp_insert_post( $post_arr );

						$item->add_meta_data( ( $item_quantity > 1 ) ? '_event_id' . $i : '_event_id', $post_id, true );
						$item->save_meta_data();
					}
				}
			}
		}

		/**
		 * Event_ticket_changing_cancelled_status
		 *
		 * @param  mixed $order_id Id of order.
		 * @return void
		 */
		public function event_ticket_changing_cancelled_status( $order_id ) {
			$order = wc_get_order( $order_id );
			$items = $order->get_items();
			foreach ( $items as $item ) {

				$product_id = $item->get_product_id();
				$product    = wc_get_product( $product_id );
				if ( 'event_ticket' === $product->get_type() ) {
					$item_id       = $item->get_id();
					$item_quantity = $item->get_quantity();
					for ( $i = 0; $i < $item_quantity; $i++ ) {
						$post_created = get_posts(
							array(
								'post_status' => 'any',
								'post_type'   => 'yith_event_ticket',
						        'meta_query'  => array(//phpcs:ignore
									'relation' => 'AND',
									array(
										'key'   => '_yith_ppwcet_item_id',
										'value' => ( $item_quantity > 1 ) ? $item_id . '-' . $i : $item_id,
									),
									array(
										'key'   => '_yith_ppwcet_order_id',
										'value' => $order_id,
									),
								),
							)
						);

						if ( 0 !== count( $post_created ) ) {
							wp_delete_post( $post_created[0]->ID );
							$item->delete_meta_data( ( $item_quantity > 1 ) ? '_event_id' . $i : '_event_id' );
							$item->save_meta_data();
						}
					}
				}
			}
		}

		/**
		 * Display_event_ticket_post_type_custom_column
		 *
		 * @param  mixed $column_name f.
		 * @param  mixed $post_id f.
		 * @return void
		 */
		public function display_event_ticket_post_type_custom_column( $column_name, $post_id ) {
			$post_meta = ( get_post_meta( $post_id, '_yith_ppwcet_meta', true ) !== '' ) ? get_post_meta( $post_id, '_yith_ppwcet_meta', true ) : null;
			if ( 'yith-checked-check' === get_post_status( $post_id ) ) {
				$class = 'yith_ppwcet_checked';
			} elseif ( 'yith-pending-check' === get_post_status( $post_id ) ) {
				$class = 'yith_ppwcet_pending';
			}

			switch ( $column_name ) {
				case 'status_column':
					echo '<span id="yith_ppwcet_post_status' . esc_html( $post_id ) . '" class=" ' . esc_html( $class ) . ' fa fa-check-circle"></span>';
					break;
				case 'ticket_column':
					echo esc_html( '#' . $post_id . ', ' . ( isset( $post_meta ) ? $post_meta['post_title'] : '' ) );
					break;
				case 'order_column':
					echo esc_html( '#' . ( isset( $post_meta ) ? $post_meta['order_id'] : '' ) . ' by ' . ( isset( $post_meta ) ? $post_meta['customer_name'] : '' ) ) . '<span>' . ( isset( $post_meta ) ? esc_html( $post_meta['customer_email'] ) : '' ) . '</span>';
					break;
				case 'purchase_status_column':
					echo esc_html( ( isset( $post_meta ) ? $post_meta['order_status'] : '' ) . ' on ' . ( isset( $post_meta ) ? $post_meta['order_status_date_simple'] : '' ) );
					break;
				case 'actions_column':
					echo '<a class=" yith_ppwcet_check_ticket button fa fa-check" title=' . esc_attr__( 'Ticket Used', 'yith-ppwc-eventticket' ) . ' id="yith_ppwcet_check_ticket' . esc_html( $post_id ) . '" aria-hidden="true" ></a><a class="button fa fa-eye tips" title=' . esc_attr__( 'View ticket', 'yith-ppwc-eventticket' ) . ' aria-hidden="true" href=' . esc_attr( get_edit_post_link( $post_id ) ) . ' ></a>';
					break;
				default:
					do_action( 'yith_ppwcet_display_custom_column', $column_name, $post_id );
					break;
			}
			wp_localize_script( 'yith_ppwcet_admin_script', 'yith_ppwcet_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

		}

		/**
		 * Setup_role_shop
		 *
		 * @return void
		 */
		public function setup_role_shop() {
			if ( ! get_option( 'yith_ppwcet_add_role_event_tickets', false ) ) {

				$capabilities = array(
					'edit_event_tickets',
					'edit_event_ticket',
					'read_event_ticket',
					'delete_event_tickets',
					'delete_event_ticket',
					'publish_event_tickets',
					'read_private_event_tickets',
				);

				$admin_role = get_role( 'administrator' );
				foreach ( $capabilities as $cap ) {

					$admin_role->add_cap( $cap, true );

				}
				update_option( 'yith_ppwcet_add_role_event_tickets', true );
			};

		}

		/**
		 * Add_meta_boxes_event_ticket
		 *
		 * @return void
		 */
		public function add_meta_boxes_event_ticket() {
			global $post;
			$post_meta = get_post_meta( $post->ID, '_yith_ppwcet_meta', true );
			if ( $post_meta ) {
				add_meta_box( 'yith-ppwcet-additional-information', 'Event #' . $post->ID . ' ' . $post_meta['post_title'] . ' details', array( $this, 'view_meta_boxes' ), YITH_PPWCET_Ticket_Post_Type::$post_type );
			}
		}

		/**
		 * View_meta_boxes
		 *
		 * @param  mixed $post WP_POST.
		 * @return void
		 */
		public function view_meta_boxes( $post ) {
			yith_ppwcet_get_view( '/metaboxes/yith-ppwcet-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Variation_change_ajax
		 *
		 * @return void
		 */
		public function variation_change_ajax() {
            if ( $_POST['ticket_id'] ) {//phpcs:ignore
                $ticket_post = get_post( $_POST['ticket_id'] );//phpcs:ignore
				if ( 'yith-pending-check' === get_post_status( $ticket_post ) ) {
					wp_update_post(
						array(
							'ID'          => $ticket_post->ID,
							'post_status' => 'yith-checked-check',
						)
					);
					wp_send_json( array( 'post_status' => 'yith-checked-check' ) );
				} elseif ( 'yith-checked-check' === get_post_status( $ticket_post ) ) {
					wp_update_post(
						array(
							'ID'          => $ticket_post->ID,
							'post_status' => 'yith-pending-check',
						)
					);
					wp_send_json( array( 'post_status' => 'yith-pending-check' ) );
				}
			}
			wp_die();
		}

		/**
		 * Custom_post_status
		 *
		 * @return void
		 */
		public function register_post_status() {

			$ticket_statuses = apply_filters(
				'woocommerce_register_ticket_statuses',
				array(
					'yith-pending-check' => array(
						'label'                  => __( 'Pending check', 'yith-ppwc-eventticket' ),
						'public'                 => true,
						'exclude_from search'    => false,
						'show_in_admin_all_list' => true,
						'show_in_admin_status'   => true,
						'label_count'            => _n_noop( 'Pending check <span class="count">(%s)</span>', 'Pending check <span class="count">(%s)</span>', 'yith-ppwc-eventticket' ),//phpcs:ignore
					),
					'yith-checked-check' => array(
						'label'                  => __( 'Check', 'yith-ppwc-eventticket' ),
						'public'                 => true,
						'exclude_from search'    => false,
						'show_in_admin_all_list' => true,
						'show_in_admin_status'   => true,
						'label_count'            => _n_noop( 'Checked <span class="count">(%s)</span>', 'Checked <span class="count">(%s)</span>', 'yith-ppwc-eventticket' ),//phpcs:ignore
					),
				)
			);

			foreach ( $ticket_statuses as $ticket_status => $values ) {
				register_post_status( $ticket_status, $values );
			}
		}

		/**
		 * Enqueue_admin_styles
		 *
		 * @return void
		 */
		public function enqueue_admin_styles() {
			global $pagenow;
			$post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';// phpcs:ignore
			if ( 'edit.php' === $pagenow && 'yith_event_ticket' === $post_type ) {
				wp_enqueue_script( 'yith_ppwcet_admin_script', YITH_PPWCET_DIR_ASSETS_JS_URL . '/yith-ppwcet-admin-script.js', array(), '1.0.0', true );
				wp_enqueue_style( 'yith_ppwcet_admin_styles', YITH_PPWCET_DIR_ASSETS_CSS_URL . '/yith-ppwcet-admin-styles.css', array(), '1.0.0' );
				wp_register_style( 'yith-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', null, '1.0.0' );
				wp_enqueue_style( 'yith-fontawesome' );
			}
			wp_register_style( 'yith_ppwcet_product_data_styles', YITH_PPWCET_DIR_ASSETS_CSS_URL . '/yith-ppwcet-admin-product-data.css', array(), '1.0.0' );
		}


	}
}
