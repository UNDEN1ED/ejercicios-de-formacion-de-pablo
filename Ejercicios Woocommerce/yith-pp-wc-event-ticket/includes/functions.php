<?php
/**
 * Commons functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_ppwcet_get_view' ) ) {

	/**
	 * Yith_ppwcet_get_view
	 *
	 * @param  mixed $file_name name of the file.
	 * @param  mixed $args all arguments want for the view.
	 * @return void
	 */
	function yith_ppwcet_get_view( $file_name, $args = array() ) {
		$full_path = YITH_PPWCET_DIR_VIEWS_PATH . $file_name;

		include $full_path;
	}
}

if ( ! function_exists( 'yith_ppwcet_get_template' ) ) {
	/**
	 * Yith_ppwcet_get_template
	 *
	 * @param  mixed $file_name F.
	 * @param  mixed $args F.
	 * @return void
	 */
	function yith_ppwcet_get_template( $file_name, $args = array() ) {

		$full_path = YITH_PPWCET_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
