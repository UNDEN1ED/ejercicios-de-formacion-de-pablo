<?php
/**
 * Ajax Main Class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}



if ( ! class_exists( 'YITH_PPWCET_AJAX' ) ) {
	/**
	 * YITH_PPWCET_AJAX
	 */
	class YITH_PPWCET_AJAX {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_AJAX
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_AJAX
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_ajax_nopriv_ticket_used_front', array( $this, 'check_change_ajax' ) );
			add_action( 'wp_ajax_ticket_used_front', array( $this, 'check_change_ajax' ) );
			add_action( 'wp_ajax_nopriv_print_ticket_pdf', array( $this, 'print_pdf_ajax' ) );
			add_action( 'wp_ajax_print_ticket_pdf', array( $this, 'print_pdf_ajax' ) );
		}

		/**
		 * Check_change_ajax
		 *
		 * @return void
		 */
		public function check_change_ajax() {
			$event_post = isset( $_POST['event_id'] ) ? get_post($_POST['event_id']) : 'false';//phpcs:ignore
			if ( $event_post ) {
				if ( 'yith-checked-check' === get_post_status( $event_post ) ) {
					wp_update_post(
						array(
							'ID'          => $event_post->ID,
							'post_status' => 'yith-pending-check',
						)
					);
					wp_send_json( array( 'post_status' => 'yith-pending-check' ) );
				} elseif ( 'yith-pending-check' === get_post_status( $event_post ) ) {
					wp_update_post(
						array(
							'ID'          => $event_post->ID,
							'post_status' => 'yith-checked-check',
						)
					);
					wp_send_json( array( 'post_status' => 'yith-checked-check' ) );
				}
				wp_die();
			}
		}

		/**
		 * Print_pdf__ajax
		 *
		 * @return void
		 */
		public function print_pdf_ajax() {
			require_once YITH_PPWCET_DIR_VENDOR_PATH . '/autoload.php';
			$mpdf = new \Mpdf\Mpdf();
			$pdf->AddPage();
			$mpdf->WriteHTML( '<h1>Hello world!</h1>' );
			$mpdf->Output( 'document.pdf' );
			wp_die();
		}
	}
}
