<?php
/**
 *
 * Singleton Class Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPWCET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPWCET_Ticket_Post_Type' ) ) {
	/**
	 * YITH_PPWCET_Ticket_Post_Type
	 */
	class YITH_PPWCET_Ticket_Post_Type {
		/**
		 * Main instance
		 *
		 * @var YITH_PPWCET_Ticket_Post_Type
		 */
		private static $instance;


		/**
		 * Post Type
		 *
		 * @var YITH_PPWCET_Ticket_Post_Type
		 */
		public static $post_type = 'yith_event_ticket';

		/**
		 * Get_instance
		 *
		 * @return YITH_PPWCET_Ticket_Post_Type
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_types' ) );
		}

		/**
		 * Setup_post_types
		 *
		 * @return void
		 */
		public function setup_post_types() {
			// This array contains all the labels that we want to appear in our dashboard when managing our custom posts.
			$labels = array(

				'name'               => __( 'Event Tickets' ),
				'singular_name'      => __( 'Event Ticket' ),
				'add_new'            => __( 'Add New Event Ticket' ),
				'add_new_item'       => __( 'Add New Event Ticket' ),
				'edit_item'          => __( 'Edit Event Ticket' ),
				'view_item'          => __( 'View Event Ticket' ),
				'search_items'       => __( 'Search Event Tickets' ),
				'not_found'          => __( 'No Event Tickets found' ),
				'not_found_in_trash' => __( 'No Event Tickets found in Trash' ),
				'all_items'          => __( 'All Event Tickets' ),
			);
			// Here it will give all the option that our custom post will use like the labels, and the type of custom post.
			$args = array(
				'labels'              => $labels,
				'description'         => __( 'Post type to store Event Ticket' ),
				'public'              => false,
				'publicly_ queryable' => true,
				'show_ui'             => true,
				'query_var'           => true,
				'menu_icon'           => 'dashicons-clipboard',
				'capability_type'     => array( 'event_tickets', 'event_ticket' ),
				'capabilities'        => array( 'create_posts' => 'do_not_allow' ),
				'has_archive'         => true,
				'hierarchical'        => false,
				'exclude_from_search' => true,
				'menu_position'       => 24,
				'show_in_rest'        => false,
				'supports'            => array( 'none' ),

			);
			// Register the custom post with all the arguments that we want it to use.
			register_post_type( 'yith_event_ticket', $args );
		}


	}
}
