<?php
/**
 *
 * Event List Manager template
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
if ( isset( $a ) && isset( $posts_list ) ) {
	$product = wc_get_product( $a['product_id'] );
}


?>

<div class='yith_ppwcet_shortcode_container'>
	<form action="post" class="yith_ppwcet_form_events" >
		<input type="text" id="yith_ppwcet_search_input" placeholder="Enter the ticket number here" name="yith_ppwcet_search_input">
		<button type="button" class='button yith_ppwcet_search_button'>Search</button>
		<p class="yith_ppwcet_post_list_title">Tickets purchased for <b>"<?php echo isset( $product ) ? esc_html( $product->get_title() ) : ''; ?>"</b> event</p>
		<div class="yith_ppwcet_post_list">
			<div class="yith_ppwcet_post_list_header">
				<span class=" fa fa-check-square"></span>
				<span class="yith_ppwcet_ticket_id_title"><b>Ticket Info</b></span>
			</div>
			<?php
			foreach ( $posts_list as $post_item ) :
				?>
			<div class="yith_ppwcet_post_list_rows">
				<div class="yith_ppwcet_row_header">
					<div class="yith_ppwcet_column yith_ppwcet_ticket_status"><span id="yith_ppwcet_ticket_status-<?php echo esc_html( $post_item->ID ); ?>" class="<?php echo ( 'yith-checked-check' === get_post_status( $post_item ) ) ? 'yith_pppwcet_status_symbol_checked' : 'yith_pppwcet_status_symbol'; ?> fa fa-check-circle"></span></div>
					<div class="yith_ppwcet_column yith_ppwcet_ticket_id">
						<span>
							<b>#<?php echo isset( $post_item ) ? esc_html( $post_item->ID ) : ''; ?></b>
							<small>, purchase by </small><b><?php echo isset( $post_item ) ? esc_html( get_post_meta( $post_item->ID, '_yith_ppwcet_meta', true )['customer_name'] ) : ''; ?></b>
						</span>
					</div>
					<div class="yith_ppwcet_column yith_ppwcet_row_action">
						<span class="yith_ppwcet_info_button fa fa-caret-up" id="yth_ppwcet_arrow-<?php echo esc_attr( $post_item->ID ); ?>"></span>
						<button type="button" id="yith_ppwcet_check_button-<?php echo esc_html( $post_item->ID ); ?>" class='yith_ppwcet_check_button fa fa-check' ></button>
						<button type="button" id="yith_ppwcet_pdf_button-<?php echo esc_html( $post_item->ID ); ?>" class='yith_ppwcet_pdf_button fa fa-file-text' ></button>
					</div>
				</div>
				<div class="yith_ppwect_row_info" id="yith_ppwect_row_info-<?php echo esc_attr( $post_item->ID ); ?>" style="display: none;">
					<div class ="yith_ppwect_row_info_header">
							<span><b>Fields</b></span>
					</div>
					<div class ="yith_ppwect_row_info_content">
						<div class ="yith_ppwect_row_info_name" >
							<b>Name: </b><?php echo isset( $post_item ) ? esc_html( get_post_meta( $post_item->ID, '_yith_ppwcet_meta', true )['name'] ) : ''; ?>
						</div>
						<div class ="yith_ppwect_row_info_surname">
							<b>Surname: </b><?php echo isset( $post_item ) ? esc_html( get_post_meta( $post_item->ID, '_yith_ppwcet_meta', true )['surname'] ) : ''; ?>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</form>
</div>
