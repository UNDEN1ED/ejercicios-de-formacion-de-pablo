<?php
/**
 * Simple custom product
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

do_action( 'event_ticket_before_add_to_cart_form' );  ?>

<form class="event_ticket_cart" method="post" enctype='multipart/form-data'>
	<table cellspacing="0">
		<tbody>
			<tr>
				<td >
					<label for="event_ticket_amount"><?php esc_html_e( 'Amount' ); ?></label>
				</td>
				<td class="price">
					<?php
					$get_price = get_post_meta( $product->get_id(), '_event_ticket_price' );
					$price     = 0;
					if ( isset( $get_price[0] ) ) {
						$price = wc_price( $get_price[0] );
					}
					echo esc_html( $price );
					?>
				</td>
			</tr>
		</tbody>
	</table>
	<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
</form>

<?php do_action( 'event_ticket_after_add_to_cart_form' ); ?>
