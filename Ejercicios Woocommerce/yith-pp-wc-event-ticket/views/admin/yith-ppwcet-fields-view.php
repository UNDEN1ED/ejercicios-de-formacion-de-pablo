<?php
/**
 * Main Admin Fields
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}

?>
<table id="yith_ppwcet_fields_table">
	<tr>
		<th><?php esc_html_e( 'Name', 'yith-ppwc-eventticket' ); ?></th>
		<th><?php esc_html_e( 'Required', 'yith-ppwc-eventticket' ); ?></th>
	</tr>
	<tr>
		<td><label><?php esc_html_e( 'Name', 'yith-ppwc-eventticket' ); ?></label></td>
		<td><input type="checkbox" name="yith_ppwcet_required[name]" value="on" <?php echo isset( $name ) ? checked( $name, 'on' ) : ''; ?>></td>
	</tr>
	<tr>
		<td><label><?php esc_html_e( 'Surname', 'yith-ppwc-eventticket' ); ?></label></td>
		<td><input type="checkbox" name="yith_ppwcet_required[surname]" value="on" <?php echo isset( $surname ) ? checked( $surname, 'on' ) : ''; ?>></td>
	</tr>
</table>
