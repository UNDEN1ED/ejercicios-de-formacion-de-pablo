<?php
/**
 *
 *  Event Ticket Content Metabox
 *
 * @package WordPress
 */

if ( ! class_exists( 'mPDF' ) ) {
	require_once YITH_PPWCET_DIR_VENDOR_PATH . '/autoload.php';
}

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
if ( isset( $post ) ) {
	$post_meta = get_post_meta( $post->ID, '_yith_ppwcet_meta', true );
}
?>

<div id="yith_ppwcet_ticket_info_container">
	<p><?php echo isset( $post_meta ) ? esc_html__( 'Purchase date: ', 'yith-ppwc-eventticket' ) . esc_html( $post_meta['order_status_datetime'] ) : ''; ?></p>
	<h4><?php echo esc_html__( 'Field details', 'yith-ppwc-eventticket' ); ?></h4>
	<p><?php echo esc_html__( 'Name : ', 'yith-ppwc-eventticket' ) . ( isset( $post_meta ) ? esc_html( $post_meta['name'] ) : '' ); ?></p>
	<p><?php echo esc_html__( 'Surname : ', 'yith-ppwc-eventticket' ) . ( isset( $post_meta ) ? esc_html( $post_meta['name'] ) : '' ); ?></p>
	</div>
