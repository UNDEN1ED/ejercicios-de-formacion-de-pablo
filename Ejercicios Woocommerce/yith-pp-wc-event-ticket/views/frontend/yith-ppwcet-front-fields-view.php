<?php
/**
 * Main Front Fields
 *
 * @package WordPress
 */

foreach ( $args as $arg => $val ) {
	${$arg} = $val;
}
global $product;

?>
<div id="yith_ppwcet_container-INDEX-" class="yith_ppwcet_tickets" style="display:none">
	<h4><?php echo esc_html( $product->get_title() . ' #-INDEX-' ); ?></h4>
	<p><?php echo esc_html__( 'Name', 'yith-ppwc-eventticket' ) . ( isset( $name ) ? esc_html( '*' ) : '' ); ?></p>
	<input type="text"  class="yith_ppwcet_event_field">
	<p><?php echo esc_html__( 'Surname', 'yith-ppwc-eventticket' ) . ( isset( $surname ) ? esc_html( '*' ) : '' ); ?></p>
	<input type="text"  class="yith_ppwcet_event_field">
</div>
<div id="yith_ppwcet_container1" class="yith_ppwcet_tickets" >
	<h4><?php echo esc_html( $product->get_title() . ' #1' ); ?></h4>
	<p><?php echo esc_html__( 'Name', 'yith-ppwc-eventticket' ) . ( isset( $name ) ? esc_html( '*' ) : '' ); ?></p>
	<input type="text" name="yith_ppwcet_event_fields[1][name]" class="yith_ppwcet_event_field">
	<p><?php echo esc_html__( 'Surname', 'yith-ppwc-eventticket' ) . ( isset( $surname ) ? esc_html( '*' ) : '' ); ?></p>
	<input type="text" name="yith_ppwcet_event_fields[1][surname]" class="yith_ppwcet_event_field">
</div>
