��    	      d      �       �   8   �   J        e     k     t  '   y     �  )   �  �  �  .   [  M   �     �  	   �     �  ;   �     9  %   G                                          	    Congratulations, you have joined the raffle.¡Good luck! Create your own raffles. Make your viewers and users able to join raffles. Email Lastname Name This user has already joined the raffle YITH Raffle Yes, I want to participate in the raffle. Project-Id-Version: YITH Raffle 1.7.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-pp-raffle
PO-Revision-Date: 2020-11-04 13:29+0000
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Domain: yith_pp_raffles
Plural-Forms: nplurals=2; plural=(n != 1);
 Felicidades,has entrado en el sorteo.¡Suerte! Crear tus propias encuestas. Haz que tus viewers puedan unir a tus encuestas. Correo Electrónico Apellidos Nombre Este usuario ya ha sido inscrito en el sorteo anteriormente YITH Encuesta Si, quiero participar en la encuesta. 