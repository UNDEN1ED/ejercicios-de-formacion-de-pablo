<?php
/**
 *
 * Raffle Template
 *
 * @package WordPress
 */

$raffle_user_email = $args['user_email'];
$raffle_userlist   = $args['user_list'];
?>


<div id="yith_ppraf_container">
	<h3><?php esc_html_e( 'YITH Raffle', 'yith_pp_raffles' ); ?></h3>
	<form method="post">
	<div id="yith_ppraf_form_container">	
<?php
	wp_nonce_field( 'yith_ppraf_submit', 'yith_pp_raffle_nonce' );
if ( empty( $raffle_user_email ) ) {
	?>
		<label for="yith_ppraf_name" class="yith_ppraf_items"><?php esc_html_e( 'Name', 'yith_pp_raffles' ); ?></label>
		<input type="text" name="yith_ppraf_name" id="yith_ppraf_name" class="yith_ppraf_items">
		<label for="yith_ppraf_lastname" class="yith_ppraf_items"><?php esc_html_e( 'Lastname', 'yith_pp_raffles' ); ?></label>
		<input type="text" name="yith_ppraf_lastname" id="yith_ppraf_lastname" class="yith_ppraf_items">
		<label for="yith_ppraf_email" class="yith_ppraf_items"><?php esc_html_e( 'Email', 'yith_pp_raffles' ); ?></label>
		<input type="text" name="yith_ppraf_email" id="yith_ppraf_email" class="yith_ppraf_items">
<?php } ?>
		<input type="checkbox"  name="yith_ppraf_check" id="yith_ppraf_check"  onclick="yith_ppraf_onClick_check()" class="yith_ppraf_items">
		<label for="yith_ppraf_check" class="yith_ppraf_items" id="yith_ppraf_check_label"><?php esc_html_e( 'Yes, I want to participate in the raffle.', 'yith_pp_raffles' ); ?></label>
	</div>
	<br>
<?php if ( get_option( 'yith_raffle' ) === '1' ) { ?>
	<div id="yith_ppraf_msg_container_conf">
		<p id="yith_ppraf_msg"><?php esc_html_e( 'Congratulations, you have joined the raffle.¡Good luck!', 'yith_pp_raffles' ); ?></p>
	</div>
<?php } elseif ( get_option( 'yith_raffle' ) === '0' ) { ?>
	<div id="yith_ppraf_msg_container_err">
		<p id="yith_ppraf_msg"><?php esc_html_e( 'This user has already joined the raffle', 'yith_pp_raffles' ); ?></p>
	</div>
<?php } ?>
	<input type="submit" id="yith_ppraf_submit" onclick="yith_ppraf_onClick_submit()" name="yith_ppraf_submit" disabled=disabled>
	</form>
	<?php
	if ( ! empty( $raffle_userlist ) ) {
		?>
	<div id='yith_ppraf_user_list_container'>
		<h6>Últimos usuarios registrados</h6>
		<ul>
		<?php
		foreach ( $raffle_userlist as $raffle_user ) {
			?>
			<li><?php echo esc_html( $raffle_user ); ?></li>
			<?php
		}
		?>
		</ul>
	</div>
	<?php }; ?>
</div>
