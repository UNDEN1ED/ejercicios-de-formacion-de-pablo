function yith_ppraf_onClick_check() {
    if (document.getElementById('yith_ppraf_check').checked) {
        document.getElementById('yith_ppraf_submit').disabled = false;
    } else {
        document.getElementById('yith_ppraf_submit').disabled = true;
    }
}