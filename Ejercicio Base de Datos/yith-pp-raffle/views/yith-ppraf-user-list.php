<?php
/**
 *
 * View
 *
 * @package WordPress
 */

?>
<div class="yith-ppraf-wrap">    
	<h2><?php esc_html_e( 'YITH RAFFLE User list' ); ?></h2>
	<div id="yith-ppraf-user-list">			
		<div id="yith-ppraf-content">		
			<form id="yith-ppraf-user-list-form" method="get">					
				<?php $this->user_list_table->display(); ?>					
			</form>
		</div>
	</div>
</div>
