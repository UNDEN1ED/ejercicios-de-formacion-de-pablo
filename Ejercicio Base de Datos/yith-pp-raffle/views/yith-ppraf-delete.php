<?php
/**
 *
 * Delete row from wp_yith_raffle
 *
 * @package WordPress
 */

global $wpdb;
$wpdb->delete( 'wp_yith_raffle', array( 'user_id' => $post_id ) ); // db call ok; no-cache ok.
?>

<a href="<?php echo '?page=yith-raffle'; ?>">User deleted</a>
