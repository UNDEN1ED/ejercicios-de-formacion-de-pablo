<?php
/**
 * Shortcode
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_RAF_Shortcode' ) ) {
	/**
	 * YITH_PPRAF_Shortcode
	 */
	class YITH_PPRAF_Shortcode {
		/**
		 * Main instance
		 *
		 * @var YITH_PPRAF_Shortcode
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPRAF_Shortcode
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_ppraf_add_custom_shortcode' ) );
		}

		/**
		 * Setup_shortcode
		 *
		 * @return mixed/false
		 */
		public function setup_shortcode() {

			$user               = wp_get_current_user();
			$user_list          = YITH_PPRAF_User_List::list_set_up();
			$args['user_email'] = $user->user_email;
			$args['user_list']  = $user_list;
			$i                  = null;

			ob_start();

			if ( array_key_exists( 'yith_ppraf_submit', $_POST ) && ( isset( $_POST['yith_pp_raffle_nonce'] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['yith_pp_raffle_nonce'] ) ), 'yith_ppraf_submit' ) !== false ) ) {
				if ( is_user_logged_in() ) {

					$name_raffle     = $user->first_name;
					$lastname_raffle = $user->last_name;
					$email_raffle    = $user->user_email;
				} elseif ( isset( $_POST['yith_ppraf_name'] ) && isset( $_POST['yith_ppraf_lastname'] ) && isset( $_POST['yith_ppraf_email'] ) ) {

					$name_raffle     = sanitize_text_field( wp_unslash( $_POST['yith_ppraf_name'] ) );
					$lastname_raffle = sanitize_text_field( wp_unslash( $_POST['yith_ppraf_lastname'] ) );
					$email_raffle    = sanitize_text_field( wp_unslash( $_POST['yith_ppraf_email'] ) );
				}
				$records     = get_raffle_by_name( $email_raffle );
				$records_len = count( $records );

				if ( 0 === $records_len ) {

					yith_insert_raffle( $name_raffle, $lastname_raffle, $email_raffle );
					update_option( 'yith_raffle', '1' );
					if ( wp_next_scheduled( 'yith_ppraf_event_12h', array( '12H-Report Raffle User List' ) ) !== false ) {
						wp_clear_scheduled_hook( 'yith_ppraf_event_12h', array( '12H-Report Raffle User List' ) );
						wp_reschedule_event( time(), 'twelve_hours', 'yith_ppraf_event_12h', array( 'Daily-Report Raffle User List' ) );
						yith_ppraf_send_email( 'A new user has joined the raffle' );
					};

				} else {

					update_option( 'yith_raffle', '0' );
				}
			} else {
				update_option( 'yith_raffle', '-1' );
			}
			yith_ppraf_get_template( '/frontend/yith-raffle.php', $args );
			return ob_get_clean();

		}

		/**
		 * Yith_ppraf_add_custom_shortcode
		 *
		 * @return void
		 */
		public function yith_ppraf_add_custom_shortcode() {
			add_shortcode( 'yith_ppraf_shortcode', array( $this, 'setup_shortcode' ) );
		}
	}
}
