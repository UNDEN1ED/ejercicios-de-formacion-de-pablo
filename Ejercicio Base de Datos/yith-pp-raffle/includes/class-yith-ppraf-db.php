<?php
/**
 * DATABASE
 *
 * @package WordPress
 */

// phpcs:disable
if(!defined('ABSPATH')){
    exit;
}

if(!class_exists('YITH_RAF_DB')){
    class YITH_PPRAF_DB{
        public static $version = '1.0.0';
        public static $raffle_table ="yith_raffle";


        private function __construct(){
        }

        public static function install(){
            self::create_db_table();
        }

        private static function create_db_table($force = true){
            global $wpdb;
            $current_version = get_option('yith_ppraf_db_version');

            if($force || $current_version !==self::$version){
                $wpdb->hide_errors();

                $table_name = $wpdb->prefix .self::$raffle_table;
                $charset_collate = $wpdb->get_charset_collate();

                $sql = "CREATE TABLE $table_name (
                    `user_id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    `firstname` varchar(60) NOT NULL,
                    `lastname` varchar(60) NOT NULL,
                    `email` varchar(100) NOT NULL
                    ) $charset_collate;"
                ;
                if(!function_exists('dbDelta')){
                    require_once(ABSPATH. "wp-admin/includes/upgrade.php");
                }
                dbDelta($sql);
                update_option('yith_ppraf_db_version', self::$version);
                
            }
        }

        


    }
}