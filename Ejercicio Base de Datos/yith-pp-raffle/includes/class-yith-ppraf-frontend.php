<?php
/**
 *
 * Frontend
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPRAF_Frontend' ) ) {

	/**
	 * YITH_PPRAF_Frontend
	 */
	class YITH_PPRAF_Frontend {


		/**
		 * Main instance
		 *
		 * @var instance
		 */
		private static $instance;

		/**
		 * Ｇet_instance
		 *
		 * @return YITH_PPRAF_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles_scripts' ) );
		}

		/**
		 * Enqueue_styles_scripts
		 *
		 * @return void
		 */
		public function enqueue_styles_scripts() {
			wp_enqueue_script( 'yith_ppraf_frontend_scriptfile', YITH_PPRAF_DIR_ASSETS_JS_URL . '/yith-ppraf-frontend-scripts.js', array(), YITH_PPRAF_VERSION, false );
			wp_enqueue_style( 'yith_ppraf_frontend_stylesfile', YITH_PPRAF_DIR_ASSETS_CSS_URL . '/yith-ppraf-frontend-styles.css', array(), YITH_PPRAF_VERSION );
		}


	}



}
