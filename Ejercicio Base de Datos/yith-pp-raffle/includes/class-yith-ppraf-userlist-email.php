<?php
/**
 * Send email with user list
 *
 * @package WordPress
 */

// phpcs:disable
if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPRAF_UserList_Email' ) ) {
	/**
	 * YITH_PPRAF_Cron
	 */
	class YITH_PPRAF_UserList_Email {
		/**
		 * Main instance
		 *
		 * @var YITH_PPRAF_UserList_Email
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPRAF_UserList_Email
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        private function __construct(){
			
			add_filter('cron_schedules', array($this, 'yith_ppraf_add_cron_interval'));
			$this->yith_ppraf_event_hook();
			add_action( 'yith_ppraf_event_12h','yith_ppraf_send_email', 11);
        }

        public function yith_ppraf_add_cron_interval($schedules){
            $schedules['twelve_hours'] = array(
                'interval' => 12 * HOUR_IN_SECONDS ,
                'display' => esc_html__('Every Twelve'),
            );
            return $schedules;
		}

		public function yith_ppraf_event_hook(){

			if(! wp_next_scheduled('yith_ppraf_event_12h', array('12H-Report Raffle User List'))){
				wp_schedule_event(time(), 'twelve_hours', 'yith_ppraf_event_12h', array('12H-Report Raffle User List'));
			}

		}

		

    }
}
