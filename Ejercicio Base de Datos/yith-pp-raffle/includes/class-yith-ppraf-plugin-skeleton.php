<?php
/**
 * Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_PPRAF_Plugin_Skeleton' ) ) {
	/**
	 * YITH_PPRAF_Plugin_Skeleton
	 *
	 * @var static $instance instace of the class
	 * @param $frontend frontend main file
	 */
	class YITH_PPRAF_Plugin_Skeleton {

		/**
		 * Main instance
		 *
		 * @var YITH_PPRAF_Plugin_Skeleton
		 */

		private static $instance;   // Variable.

		/**
		 * Main Frontend instance
		 *
		 * @var YITH_PPRAF_Frontend
		 */

		public $frontend = null;




		/**
		 * Get_instance
		 *
		 * @return  YITH_PPRAF_Plugin_Skeleton
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			$require = apply_filters(
				'yith_ppraf_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-ppraf-shortcode.php',
						'includes/class-yith-ppraf-user-list.php',
						'includes/class-yith-ppraf-userlist-email.php',
					),
					'frontend' => array(
						'includes/class-yith-ppraf-frontend.php',
					),
					'admin'    => array(
						'includes/class-yith-ppraf-admin.php',
						'includes/class-yith-ppraf-wp-list-table.php',
					),
				)
			);

			$this->require_( $require );

			$this->init_classes();

			$this->init();

		}
		/**
		 *
		 * Require_()
		 *
		 * @param mixed $main_classes each document of the folder include.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_PPRAF_DIR_PATH . $class ) ) {
						require_once YITH_PPRAF_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			YITH_PPRAF_Shortcode::get_instance();
			YITH_PPRAF_User_List::get_instance();
			YITH_PPRAF_UserList_Email::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( ! is_admin() ) {
				$this->frontend = YITH_PPRAF_Frontend::get_instance();

			}
			if ( is_admin() ) {
				$this->admin = YITH_PPRAF_Admin::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'yith_ppraf_plugin_skeleton' ) ) {
	/**
	 * Yith_ppraf_plugin_skeleton
	 *
	 * @return YITH_PPRAF_Plugin_Skeleton
	 */
	function yith_ppraf_plugin_skeleton() {
		return YITH_PPRAF_Plugin_Skeleton::get_instance();
	}
}
