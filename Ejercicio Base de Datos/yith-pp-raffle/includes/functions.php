<?php
/**
 *
 * Common Functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_ppraf_get_template' ) ) {
	/**
	 * Yith_ppraf_get_template
	 *
	 * @param  mixed $file_name File path.
	 * @param  mixed $args Variables needed.
	 * @return void
	 */
	function yith_ppraf_get_template( $file_name, $args = array() ) {

		$full_path = YITH_PPRAF_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_ppraf_get_view' ) ) {	
	/**
	 * Yith_pptm_get_view
	 *
	 * @param  mixed $file_name  Name of the desired file.
	 * @param  mixed $args Arguments that want pass to the file.
	 * @return void
	 */
	function yith_ppraf_get_view( $file_name, $args = array() ) {

		$full_path = YITH_PPRAF_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_insert_raffle' ) ) {
	/**
	 * Yith_insert_raffle
	 *
	 * @param  mixed $name Name of the desired person.
	 * @param  mixed $lastname Lastname of the desired person.
	 * @param  mixed $email Email of the desired person.
	 * @return void
	 */
	function yith_insert_raffle( $name, $lastname, $email ) {

		global $wpdb;

		$data = array(
			'name'     => $name,
			'lastname' => $lastname,
			'email'    => $email,
		);

		$wpdb->insert( 'wp_yith_raffle', $data ); // db call ok.

	}
}

if ( ! function_exists( 'get_raffle' ) ) {
	/**
	 * Get_raffle
	 *
	 * @return Object|null
	 */
	function get_raffle() {
		global $wpdb;

		return $wpdb->get_results( 'SELECT * FROM wp_yith_raffle' ); // db call ok; no-cache ok.
	}
}
if ( ! function_exists( 'get_raffle_by_email' ) ) {
	/**
	 * Get_raffle_by_name
	 *
	 * @param  mixed $email Email of the desired person.
	 * @return Object|null
	 */
	function get_raffle_by_name( $email ) {
		global $wpdb;
		return $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM wp_yith_raffle WHERE email = %s', $email ) ); // db call ok; no-cache ok.
	}
}
if ( ! function_exists( 'yith_ppraf_get_raffle_names' ) ) {
	/**
	 * Yith_ppraf_get_raffle_names
	 *
	 * @return ARRAY
	 */
	function yith_ppraf_get_raffle_names() {

		$i            = null;
		$records      = get_raffle();
		$records_size = count( $records );
		for ( $i = 0; $i < $records_size; $i ++ ) {
			$user_list[] = $records[ $i ]->name;
		}

		return $user_list;
	}
}

if ( ! function_exists( 'yith_ppraf_send_email' ) ) {

	/**
	 * Yith_ppraf_send_email
	 *
	 * @param  mixed $header Header message that you want to display.
	 * @return void
	 */
	function yith_ppraf_send_email( $header ) {

		$email_to = get_option( 'admin_email' );
		$message  = '';
		$u_list   = yith_ppraf_get_raffle_names();
		foreach ( $u_list as $user ) {
			$message .= '-' . $user . "\r\n";
		}

		wp_mail( 'ppa9819@gmail.com', 'Raffle-User-List', $message, $header );

	}
}
