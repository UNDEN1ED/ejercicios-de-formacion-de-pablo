<?php
/**
 *
 * Admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPRAF_Admin' ) ) {

	/**
	 * YITH_PPRAF_Frontend
	 */
	class YITH_PPRAF_Admin {


		/**
		 * Main instance
		 *
		 * @var instance
		 */
		private static $instance;

		/**
		 * User_list_table
		 *
		 * @var mixed
		 */
		private $user_list_table;

		/**
		 * Plugin_name
		 *
		 * @var string
		 */
		public $plugin_name = 'yith-raffle';

		/**
		 * Ｇet_instance
		 *
		 * @return YITH_PPRAF_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			$this->add_plugin_admin_menu();
		}



		/**
		 * Add_plugin_admin_menu
		 *
		 * @return void
		 */
		public function add_plugin_admin_menu() {

			$page_hook = add_users_page(
				__( 'WP User-List Raffle' ), // page title.
				__( 'WP User-List Raffle' ), // menu title.
				'manage_options', // capability.
				$this->plugin_name, // menu_slug.
				array( $this, 'load_user_list_table' )
			);

			/*
			 * The $page_hook_suffix can be combined with the load-($page_hook) action hook
			 * https://codex.wordpress.org/Plugin_API/Action_Reference/load-(page)
			 *
			 * The callback below will be called when the respective page is loaded
			 */
			add_action( 'load-' . $page_hook, array( $this, 'load_user_list_table_screen_options' ) );
		}


		/**
		 * Load_user_list_table_screen_options
		 *
		 * @return void
		 */
		public function load_user_list_table_screen_options() {
			$arguments = array(
				'label'   => __( 'Users Per Page' ),
				'default' => 5,
				'option'  => 'users_per_page',
			);
			add_screen_option( 'per_page', $arguments );
			/*
			 * Instantiate the User List Table. Creating an instance here will allow the core WP_List_Table class to automatically
			 * load the table columns in the screen options panel
			 */
			$this->user_list_table = YITH_PPRAF_WP_List_Table::get_instance();
		}
		/*
		* Display the User List Table
		* Callback for the add_users_page() in the add_plugin_admin_menu() method of this class.
		*/

		/**
		 * Load_user_list_table
		 *
		 * @return void
		 */
		public function load_user_list_table() {
			// query, filter, and sort the data.
			$this->user_list_table->prepare_items();

				// render the List Table.

			include_once YITH_PPRAF_DIR_VIEWS_PATH . '/yith-ppraf-user-list.php';

		}


	}
}
