<?php
/**
 * Updated List of users from db->yith_pp_raffle
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPRAF_User_List' ) ) {
	/**
	 * YITH_PPRAF_User_List
	 */
	class YITH_PPRAF_User_List {
		/**
		 * Main instance
		 *
		 * @var YITH_PPRAF_User_List
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPRAF_User_List
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
		}

		/**
		 * List_set_up
		 *
		 * @return ARRAY
		 */
		public static function list_set_up() {
			$i         = null;
			$user_list = array();

			$user_list = get_transient( 'yith_ppraf_transient_user_list' );

			if ( false === $user_list ) {

				$records = get_raffle();

				$records_size = count( $records );

				if ( $records_size > 5 ) {
					$records_index = $records_size - 5;
				} else {
					$records_index = 0;
				}

				for ( $i = $records_size - 1; $i >= $records_index; $i -- ) {
					$user_list[] = $records[ $i ]->name;
				}

				set_transient( 'yith_ppraf_transient_user_list', $user_list, 24 * HOUR_IN_SECONDS );
			}

			return $user_list;

		}
	}
}
