<?php
/**
 *
 * Custom WP_LIST_TABLE RAFFLE
 *
 * @package WordPress
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list.php';
}

/**
 * YITH_PPRAF_WP_List_Table
 */
class YITH_PPRAF_WP_List_Table extends WP_List_Table {

	/**
	 * Main Instance
	 *
	 * @var YITH_PPRAF_WP_List_Table
	 */
	private static $instance;

	/**
	 * Get_instance
	 *
	 * @return YITH_PPRAF_WP_List_Table
	 */
	public static function get_instance() {
		return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
	}

	/**
	 * Get_columns
	 *
	 * @return ARRAY
	 */
	public function get_columns() {
		$table_columns = array(
			'cb'        => '<input type="checkbox" />', // to display the checkbox.
			'user_id'   => __( 'User Id' ),
			'firstname' => __( 'Firstname' ),
			'lastname'  => __( 'Lastname' ),
			'email'     => __( 'Email' ),
			'action'    => __( 'Action' ),

		);
		return $table_columns;
	}

	/**
	 * No_items
	 *
	 * @return void
	 */
	public function no_items() {
		__e( 'No users avaliable.' );
	}

	/**
	 * Prepare_items
	 *
	 * @return void
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();
		$this->process_bulk_action();
		$table_data = $this->fetch_table_data();

		$this->items = $table_data;

	}

	/**
	 * Fetch_table_data
	 *
	 * @return ARRAY|Object|NULL
	 */
	public function fetch_table_data() {
		global $wpdb;
		$wpdb_table = $wpdb->prefix . 'yith_raffle';
		$orderby    = ( isset( $_GET['orderby'] ) ) ? esc_sql( $_GET['orderby'] ) : 'firstname';
		$order      = ( isset( $_GET['order'] ) ) ? esc_sql( $_GET['order'] ) : 'ASC';

		$user_query = "SELECT 
						  *
						FROM 
						  $wpdb_table 
						ORDER BY $orderby $order";

		// query output_type will be an associative array with ARRAY_A.
		$query_results = $wpdb->get_results( $user_query, ARRAY_A );// db call ok; no-cache ok.

		// return result array to prepare_items.
		return $query_results;
	}

	/**
	 * Column_default
	 *
	 * @param  mixed $item Each row.
	 * @param  mixed $column_name Name of the column.
	 * @return String
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'user_firstname':
			case 'user_lastname':
			case 'user_email':
			case 'user_id':
				return $item[ $column_name ];
			default:
				return $item[ $column_name ];
		}
	}

	/**
	 * Column_action
	 *
	 * @param  mixed $item Action row.
	 * @return String|False
	 */
	public function column_action( $item ) {
		$actions = array(
			'delete' => sprintf( '<a href="?page=yith-raffle&action=%s&user_id=%s">Delete</a>', 'delete', $item['user_id'] ),
		);
		return sprintf( '' . $this->row_actions( $actions, true ) . '' );
	}

	/**
	 * Process_bulk_action
	 *
	 * @return void
	 */
	public function process_bulk_action() {
		if ( 'delete' === $this->current_action() ) {
			$this->delete_user_raffle( $_REQUEST['user_id'] );
		}
	}

	/**
	 * Delete_user_raffle
	 *
	 * @param  mixed $user_id Id of the user from the database.
	 * @return void
	 */
	public function delete_user_raffle( $user_id ) {
		global $wpdb;
		$wpdb->delete( 'wp_yith_raffle', array( 'user_id' => $user_id ) );
	}




}
