<?php
/**
 * Plugin Name: YITH Raffle
 * Plugin URI: http://local.wordpress.test/
 * Description: Create your own raffles. Make your viewers and users able to join raffles.
 * Author: Pablo Pérez Arteaga
 * Text Domain: yith_pp_raffles
 * Version: 1.7.2
 * Author URI:
 *
 * @package WordPress
 */

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'YITH_PPRAF_VERSION' ) ) {
	define( 'YITH_PPRAF_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PPRAF_DIR_URL' ) ) {
	define( 'YITH_PPRAF_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PPRAF_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PPRAF_DIR_ASSETS_URL', YITH_PPRAF_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PPRAF_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PPRAF_DIR_ASSETS_CSS_URL', YITH_PPRAF_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PPRAF_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PPRAF_DIR_ASSETS_JS_URL', YITH_PPRAF_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PPRAF_DIR_PATH' ) ) {
	define( 'YITH_PPRAF_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PPRAF_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PPRAF_DIR_INCLUDES_PATH', YITH_PPRAF_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PPRAF_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PPRAF_DIR_TEMPLATES_PATH', YITH_PPRAF_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PPRAF_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PPRAF_DIR_VIEWS_PATH', YITH_PPRAF_DIR_PATH . '/views' );
}

if ( ! function_exists( 'yith_ppraf_init_classes' ) ) {

	/**
	 * Yith_ppraf_init_classes
	 *
	 * @return void
	 */
	function yith_ppraf_init_classes() {

		load_plugin_textdomain( 'yith_pp_raffles', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PPRAF_DIR_INCLUDES_PATH . '/class-yith-ppraf-plugin-skeleton.php';

		if ( class_exists( 'YITH_PPRAF_Plugin_Skeleton' ) ) {

			yith_ppraf_plugin_skeleton();

		}
	}
}

if ( ! function_exists( 'yith_ppraf_install' ) ) {

	/**
	 * Yith_ppraf_install
	 *
	 * @return void
	 */
	function yith_ppraf_install() {

		require_once YITH_PPRAF_DIR_INCLUDES_PATH . '/class-yith-ppraf-db.php';
		YITH_PPRAF_DB::install();
	}
}

add_action( 'plugins_loaded', 'yith_ppraf_install', 11 );
add_action( 'plugins_loaded', 'yith_ppraf_init_classes', 11 );
