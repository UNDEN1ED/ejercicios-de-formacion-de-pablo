//Realizo una pequeña función para introducir los valores del post pasados del .php al div
//al ejecutarse la función que va ligada al boton y cambio el estilo del div a visible en caso de que ya haya sido usado el boton antes se esconde de nuevo el div
var button_used= false;
let fillData = () =>{
    if(button_used == false){
        let ele = document.getElementById('box');
        ele.innerHTML+="Nombre del autor: " + String(meta_list.author_name)
                        + "<br/>Email del autor: " + String(meta_list.author_email)
                        + "<br/>Fecha de publicación: " + String(meta_list.publish_date);
        ele.style.visibility="visible";

        button_used = true;
    }else{
        let ele = document.getElementById('box');
        ele.innerHTML="";
        ele.style.visibility="hidden";
        button_used= false;
    }
    
}