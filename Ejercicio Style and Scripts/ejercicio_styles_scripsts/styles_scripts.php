<?php

/*
Plugin Name: Ejercicio Styles &a Scripts
Plugin URI: http://local.wordpress.test/
Description: Crear una caja de información en los Post, con los datos del author del post y la fecha del post. Dicha caja aparecerá cerrada al cargar la página,
            y se podrá abrir y cerrar al hacer click en un botón. El estilo de la caja y contenido es libre, pero se valora que se añadan colores al texto, fondo, etc
Author: Pablo Pérez Arteaga
Version: 1.7.2
Author URI: 
*/


defined('ABSPATH') or die("Bye bye");

define('ASSET_RUTA',plugin_dir_url(__FILE__)."assets/");
    

//### En este plugin se recogerá información del autor del post y se presentará en un div que se muestra con un boton

//Realizo una función  para cargar la hoja de estilo solo en los post
function yith_ready_scripts_styles(){

    if(is_single()){
    wp_enqueue_style('yith_mystylesfile', ASSET_RUTA .  "css/yith_stylesheet.css");
    }

};



//En esta otra función obtengo los valores deseados para mostrar y los guardo en un array que pasaré al .js 
//y cargo ese mismo archivo y creo la estructura del boton y el div
function yith_add_div_meta_info($content){
    if(is_single()){
        $meta_data_values =array(
            'author_name' => get_the_author_meta('display_name'),
            'author_email' => get_the_author_meta('user_email'),
            'publish_date' => get_the_date()
        );
        wp_register_script('yith_myscriptsfile', ASSET_RUTA . "js/yith_script.js");
        wp_localize_script('yith_myscriptsfile', 'meta_list', $meta_data_values);
        wp_enqueue_script('yith_myscriptsfile');
    
        $original_content = $content;
        $add_content = "<div id='box'></div>";
        $add_content .= "<div><button type='button' onclick='fillData()'>Post info</button></div>";
        $content = $original_content . $add_content;
    }
    return $content;
};

//Vinculo las funciones la hook correspondiente
add_action('wp_enqueue_scripts', 'yith_ready_scripts_styles');
add_filter('the_content', 'yith_add_div_meta_info');
    
/*
wp_register_script('yith_myscriptsfile', ASSET_RUTA . "js/script.js");
    wp_localize_script('yith_myscriptsfile', 'meta_list', $meta_data_values);
    wp_enqueue_script('yith_myscriptsfile');
*/
?>


