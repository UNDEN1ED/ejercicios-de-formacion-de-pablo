<?php
/*
Plugin Name: Ejercicio 1 shortcodes
Plugin URI: http://local.wordpress.test/
Description: Adds the content of a given post
Author: Pablo Pérez Arteaga
Text Domain: ejercicio_shortcode2
Version: 1.7.2
Author URI: 
*/

function yith_shrtcd2_load_plugin_textdomain() {
    load_plugin_textdomain( 'ejercicio_shortcode2', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'yith_shrtcd2_load_plugin_textdomain' );


// Creo una función que obtendrá el contenido de un post dado y lo mostrára en el current post 
function yith_post_content_by_id($atts){
    $a = shortcode_atts(array(
        'post_id' => '1'
    ), $atts);
    
    $post = get_post($a['post_id']);
    ob_start();
    if( ! $post == null){
        $content = $post->post_content;
        echo '<p>' . $content . '</p>';
    }else{
        echo "<p class='error-post'>" . esc_html__('The ID of the post does not exist', 'ejercicio_shortcode2') . "</p>";
    };
    return ob_get_clean();
}

//Action para controlar cuando se cargará el shortcode
function yith_add_custom_shortcode() {
    add_shortcode('yith_post_content_id', 'yith_post_content_by_id');
}

add_action('init', 'yith_add_custom_shortcode');

?>