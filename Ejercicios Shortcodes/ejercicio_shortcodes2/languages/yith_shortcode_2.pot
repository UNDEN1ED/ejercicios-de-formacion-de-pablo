# Copyright (C) 2020 Pablo Pérez Arteaga
# This file is distributed under the same license as the Ejercicio 1 shortcodes plugin.
msgid ""
msgstr ""
"Project-Id-Version: Ejercicio 1 shortcodes 1.7.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/ejercicio_shortcodes2\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-10-15T11:53:15+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.5.0-alpha-9061b3e\n"
"X-Domain: ejercicio_shortcode2\n"

#. Plugin Name of the plugin
msgid "Ejercicio 1 shortcodes"
msgstr ""

#. Plugin URI of the plugin
msgid "http://local.wordpress.test/"
msgstr ""

#. Description of the plugin
msgid "Adds the content of a given post"
msgstr ""

#. Author of the plugin
msgid "Pablo Pérez Arteaga"
msgstr ""

#: init.php:30
msgid "The ID of the post does not exist"
msgstr ""
