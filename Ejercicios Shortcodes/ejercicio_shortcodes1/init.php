<?php
/*
Plugin Name: Retrive last 5 posts
Plugin URI: http://local.wordpress.test/
Description: Add the title of the last 5 posts
Author: Pablo Pérez Arteaga
Text Domain: ejercicio_shortcodes1
Version: 1.7.2
Author URI: 
*/

//### Con este plugin  se genera un shortcode que se usa para obtener el titulo de los 5 ultimos posts y los mostrará.

function yith_shrtcd1_load_plugin_textdomain() {
    load_plugin_textdomain( 'ejercicio_shortcodes1', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'yith_shrtcd1_load_plugin_textdomain' );

//Esta funcion cogerá los titulos posts y mostrandolos
function yith_retrieve_last_5_posts(){
    $args= array(
        'numberposts' => 5
    );

    $posts= get_posts($args);

    ob_start();

    if(!count($posts) == 0){
        foreach($posts as $post_l){
            echo  "<p>- ".$post_l->post_title ."</p>";
        }
    }else{
        echo "<p class='error-post'>" . esc_html__('There are no posts in the database', 'ejercicio_shortcodes1') . "</p>"
    };
    
    return ob_get_clean();
}

//Función  que controlará cuando se aplicará el shortcode
function wpdocs_add_custom_shortcode() {
    add_shortcode('yith_retrieve_5_posts', 'yith_retrieve_last_5_posts');
}
add_action( 'init', 'wpdocs_add_custom_shortcode' );

?>