# Copyright (C) 2020 Pablo Pérez Arteaga
# This file is distributed under the same license as the Retrive last 5 posts plugin.
msgid ""
msgstr ""
"Project-Id-Version: Retrive last 5 posts 1.7.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/ejercicio_shortcodes1\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-10-15T11:36:57+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.5.0-alpha-9061b3e\n"
"X-Domain: ejercicio_shortcodes1\n"

#. Plugin Name of the plugin
msgid "Retrive last 5 posts"
msgstr ""

#. Plugin URI of the plugin
msgid "http://local.wordpress.test/"
msgstr ""

#. Description of the plugin
msgid "Add the title of the last 5 posts"
msgstr ""

#. Author of the plugin
msgid "Pablo Pérez Arteaga"
msgstr ""

#: init.php:34
msgid "There are no posts in the database"
msgstr ""
