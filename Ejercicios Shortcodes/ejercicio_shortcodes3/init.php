<?php
/*
Plugin Name: Ejercicio 3 shortcodes
Plugin URI: http://local.wordpress.test/
Description: Shortcode that show user's details: name, surname, nickname 
Author: Pablo Pérez Arteaga
Text Domain: ejercicio_shortcode3
Version: 1.7.2
Author URI: 
*/

function yith_shrtcd3_load_plugin_textdomain() {
    load_plugin_textdomain( 'ejercicio_shortcode3', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'yith_shrtcd3_load_plugin_textdomain' );

//Funcion del shortcode dando el id del usuario coja sus datos y los muestre
function yith_meta_data_by_user($atts){

    $a = shortcode_atts(array(
        'user_id' => '2'
    ), $atts);
    
    $user_id = $a['user_id'];
    ob_start();
    $user= get_userdata($user_id);
    if($user){
        $user_name =apply_filters('yith_before_display_user_first_name', $user->first_name);
        $user_lastname = $user->last_name;
        $user_nickname = $user->user_login;
    
        
        do_action('hook_before_content_ppa');
    
        echo "<p>" . esc_html__('Name', 'ejercicio_shortcode3') . " :"  . $user_name . "</p>"
            . "<p>" . esc_html__('Lastname', 'ejercicio_shortcode3') . " :"  . $user_lastname . "</p>"
            . "<p>" . esc_html__('Nickname', 'ejercicio_shortcode3') . " :"  . $user_nickname . "</p>";

        do_action('hook_after_content_ppa', $user_id );
    
        
    }else{
        echo "<p class='error-post'>" . esc_html__('Could not find the designated user', 'ejercicio_shortcode3') . "</p>";
    }

        return ob_get_clean();
};

//Action para controlar cuando se cargará el shortcode
function yith_shortcode() {
    add_shortcode('yith_meta_data_user', 'yith_meta_data_by_user');
};
add_action( 'init', 'yith_shortcode' );