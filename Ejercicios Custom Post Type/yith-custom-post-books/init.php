<?php

/*
Plugin Name: Custom Post Books
Plugin URI: http://local.wordpress.test/
Description: Adds a new type of posts in which you can store books
Author: Pablo Pérez Arteaga
Text Domain: yith_custom_post_books
Version: 1.7.2
Author URI: 
*/


//This function will load the translation file 
function yith_cptlib_load_plugin_textdomain() {
    load_plugin_textdomain( 'yith_custom_post_books', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'plugins_loaded', 'yith_cptlib_load_plugin_textdomain' );


//Starting creating the function for our Custom Post Type
    function  yith_cptlib_register(){

        //This array contains all the labels that we want to appear in our dashboard when managing our custom posts
        $labels = array(

            'name' => __('Books', 'yith_custom_post_books'),
            'singular_name' => __('Book','yith_custom_post_books'),
            'add_new' => __('Add New Book', 'yith_custom_post_books'),
            'add_new_item' => __('Add New Book', 'yith_custom_post_books'),
            'edit_item' => __('Edit Book', 'yith_custom_post_books'),
            'new_item' => __('New Book', 'yith_custom_post_books'),
            'view_item' => __('View Book', 'yith_custom_post_books'),
            'search_items' => __('Search Books', 'yith_custom_post_books'),
            'not_found' => __('No Books found', 'yith_custom_post_books'),
            'not_found_in_trash' => __('No Books found in Trash', 'yith_custom_post_books'),
            'all_items' => __('All Books', 'yith_custom_post_books')
        );
        //Here it will give all the option that our custom post will use like the labels, and the type of custom post
        $args = array(
            'labels' => $labels,
            'description' => __('Post type to store books and display it like each one separately', 'yith_custom_post_books'),
            'public' => true,
            'publicly_ queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'menu_icon' => 'dashicons-book-alt',
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 20,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'comments')


        );
        //Register the custom post with all the arguments that we want it to use
        register_post_type('books', $args);

    }
//Hook the main function with the init
    add_action('init', 'yith_cptlib_register');

?>