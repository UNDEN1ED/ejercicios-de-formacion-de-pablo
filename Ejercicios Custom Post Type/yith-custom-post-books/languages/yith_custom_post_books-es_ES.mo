��          �      �       H     I  5   V  	   �     �     �     �  	   �     �     �     �  @   �     .  	   ;  �  E     �  ;   �     .     ?     E     L     l     y     �  $   �  >   �       	                               
      	                               Add New Book Adds a new type of posts in which you can store books All Books Book Books Custom Post Books Edit Book New Book No Books found No Books found in Trash Post type to store books and display it like each one separately Search Books View Book Project-Id-Version: Custom Post Books 1.7.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith_custom_post_books
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-10-16 14:15+0100
X-Generator: Poedit 2.4.1
X-Domain: yith_custom_post_books
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Añadir nuevo Libro Añade un nuevo tipo de entrada donde puedes guardar libros Todos los Libros Libro Libros Entrada Personalizada de Libros Editar Libro Nuevo Libro Ningún Libro encontrado Ningún Libro encontrado en Papelera Tipo de Entrada para guardar libros y mostrarlos separadamente Buscar Libros Ver Libro 