function onclickBadgecheck() {
    if (document.getElementById("yith_pptm_toggle_badge").checked) {
        document.getElementById('yith_pptm_badge_options').style.display = "block";
    } else {
        document.getElementById('yith_pptm_badge_options').style.display = "none";
    }
}

document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById("yith_pptm_toggle_badge").checked) {
        document.getElementById('yith_pptm_badge_options').style.display = "block";
    }
}, false);

jQuery(document).ready(function($) {
    $('.my-color-field').wpColorPicker();
});