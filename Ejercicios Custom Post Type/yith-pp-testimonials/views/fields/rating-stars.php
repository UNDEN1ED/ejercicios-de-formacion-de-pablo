<?php
/**
 *
 * Ratings Stars
 *
 * @package WordPress
 */

global $post;
$rating_stars = get_post_meta( $post->ID, 'yith_pptm_field_rate', true );
?>
<div class="rating">
	<input type="radio" id="star5" name="<?php echo esc_html( $args['name'] ); ?>"  class="yith_pptm_radio_buttons" value="5" 
	<?php
	checked( '5', $rating_stars );
	?>
	/>
	<label for="star5" title="text">★</label>
	<input type="radio" id="star4" name="<?php echo esc_html( $args['name'] ); ?>" class="yith_pptm_radio_buttons" value="4" 
	<?php
	checked( '4', $rating_stars );
	?>
	/>
	<label for = 'star4' title = 'text' >★</label>
	<input type = 'radio' id = 'star3' name = '<?php echo esc_html( $args['name'] ); ?>' class = 'yith_pptm_radio_buttons'  value = '3'
	<?php
	checked( '3', $rating_stars );
	?>
	/>
	<label for="star3" title="text">★</label>
	<input type="radio" id="star2" name="<?php echo esc_html( $args['name'] ); ?>" class="yith_pptm_radio_buttons" value="2" 
	<?php
	checked( '2', $rating_stars );
	?>
	/>
	<label for="star2" title="text">★</label>
	<input type="radio" id="star1" name="<?php echo esc_html( $args['name'] ); ?>" class="yith_pptm_radio_buttons" value="1" 
	<?php
	checked( '1', $rating_stars );
	?>
	/>
	<label for="star1" title="text">★</label>
</div>
