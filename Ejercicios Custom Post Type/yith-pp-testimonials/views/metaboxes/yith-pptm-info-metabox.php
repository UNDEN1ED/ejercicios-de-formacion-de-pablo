<?php
/**
 *
 * Metabox View
 *
 * @package WordPress
 */

wp_nonce_field( 'rate', 'yith_pptm_rating' );
$rating_stars = get_post_meta( $post->ID, '_yith_pptm_meta_field_rating', true );
?>

<label for= "yith_pptm_field"><?php esc_html_e( 'Role: ', 'yith_pp_testimonials' ); ?></label>
<input type="text" name="yith_pptm_field_role" class="yith_pptm_field" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pptm_meta_field_role', true ) ); ?>">
<br><br>
<label for="yith_pptm_field"><?php esc_html_e( 'Company: ', 'yith_pp_testimonials' ); ?></label>
<input type="text" name="yith_pptm_field_company" class="yith_pptm_field" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pptm_meta_field_company', true ) ); ?>">
<br><br>
<label for="yith_pptm_field"><?php esc_html_e( 'Web-site: ', 'yith_pp_testimonials' ); ?></label>
<input type="text" name="yith_pptm_field_website" class="yith_pptm_field" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pptm_meta_field_website', true ) ); ?>">
<br><br>
<label for="yith_pptm_field"><?php esc_html_e( 'Email: ', 'yith_pp_testimonials' ); ?></label>
<input type="text" name="yith_pptm_field_email" class="yith_pptm_field" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pptm_meta_field_email', true ) ); ?>">
<br><br>
<label for="yith_pptm_field"><?php esc_html_e( 'Rating: ', 'yith_pp_testimonials' ); ?></label>
<div class="rating">
	<input type="radio" id="star5" name="rate"  class="yith_pptm_radio_buttons" value="5" 
	<?php
	if ( '5' === $rating_stars ) {
		echo esc_html( 'checked' );}
	?>
		/>
	<label for="star5" title="text">★</label>
	<input type="radio" id="star4" name="rate" class="yith_pptm_radio_buttons" value="4" 
	<?php
	if ( '4' === $rating_stars ) {
		echo esc_html( 'checked' );}
	?>
		/>
	<label for="star4" title="text">★</label>
	<input type="radio" id="star3" name="rate" class="yith_pptm_radio_buttons"  value="3" 
	<?php
	if ( '3' === $rating_stars ) {
		echo esc_html( 'checked' );}
	?>
		/>
	<label for="star3" title="text">★</label>
	<input type="radio" id="star2" name="rate" class="yith_pptm_radio_buttons" value="2" 
	<?php
	if ( '2' === $rating_stars ) {
		echo esc_html( 'checked' );}
	?>
	/>
	<label for="star2" title="text">★</label>
	<input type="radio" id="star1" name="rate" class="yith_pptm_radio_buttons" value="1" 
	<?php
	if ( '1' === $rating_stars ) {
		echo esc_html( 'checked' );}
	?>
	/>
	<label for="star1" title="text">★</label>
</div>
<br>
<label for="yith_pptm_field"><?php esc_html_e( 'VIP: ', 'yith_pp_testimonials' ); ?></label>
<label class="switch">
	<input type="checkbox" name="yith_pptm_field_vip" value="on" 
	<?php
	if ( get_post_meta( $post->ID, '_yith_pptm_meta_field_vip', true ) === 'on' ) {
		echo esc_html( 'checked' );}
	?>
	>
	<span class="slider"></span>
</label>
<br><br>
<label for="yith_pptm_field"><?php esc_html_e( 'Badge: ', 'yith_pp_testimonials' ); ?> </label>
<label class="switch ">
	<input type="checkbox" id="yith_pptm_toggle_badge" name="yith_pptm_field_badge" value="on" onclick="onclickBadgecheck()" 
	<?php
	if ( get_post_meta( $post->ID, '_yith_pptm_meta_field_badge', true ) === 'on' ) {
		echo esc_html( 'checked' );}
	?>
	>
	<span class="slider"></span>
</label><br><br>
<div id="yith_pptm_badge_options">
	<label for="yith_pptm_field"><?php esc_html_e( 'Badge Name: ', 'yith_pp_testimonials' ); ?></label>
	<input type="text" name="yith_pptm_field_badge_name" class="yith_pptm_badge_name" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pptm_meta_field_badge_name', true ) ); ?>">
	<br><br>
	<label for="yith_pptm_field"><?php esc_html_e( 'Badge Color: ', 'yith_pp_testimonials' ); ?></label>
	<input type="text" name="yith_pptm_field_badge_color" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pptm_meta_field_badge_color', true ) ); ?>" class="my-color-field" data-default-color="rgb(98, 202, 98)" />
</div>
