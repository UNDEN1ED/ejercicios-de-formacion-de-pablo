<?php
/**
 *
 * PPWCN_Options View
 *
 * @package WordPress
 */

?>

<div class="wrap">
	<h1><?php esc_html_e( 'Settings' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'pptm-options-page' );
			do_settings_sections( 'pptm-options-page' );
			submit_button();
		?>
	</form>
</div>
