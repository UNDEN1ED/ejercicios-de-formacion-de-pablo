<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package WordPress
 */

wp_nonce_field( 'yith_pptm_rating', 'rate' );

?>

<div class='yith_pptm_container'>
<?php
foreach ( $posts_list as $post_item ) {
	?>
	<div class='yith_pptm_item 
	<?php
	if ( $a['hover_effect'] == 'highlight' ) {
		?>
		yith_pptm_hover_highlight
		<?php
	} elseif ( $a['hover_effect'] == 'zoom' ) {
		?>
		yith_pptm_hover_zoom
		<?php
	}
	if ( get_post_meta( $post_item->ID, '_yith_pptm_meta_field_vip', true ) == 'on' ) {
		?>
		yith_pptm_vip
		<?php
	}
	?>
	'>
	<?php

	if ( 'yes' === $a['show_image'] && get_the_post_thumbnail_url( $post_item->ID ) !== false ) {
		?>
		<img src='<?php echo esc_html( get_the_post_thumbnail_url( $post_item->ID ) ); ?>' class='yith_pptm_thumbnail' alt='No' loading='lazy' />
		<?php
	}
	if ( get_post_meta( $post_item->ID, '_yith_pptm_meta_field_badge', true ) == 'on' ) {
		?>
		<div class='yith_pptm_badge' style='background-color:<?php echo get_post_meta( $post_item->ID, 'yith_pptm_field_badge_color', true ); ?>'><?php echo get_post_meta( $post_item->ID, 'yith_pptm_field_badge_text', true ); ?></div>
		<?php
	}


	if ( has_term( '', 'survivors', $post_item->ID ) || has_term( '', 'country', $post_item->ID ) ) {

		$taxo_list  = get_object_taxonomies( 'yith_testimonials' );
		$terms_list = wp_get_object_terms( $post_item->ID, $taxo_list, array( 'fields' => 'names' ) );
		?>
			<p class="yith_pptm_taxonomies">
		<?php

		foreach ( $terms_list as $term ) {
			if ( $term == end( $terms_list ) ) {
				echo $term;
			} else {
				echo $term . ', ';
			}
		}

		?>
		</p>
		<?php
	}

	?>
		<p class='yith_pptm_title'><?php echo $post_item->post_title; ?></p>
		<p class='yith_pptm_content'><?php echo $post_item->post_content; ?></p>
		<p class='yith_pptm_role'>
		<?php
		echo get_post_meta( $post_item->ID, 'yith_pptm_field_role', true );
		if ( get_post_meta( $post_item->ID, 'yith_pptm_field_website', true ) != '' ) {
			esc_html_e( ' at ', 'yith_pp_testimonials' );}
		?>
		<a href='<?php echo get_post_meta( $post_item->ID, 'yith_pptm_field_website', true ); ?>'><?php echo get_post_meta( $post_item->ID, 'yith_pptm_field_company', true ); ?></a></p>
					<p class='yith_pptm_email'><a href=#><?php echo get_post_meta( $post_item->ID, 'yith_pptm_field_email', true ); ?></a></p>
					<?php
					if ( get_post_meta( $post_item->ID, 'yith_pptm_field_rate', true ) != '' ) {
						yith_pptm_rating_stars( get_post_meta( $post_item->ID, 'yith_pptm_field_rate', true ) );}
					?>
					  
	</div>
	<?php
}
?>
</div>
