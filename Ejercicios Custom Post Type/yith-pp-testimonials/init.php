<?php
/**
 * Plugin Name: Custom Post "Testimonials"
 * Plugin URI: http://local.wordpress.test/
 * Description: Custom Post Type Testimonials store Name, Message and a Picture. It can only be shown by it respective shortcode in any other post type
 * Author: Pablo Pérez Arteaga
 * Text Domain: yith_pp_testimonials
 * Version: 1.7.2
 * Author URI:
 *
 * @package WordPress
 */

// Starting creating the function for our Custom Post Type.


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Starting creating the function for our Custom Post Type.

if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	define( 'YITH_PPTM_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PPTM_DIR_URL' ) ) {
	define( 'YITH_PPTM_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PPTM_FILE' ) ) {
	define( 'YITH_PPTM_FILE', __FILE__ );
}

if ( ! defined( 'YITH_PPTM_SLUG' ) ) {
	define( 'YITH_PPTM_SLUG', 'yith_pp_testimonials' );
}
if ( ! defined( 'YITH_PPTM_INIT' ) ) {
	define( 'YITH_PPTM_INIT', plugin_basename( __FILE__ ) );
}
if ( ! defined( 'YITH_PPTM_SECRETKEY' ) ) {
	define( 'YITH_PPTM_SECRETKEY', 'zd9egFgFdF1D8Azh2ifK' );
}

if ( ! defined( 'YITH_PPTM_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PPTM_DIR_ASSETS_URL', YITH_PPTM_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PPTM_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PPTM_DIR_ASSETS_CSS_URL', YITH_PPTM_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PPTM_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PPTM_DIR_ASSETS_JS_URL', YITH_PPTM_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PPTM_DIR_PATH' ) ) {
	define( 'YITH_PPTM_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PPTM_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PPTM_DIR_INCLUDES_PATH', YITH_PPTM_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PPTM_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PPTM_DIR_TEMPLATES_PATH', YITH_PPTM_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PPTM_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PPTM_DIR_VIEWS_PATH', YITH_PPTM_DIR_PATH . '/views' );
}

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_PPTM_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_PPTM_DIR_PATH . 'plugin-fw/init.php';
}


if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

yit_maybe_plugin_fw_loader( YITH_PPTM_DIR_PATH );
register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );


if ( ! function_exists( 'yith_pptm_init_classes' ) ) {

	/**
	 * Yith_pptm_init_classes
	 *
	 * @return void
	 */
	function yith_pptm_init_classes() {

		load_plugin_textdomain( 'yith_pp_testimonials', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PPTM_DIR_INCLUDES_PATH . '/class-yith-pptm-plugin-skeleton.php';

		if ( class_exists( 'YITH_PPTM_Plugin_Skeleton' ) ) {

			yith_pptm_plugin_skeleton();

		}
	}
}

add_action( 'plugins_loaded', 'yith_pptm_init_classes', 11 );
