<?php
$taxonomy_survivors = array(
	'custom-taxonomy-survivors' => array(
		'custom-taxonomy_list_table' => array(
			'type'     => 'taxonomy',
			'taxonomy' => 'yith_pptm_survivors',
		),
	),
);

return $taxonomy_survivors;
