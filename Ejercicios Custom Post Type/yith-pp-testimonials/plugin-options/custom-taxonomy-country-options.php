<?php
$taxonomy_country = array(
	'custom-taxonomy-country' => array(
		'custom-taxonomy_list_table' => array(
			'type'     => 'taxonomy',
			'taxonomy' => 'yith_pptm_country',
		),
	),
);

return $taxonomy_country;
