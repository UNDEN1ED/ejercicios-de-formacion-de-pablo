<?php
$settings = array(
	'settings' => array(
		'section-1' => array(
			array(
				'name' => __( 'Section Title', 'yith-pp-testimonials' ),
				'type' => 'title',
			),
			array(
				'id'   => 'yith_pptm_shortcode_number',
				'name' => __( 'Number', 'yith-pp-testimonials' ),
				'type' => 'number',
				'std'  => 6,
				'desc' => __( 'This is a text field', 'yith-pp-testimonials' ),
			),
			array(
				'id'   => 'yith_pptm_shortcode_show_image',
				'name' => __( 'Show image', 'yith-pp-testimonials' ),
				'type' => 'checkbox',
				'std'  => true,
				'desc' => __( 'This is a text field', 'yith-pp-testimonials' ),

			),
			array(
				'id'      => 'yith_pptm_shortcode_hover_effect',
				'name'    => __( 'Hover Effect', 'yith-pp-testimonials' ),
				'type'    => 'select',
				'options' => array(
					'none'      => __( 'None', 'yith-pp-testimonials' ),
					'zoom'      => __( 'Zoom', 'yith-pp-testimonials' ),
					'highlight' => __( 'Highlight', 'yith-pp-testimonials' ),
				),

			),
			array(
				'id'   => 'yith_pptm_shortcode_border_radius',
				'name' => __( 'Radius', 'yith-pp-testimonials' ),
				'type' => 'number',
				'std'  => 7,
				'desc' => __( 'This is a text field', 'yith-pp-testimonials' ),
			),
			array(
				'id'   => 'yith_pptm_shortcode_links_color',
				'name' => __( 'Color Picker', 'yith-pp-testimonials' ),
				'type' => 'colorpicker',
				'std'  => '#0073aa',
				'desc' => __( 'This is a color-picker field', 'yith-pp-testimonials' ),
			),
			array(
				'type' => 'close',
			),
		),
	),
);

return apply_filters( 'yith_pptm_settings_options', $settings );
