<?php
$cpt_tab = array(
	'custom-post-type' => array(
		'custom-post-type_list_table' => array(
			'type'      => 'post_type',
			'post_type' => YITH_PPTM_Post_Types::$post_type,
		),
	),
);

return $cpt_tab;
