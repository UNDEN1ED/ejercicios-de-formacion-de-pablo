<?php
/**
 * Skeleton
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_PPTM_Plugin_Skeleton' ) ) {
	/**
	 * YITH_PPTM_Plugin_Skeleton
	 *
	 * @var static $instance instace of the class
	 * @param $frontend frontend main file
	 */
	class YITH_PPTM_Plugin_Skeleton {

		/**
		 * Description
		 *
		 * @var YITH_PPTM_Plugin_Skeleton
		 */

		private static $instance;   // Variable.

		/**
		 * Description
		 *
		 * @var YITH_PPTM_Frontend
		 */

		public $frontend = null;

		/**
		 * Main Frontend Instance
		 *
		 * @var YITH_PPTM_Frontend
		 */

		public $admin = null;


		/**
		 * Get_instance
		 *
		 * @return  YITH_PPTM_Plugin_Skeleton
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
			$require = apply_filters(
				'yith_pptm_require_class',
				array(
					'common'   => array(
						'includes/class-yith-pptm-post-types.php',
						'includes/functions.php',
						'includes/class-yith-pptm-testimonials-shortcode.php',
					),
					'frontend' => array(
						'includes/class-yith-pptm-frontend.php',
					),
					'admin'    => array(
						'includes/class-yith-pptm-admin.php',
						'includes/class-yith-pptm-roles.php',
					),
				)
			);

			$this->require_( $require );

			$this->init_classes();

			$this->init();

		}
		/**
		 *
		 * Require_()
		 *
		 * @param mixed $main_classes each document of the folder include.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PPTM_DIR_PATH . $class ) ) {
						require_once YITH_PPTM_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			YITH_PPTM_Post_Types::get_instance();
			YITH_PPTM_Testimonials_Shortcode::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( ! is_admin() ) {
				$this->frontend = YITH_PPTM_Frontend::get_instance();

			}
			if ( is_admin() ) {
				$this->admin = YITH_PPTM_Admin::get_instance();
				YITH_PPTM_Roles::get_instance();
			}
		}

		/**
		 * Plugin_fw_loader
		 *
		 * @return void
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}
		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_PPTM_INIT, YITH_PPTM_SECRETKEY, YITH_PPTM_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_PPTM_SLUG, YITH_PPTM_INIT );
			}
		}

	}
}

if ( ! function_exists( 'yith_pptm_plugin_skeleton' ) ) {
	/**
	 * Yith_pptm_plugin_skeleton
	 *
	 * @return YITH_PPTM_Plugin_Skeleton
	 */
	function yith_pptm_plugin_skeleton() {
		return YITH_PPTM_Plugin_Skeleton::get_instance();
	}
}
