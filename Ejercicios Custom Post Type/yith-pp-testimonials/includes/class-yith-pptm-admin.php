<?php

use function PHPSTORM_META\type;

if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPTM_Admin' ) ) {

	/**
	 * YITH_PPTM_Admin
	 */
	class YITH_PPTM_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_PPTM_Admin
		 */

		private static $instance;

		/**
		 * Panel
		 *
		 * @var Panel page
		 */
		protected $panel_page = 'yith_pptm_panel';

		/**
		 * Option Panel
		 *
		 * @var YIT_Plugin_Panel $_panel the panel
		 */
		private $_panel;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPTM_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
			add_action( 'save_post_yith_testimonials', array( $this, 'save_meta_box' ) );
			add_action( 'manage_yith_testimonials_posts_custom_column', array( $this, 'display_testimonials_post_type_custom_column' ), 10, 2 );
			add_action( 'admin_init', array( $this, 'register_settings' ) );
			add_action( 'admin_init', array( $this, 'add_metabox' ), 10 );
			add_action( 'yith_pptm_print_custom_field', array( $this, 'show_custom_field_action' ), 10, 1 );
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_PPTM_DIR_PATH . '/' . basename( YITH_PPTM_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_filter( 'manage_yith_testimonials_posts_columns', array( $this, 'add_testimonials_post_type_columns' ) );
		}

		/**
		 * Add_testimonials_post_type_columns
		 *
		 * @param  mixed $post_columns Desired post column.
		 * @return Array
		 */
		public function add_testimonials_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_pptm_custom_columns',
				array(
					'role_column'    => __( 'Role', 'yith_pp_testimonials' ),
					'company_column' => __( 'Company', 'yith_pp_testimonials' ),
					'email_column'   => __( 'Email', 'yith_pp_testimonials' ),
					'stars_column'   => __( 'Stars', 'yith_pp_testimonials' ),
					'vip_column'     => __( 'VIP', 'yith_pp_testimonials' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;

		}


		public function display_testimonials_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {
				case 'role_column':
						echo get_post_meta( $post_id, '_yith_pptm_meta_field_role', true );
					break;
				case 'company_column':
						echo get_post_meta( $post_id, '_yith_pptm_meta_field_company', true );
					break;
				case 'email_column':
						echo get_post_meta( $post_id, '_yith_pptm_meta_field_email', true );
					break;
				case 'stars_column':
						echo get_post_meta( $post_id, '_yith_pptm_meta_field_rating', true );
					break;
				case 'vip_column':
						echo get_post_meta( $post_id, '_yith_pptm_meta_field_vip', true );
					break;
				default:
					do_action( 'yith_pptm_display_custom_column', $column, $post_id );
					break;
			}

		}

		public function create_menu_for_general_options() {

			add_menu_page(
				esc_html__( 'Plugin Testimonials Options', 'yith_pp_testimonials' ),
				esc_html__( 'Plugin Testimonials Options', 'yith_pp_testimonials' ),
				'manage_testimonials',
				'plugin_testimonials_options',
				array( $this, 'testimonials_custom_menu_page' ),
				'',
				40
			);
		}

		function testimonials_custom_menu_page() {
			yith_pptm_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Register_settings
		 *
		 * @return void
		 */
		public function register_settings() {

			$page_name    = 'pptm-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_pptm_shortcode_number',
					'title'    => esc_html__( 'Number', 'yith_pp_testimonials' ),
					'callback' => 'print_number_input',
				),
				array(
					'id'       => 'yith_pptm_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith_pp_testimonials' ),
					'callback' => 'print_show_image',
				),
				array(
					'id'       => 'yith_pptm_shortcode_hover_effect',
					'title'    => esc_html__( 'Hover Effect', 'yith_pp_testimonials' ),
					'callback' => 'print_show_hover_effect',
				),
				array(
					'id'       => 'yith_pptm_shortcode_border_radius',
					'title'    => esc_html__( 'Border-radius(in pixels)', 'yith_pp_testimonials' ),
					'callback' => 'print_show_border_radius',
				),
				array(
					'id'       => 'yith_pptm_shortcode_links_color',
					'title'    => esc_html__( 'Links Color', 'yith_pp_testimonials' ),
					'callback' => 'print_show_links_color',
				),
			);

			add_settings_section( $section_name, esc_html__( 'Section', 'yith_pp_testimonials' ), '', $page_name );

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}

		public function print_number_input() {
			$tmshrt_number = intval( get_option( 'yith_pptm_shortcode_number', 6 ) );
			?>
			<input type="number" id="yith_pptm_shortcode_number" name="yith_pptm_shortcode_number" value="<?php echo esc_html( $tmshrt_number ); ?>">
			<?php
		}

		public function print_show_image() {
			?>
			<input type="checkbox" class="pptm-option-panel_onoff_input" name="yith_pptm_shortcode_show_image" value='yes' id="yith_pptm_shortcode_show_image"
			<?php checked( get_option( 'yith_pptm_shortcode_show_image', 'yes' ), 'yes' ); ?>
			>
			<label for="shortcode_show_image" class="pptm-option-panel_onoff_label">
				<span class="pptm-option-panel_onoff_btn"></span>
			</label>
			<?php
		}

		public function print_show_hover_effect() {
			$hover_effect = get_option( 'yith_pptm_shortcode_hover_effect', '' );
			?>
			<select name="yith_pptm_shortcode_hover_effect" class="pptm-option-panel_onoff_input" id="yith_pptm_shortcode_show_image">
				<option value="" <?php selected( $hover_effect, '' ); ?>><?php esc_html_e( 'None', 'yith_pp_testimonials' ); ?></option>
				<option value="zoom" <?php selected( $hover_effect, 'zoom' ); ?>><?php esc_html_e( 'Zoom', 'yith_pp_testimonials' ); ?></option>
				<option value="highlight" <?php selected( $hover_effect, 'highlight' ); ?>><?php esc_html_e( 'Highlight', 'yith_pp_testimonials' ); ?></option>
			</select>
			<?php
		}

		public function print_show_border_radius() {
			?>
			<input type="number" id="yith_pptm_shortcode_border_radius" name="yith_pptm_shortcode_border_radius" value="<?php echo intval( get_option( 'yith_pptm_shortcode_border_radius', 7 ) ); ?>">
			<?php
		}

		public function print_show_links_color() {
			?>
			<input type="text" id="yith_pptm_shortcode_links_color" name="yith_pptm_shortcode_links_color" class="my-color-field" data-default-color="#0073aa" value="<?php echo get_option( 'yith_pptm_shortcode_links_color', '#0073aa' ); ?>">
			<?php
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_PPTM_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_PPTM_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_PPTM_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

		/**
		 * Register_panel
		 *
		 * @return void
		 */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}
			wp_enqueue_script( 'yith_pptm_admin_script' );

			$admin_tabs = array(
				'settings'                  => __( 'Settings', 'yith_pp_testimonials' ),
				'custom-post-type'          => __( 'Custom Post Type', 'yith_pp_testimonials' ),
				'custom-taxonomy-country'   => __( 'Taxonomy Country', 'yith_pp_testimonials' ),
				'custom-taxonomy-survivors' => __( 'Taxonomy Survivors', 'yith_pp_testimonials' ),
			);

			$args         = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Testimonial', // this text MUST be NOT translatable
				'menu_title'         => 'YITH Testimonial', // this text MUST be NOT translatable
				'plugin_description' => __( 'Put here your plugin description', 'yith_pp_testimonials' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith_pp_testimonials',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_pptm_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_PPTM_DIR_PATH,
				'options-path'       => YITH_PPTM_DIR_PATH . 'plugin-options',
			);
			$this->_panel = new YIT_Plugin_Panel( $args );
		}

		/**
		 * Add_metabox
		 *
		 * @return void
		 */
		public function add_metabox() {
			$args = array(
				'label'    => __( 'Custom Metabox', 'yith_pp_testimonials' ),
				'pages'    => YITH_PPTM_Post_Types::$post_type,
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'settings' => array( // tab.
						'label'  => __( 'Settings', 'yith_pp_testimonials' ),
						'fields' => array(
							'yith_pptm_field_role'        => array(
								'label'   => __( 'Rol', 'yith_pp_testimonials' ),
								'desc'    => __( 'The rol you have in the company.', 'yith_pp_testimonials' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_pptm_field_company'     => array(
								'label'   => __( 'Company', 'yith_pp_testimonials' ),
								'desc'    => __( 'The company name.', 'yith_pp_testimonials' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_pptm_field_website'     => array(
								'label'   => __( 'Website', 'yith_pp_testimonials' ),
								'desc'    => __( 'The company website.', 'yith_pp_testimonials' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_pptm_field_email'       => array(
								'label'   => __( 'Email', 'yith_pp_testimonials' ),
								'desc'    => __( 'The personal email.', 'yith_pp_testimonials' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_pptm_field_custom'      => array(
								'id'     => 'yith_pptm_field_rate',
								'label'  => __( 'Rate', 'yith_pp_testimonials' ),
								'desc'   => __( 'The rating for this page or service.', 'yith_pp_testimonials' ),
								'type'   => 'custom',
								'action' => 'yith_pptm_print_custom_field',
							),
							'yith_pptm_field_vip'         => array(
								'label'   => __( 'VIP', 'yith_pp_testimonials' ),
								'desc'    => __( 'Give this person de VIP status.', 'yith_pp_testimonials' ),
								'type'    => 'onoff',
								'private' => false,
								'std'     => '',
							),
							'yith_pptm_field_badge'       => array(
								'id'      => 'yith_pptm_toggle_badge',
								'label'   => __( 'Badge', 'yith_pp_testimonials' ),
								'desc'    => __( 'Give this person the special badge.', 'yith_pp_testimonials' ),
								'type'    => 'onoff',
								'private' => false,
								'std'     => '',
							),
							'yith_pptm_field_badge_text'  => array(
								'id'      => 'yith_pptm_badge_options_text',
								'label'   => __( 'Badge Text', 'yith_pp_testimonials' ),
								'desc'    => __( 'Badge content.', 'yith_pp_testimonials' ),
								'type'    => 'text',
								'private' => true,
								'std'     => '',
								'deps'    => array(
									'id'    => 'yith_pptm_toggle_badge',
									'value' => 'yes',
									'type'  => 'hide',

								),
							),
							'yith_pptm_field_badge_color' => array(
								'id'      => 'yith_pptm_badge_options_color',
								'label'   => __( 'Badge Color', 'yith_pp_testimonials' ),
								'desc'    => __( 'Badge color.', 'yith_pp_testimonials' ),
								'type'    => 'colorpicker',
								'private' => true,
								'std'     => '',
								'deps'    => array(
									'id'    => 'yith_pptm_toggle_badge',
									'value' => 'yes',
									'type'  => 'hide',

								),
							),

						),
					),
				),
			);

			$metabox1 = YIT_Metabox( 'yith_pptm_metabox' );
			$metabox1->init( $args );
		}

		/**
		 * Show_custom_field_action
		 *
		 * @param  mixed $field arguments of the input.
		 * @return void
		 */
		public function show_custom_field_action( $field ) {
			yith_pptm_get_view( '/fields/rating-stars.php', $field );
		}


		public function enqueue_admin_styles() {
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'yith_pptm_admin_stylesheet', YITH_PPTM_DIR_ASSETS_CSS_URL . '/yith_pptm_admin_stylesheet.css' );

		}
		public function enqueue_admin_scripts() {
			wp_register_script( 'yith_pptm_admin_script', YITH_PPTM_DIR_ASSETS_JS_URL . '/yith_pptm_admin_script.js', array( 'wp-color-picker' ), false, true );

		}


	}


}


?>
