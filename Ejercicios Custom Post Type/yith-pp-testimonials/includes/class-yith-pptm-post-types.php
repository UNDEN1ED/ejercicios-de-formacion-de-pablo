<?php


if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPTM_Post_Types' ) ) {
	class YITH_PPTM_Post_Types {


		private static $instance;

		public static $post_type = 'yith_testimonials';

		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_types' ) );
			add_action( 'init', array( $this, 'setup_custom_taxonomies_survivors' ) );
			add_action( 'init', array( $this, 'setup_custom_terms_survivors' ) );
			add_action( 'init', array( $this, 'setup_custom_taxonomies_country' ) );
		}

		public function setup_post_types() {
			// This array contains all the labels that we want to appear in our dashboard when managing our custom posts
			$labels = array(

				'name'               => __( 'Testimonials', 'yith_pp_testimonials' ),
				'singular_name'      => __( 'Testimonial', 'yith_pp_testimonials' ),
				'add_new'            => __( 'Add New Testimonial', 'yith_pp_testimonials' ),
				'add_new_item'       => __( 'Add New Testimonial', 'yith_pp_testimonials' ),
				'edit_item'          => __( 'Edit Testimonial', 'yith_pp_testimonials' ),
				'new_item'           => __( 'New Testimonial', 'yith_pp_testimonials' ),
				'view_item'          => __( 'View Testimonial', 'yith_pp_testimonials' ),
				'search_items'       => __( 'Search Testimonials', 'yith_pp_testimonials' ),
				'not_found'          => __( 'No Testimonials found', 'yith_pp_testimonials' ),
				'not_found_in_trash' => __( 'No Testimonials found in Trash', 'yith_pp_testimonials' ),
				'all_items'          => __( 'All Testimonials', 'yith_pp_testimonials' ),
			);
			// Here it will give all the option that our custom post will use like the labels, and the type of custom post
			$args = array(
				'labels'              => $labels,
				'description'         => __( 'Post type to store Testimonials and display with the use of a shortcode', 'yith_pp_testimonials' ),
				'public'              => false,
				'publicly_ queryable' => false,
				'show_ui'             => true,
				'show_in_menu'        => false,
				'query_var'           => false,
				'menu_icon'           => 'dashicons-book-alt',
				'capability_type'     => array( 'testimonial', 'testimonials' ),
				'has_archive'         => true,
				'hierarchical'        => false,
				'exclude_from_search' => true,
				'menu_position'       => 20,
				'show_in_rest'        => false,
				'supports'            => array( 'title', 'editor', 'thumbnail' ),

			);
			// Register the custom post with all the arguments that we want it to use
			register_post_type( 'yith_testimonials', $args );
		}

		public function setup_custom_taxonomies_survivors() {

			$labels = array(

				'name'               => __( 'Survivors', 'yith_pp_testimonials' ),
				'singular_name'      => __( 'Survivor', 'yith_pp_testimonials' ),
				'add_new'            => __( 'Add New Survivor', 'yith_pp_testimonials' ),
				'add_new_item'       => __( 'Add New Survivor', 'yith_pp_testimonials' ),
				'edit_item'          => __( 'Edit Survivor', 'yith_pp_testimonials' ),
				'new_item'           => __( 'New Survivor', 'yith_pp_testimonials' ),
				'view_item'          => __( 'View Survivor', 'yith_pp_testimonials' ),
				'search_items'       => __( 'Search Survivors', 'yith_pp_testimonials' ),
				'not_found'          => __( 'No Survivors found', 'yith_pp_testimonials' ),
				'not_found_in_trash' => __( 'No Survivors found in Trash', 'yith_pp_testimonials' ),
				'all_items'          => __( 'All Survivors', 'yith_pp_testimonials' ),
			);

			$args = array(
				'labels'              => $labels,
				'description'         => __( 'Category for different types of reviews from important people', 'yith_pp_testimonials' ),
				'public'              => false,
				'publicly_ queryable' => false,
				'show_ui'             => true,
				'query_var'           => false,
				'has_archive'         => true,
				'hierarchical'        => true,
				'capabilities'        => array(
					'manage_terms' => 'edit_testimonial',
					'edit_terms'   => 'edit_testimonial',
					'delete_terms' => 'edit_testimonial',
					'assign_terms' => 'edit_testimonial',
				),
			);

			register_taxonomy( 'yith_pptm_survivors', 'yith_testimonials', $args );

		}

		public function setup_custom_taxonomies_country() {

			$labels = array(

				'name'               => __( 'Countries', 'yith_pp_testimonials' ),
				'singular_name'      => __( 'Country', 'yith_pp_testimonials' ),
				'add_new'            => __( 'Add New Country', 'yith_pp_testimonials' ),
				'add_new_item'       => __( 'Add New Country', 'yith_pp_testimonials' ),
				'edit_item'          => __( 'Edit Country', 'yith_pp_testimonials' ),
				'new_item'           => __( 'New Country', 'yith_pp_testimonials' ),
				'view_item'          => __( 'View Country', 'yith_pp_testimonials' ),
				'search_items'       => __( 'Search Countries', 'yith_pp_testimonials' ),
				'not_found'          => __( 'No Countries found', 'yith_pp_testimonials' ),
				'not_found_in_trash' => __( 'No Countries found in Trash', 'yith_pp_testimonials' ),
				'all_items'          => __( 'All Countries', 'yith_pp_testimonials' ),
			);

			$args = array(
				'labels'              => $labels,
				'description'         => __( 'Category for different types of reviews arranged by country', 'yith_pp_testimonials' ),
				'public'              => false,
				'publicly_ queryable' => false,
				'show_ui'             => true,
				'show_in_menu'        => false,
				'query_var'           => false,
				'has_archive'         => true,
				'hierarchical'        => false,
			);

			register_taxonomy( 'yith_pptm_country', 'yith_testimonials', $args );

		}

		public function setup_custom_terms_survivors() {

			wp_insert_term(
				__( 'Modern days', 'yith_pp_testimonials' ),
				'yith_pptm_survivors',
				array(
					'description' => __( 'Survivors after the events of WWII', 'yith_pp_testimonials' ),
					'slug'        => 'modern',
				)
			);

			wp_insert_term(
				__( 'Old days', 'yith_pp_testimonials' ),
				'yith_pptm_survivors',
				array(
					'description' => __( 'Survivors before the events of WWII', 'yith_pp_testimonials' ),
					'slug'        => 'old',
				)
			);

		}





	}
}
