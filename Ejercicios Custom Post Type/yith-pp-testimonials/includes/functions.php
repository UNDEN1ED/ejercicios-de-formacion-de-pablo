<?php

if ( ! function_exists( 'yith_pptm_get_template' ) ) {
	function yith_pptm_get_template( $file_name, $args = array() ) {
		extract( $args );

		$full_path = YITH_PPTM_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_pptm_get_view' ) ) {
	function yith_pptm_get_view( $file_name, $args = array() ) {
		extract( $args );

		$full_path = YITH_PPTM_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


function yith_pptm_rating_stars( $rating ) {
	echo "<div class='yith_pptm_rating'>";
	for ( $i = 1; $i < 6;$i++ ) {
		if ( $i <= $rating ) {
			echo "<label for='star" . $i . "' title='text' class='yith_pptm_star_full'>★</label>";
		} else {
			echo "<label for='star" . $i . "' title='text' class='yith_pptm_star_empty'>★</label>";
		}
	};
	echo '</div>';
}
