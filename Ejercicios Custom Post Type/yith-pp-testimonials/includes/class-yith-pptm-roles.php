<?php

if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPTM_Roles' ) ) {

	class YITH_PPTM_Roles {

		private static $instance;

		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		private function __construct() {

			add_action( 'admin_init', array( $this, 'setup_role_ttmls' ) );
			add_action( 'admin_init', array( $this, 'add_admin_cap_testimonial' ) );

		}

		public function setup_role_ttmls() {

			if ( get_option( 'yith_add_role_testimonials', false ) ) {
				add_role(
					'ttmls-manager',
					'TTMLS Manager',
					array(
						'edit_testimonials'         => true,
						'edit_testimonial'          => true,
						'read_testimonial'          => true,
						'delete_testimonials'       => true,
						'delete_testimonial'        => true,
						'publish_testimonials'      => true,
						'read_private_testimonials' => true,
						'create_testimonials'       => true,
						'manage_testimonials'       => true,
						'upload_files'              => true,
						'read'                      => true,
					)
				);
				update_option( 'yith_pptm_add_role_testimonials', true );

			};

		}

		public function add_admin_cap_testimonial() {
			if ( get_option( 'yith_add_cap_admin_testimonials', false ) ) {
				$capabilities = array(
					'edit_testimonials',
					'edit_testimonial',
					'read_testimonial',
					'delete_testimonials',
					'delete_testimonial',
					'publish_testimonials',
					'read_private_testimonials',
					'create_testimonials',
					'manage_testimonials',
					'upload_files',
					'read',
				);

				$admin_role = get_role( 'administrator' );

				foreach ( $capabilities as $cap ) {

					$admin_role->add_cap( $cap, true );

				}
				update_option( 'yith_add_cap_admin_testimonials', true );

			}
		}

	}
}
