<?php

if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPTM_Frontend' ) ) {

	class YITH_PPTM_Frontend {


		private static $instance;

		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );

		}

		public function enqueue_styles() {

			wp_register_style( 'yith_pptm_mystylesfile', YITH_PPTM_DIR_ASSETS_CSS_URL . '/yith_pptm_frontend_stylesheet.css' );
			$custom_css = '
                            .yith_pptm_container .yith_pptm_item{
                                border-radius: ' . get_option( 'yit_yith_pp_testimonials_options' )['yith_pptm_shortcode_border_radius'] . 'px
                            }
                            .yith_pptm_container a{
                                color: ' . get_option( 'yit_yith_pp_testimonials_options' )['yith_pptm_shortcode_links_color'] . '
                            }';
			wp_add_inline_style( 'yith_pptm_mystylesfile', $custom_css );
		}



	}



}
