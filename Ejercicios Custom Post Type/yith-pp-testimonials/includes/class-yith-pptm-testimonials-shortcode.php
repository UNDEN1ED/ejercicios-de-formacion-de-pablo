<?php


if ( ! defined( 'YITH_PPTM_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPTM_Testimonials_Shortcode' ) ) {
	class YITH_PPTM_Testimonials_Shortcode {

		/**
		 * Main Instance
		 *
		 * @var YITH_PPTM_Testimonials_Shortcode
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PPTM_Testimonials_Shortcode
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_pptm_add_custom_shortcode' ) );
		}

		/**
		 * Setup_shortcode
		 *
		 * @param  mixed $atts attributes for shortcode.
		 * @return String
		 */
		public function setup_shortcode( $atts ) {

			$a       = shortcode_atts(
				array(
					'number'       => strval( get_option( 'yit_yith_pp_testimonials_options' )['yith_pptm_shortcode_number'] ),
					'ids'          => '',
					'show_image'   => get_option( 'yit_yith_pp_testimonials_options' )['yith_pptm_shortcode_show_image'],
					'hover_effect' => get_option( 'yit_yith_pp_testimonials_options' )['yith_pptm_shortcode_hover_effect'],
					'tax_ids'      => '',
				),
				$atts
			);
			$id_list = ( ! empty( $a['ids'] ) ) ? explode( ',', $a['ids'] ) : array();

			$args = array(
				'numberposts' => ( $a['number'] > count( $id_list ) ) ? $a['number'] : count( $id_list ),
				'post_type'   => 'yith_testimonials',
				'require '    => isset( $a['ids'] ) && ! empty( $a['ids'] ) ? $id_list : false,
			);

			if ( isset( $a['tax_ids'] ) && ! empty( $a['tax_ids'] ) ) {
				$tax_id_list       = explode( ',', $a['tax_ids'] );
				$args['tax_query'] = array(
					'relation' => ' or ',
					array(
						'taxonomy' => 'yith_pptm_survivors',
						'field'    => 'term_id',
						'terms'    => $tax_id_list,
					),
					array(
						'taxonomy' => 'country',
						'field'    => 'term_id',
						'terms'    => $tax_id_list,
					),

				);

			};

			$posts_list     = get_posts( $args );
			$shortcode_args = array(
				'a'          => $a,
				'posts_list' => $posts_list,
			);
			wp_enqueue_style( 'yith_pptm_mystylesfile' );
			ob_start();
			yith_pptm_get_template( '/frontend/testimonials.php', $shortcode_args );
			return ob_get_clean();
		}





		/**
		 * Yith_pptm_add_custom_shortcode
		 *
		 * @return void
		 */
		public function yith_pptm_add_custom_shortcode() {
			add_shortcode( 'yith_pptm_shortcode_grid', array( $this, 'setup_shortcode' ) );
		}

	}
}
