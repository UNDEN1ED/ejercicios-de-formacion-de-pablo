<?php
/*
Plugin Name: Widget 1
Plugin URI: http://local.wordpress.test/
Description: Display the titles of the last 5 recent posts
Author: Pablo Pérez Arteaga
Text Domain: ejercicio_widgets_1
Version: 1.7.2
Author URI: 
*/

function yith_wdt1_load_plugin_textdomain() {
    load_plugin_textdomain( 'ejercicio_widgets_1', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'yith_wdt1_load_plugin_textdomain' );


//Clase del widget 
class YITH_Post_Widget extends WP_Widget {

public function __construct() {
    $widget_ops = array( 
        'classname' => 'my_widget',
        'description' => esc_html__('This widget will display the lastest 5 posts', 'ejercicio_widgets_1'),
    );
    parent::__construct( 'my_widget', 'Yith Widget Post', $widget_ops );
}

//Se muestra los 5 ultimos posts desde un array generado por get_posts()
public function widget( $args, $instance ) {
    echo $args['before_widget'];	
	echo $args['before_title'] . esc_html__('POSTS LIST', 'ejercicio_widgets_1') . $args['after_title']; 
    $posts= get_posts();
    if(! count($posts) == 0){
        foreach($posts as $post_l){
            echo  "<p>- ".$post_l->post_title ."</p>";
        };
    }else{
        echo "<p class='error-post'><i>" . esc_html__('There are no post in the database', 'ejercicio_widgets_1') . "</i></p>";
    }
}

//No se requiere ninguna personalización
public function form( $instance ) {
    
}
//Al no requerirse ningun cambio no se requiere guardar el cambio
public function update( $new_instance, $old_instance ) {

}
}

//Hook para controlar cuando se registra el widget
add_action( 'widgets_init', 'yith_register_widgets_post' );
 
function yith_register_widgets_post() {
    register_widget( 'YITH_Post_Widget' );
}
?>