<?php

/*
Plugin Name: Widget 2
Plugin URI: http://local.wordpress.test/
Description: Display the introduced user information  
Author: Pablo Pérez Arteaga
Text Domain: ejercicio_widgets_2
Version: 1.7.2
Author URI: 
*/

function yith_wdt2_load_plugin_textdomain() {
    load_plugin_textdomain( 'ejercicio_widgets_2', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'yith_wdt2_load_plugin_textdomain' );


//Clase del widget 
class YITH_Widget_User_Info extends WP_Widget {

    public function __construct() {
        $widget_ops = array( 
            'classname' => 'yith_widget_user_info',
            'description' => 'This widget will display the user info',
        );
        parent::__construct( 'yith_widget_user_info', 'Widget User Info', $widget_ops );
    }
    
    //Esta funcion mostrará los datos del usuario que se introduzca en la configuración
    public function widget( $args, $instance ) {
     

        echo $args['before_widget'];	
		echo $args['before_title'] . esc_html__('User info', 'ejercicio_widgets_2') . $args['after_title'];
        echo "<p> <b>". esc_html__('Name', 'ejercicio_widgets_2') . ":</b> " . $instance["name"] . "</p>";
        echo "<p> <b>" . esc_html__('Lastname', 'ejercicio_widgets_2') . ":</b> " . $instance["lastname"] . "</p>";
        echo "<p> <b>" . esc_html__('Address', 'ejercicio_widgets_2') . ":</b> " . $instance["address"] . "</p>";
        echo "<p> <b>" . esc_html__('Phone', 'ejercicio_widgets_2') . ":</b> " . $instance["phone"] . "</p>";
        echo $args['after_widget'];
    }
    
    //Se crean los campos en los que introduceremos la información deseada
    public function form( $instance ) {
        
        $name = !empty($instance['name']) ?  $instance['name'] : '';
        $lastname = !empty($instance['lastname']) ? $instance['lastname'] : '';
        $address = !empty($instance['address']) ? $instance['address'] : '';
        $phone = !empty($instance['phone']) ? $instance['phone'] : '';


        ?>
        <p><label for="<?php echo $this->get_field_id('name'); ?>"><?php _e('Nombre: '); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('name'); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" type="text" value="<?php echo esc_attr($name);?>"></p>
        <p><label for="<?php echo $this->get_field_id( 'lastname' ); ?>"><?php _e( 'Apellidos:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'lastname' ); ?>" name="<?php echo $this->get_field_name( 'lastname' ); ?>" type="text" value="<?php echo esc_attr( $lastname ); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'direction' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Telefono:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>" /></p>

    <?php
    }
    
    //Se guardan los campos que se requieren del formulario
    public function update( $new_instance, $old_instance ) {
    
        $instance = array();
        $instance['name']= (!empty($new_instance['name'])) ? strip_tags($new_instance['name']) : '';
        $instance['lastname']= (!empty($new_instance['lastname'])) ? strip_tags($new_instance['lastname']) : '';
        $instance['address']= (!empty($new_instance['address'])) ? strip_tags($new_instance['address']) : '';
        $instance['phone']= (!empty($new_instance['phone'])) ? strip_tags($new_instance['phone']) : '';
        return $instance;

    }
    }
    
    //Hook que controla cuando se se registrara el widget
    add_action( 'widgets_init', 'yith_register_widgets_info' );
     
    function yith_register_widgets_info() {
        register_widget( 'YITH_Widget_User_Info' );
    }



?>